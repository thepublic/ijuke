Online Store IT-project
======================================================
Computer development system of non-commercial online store.  
Project is created as education base for working with different 
Java-related back-end and front-end technologies.  
It can be used for code studying and training purposes.

##Required Software installed
+ Java 1.7
+ Maven 3
+ Tomcat 7
+ Any SQL RDBMS supported by Oracle (default is MySQL)
+ jMeter (optional for testing)
+ IDE (default is Intellij Idea)
+ SonarQube and Jenkins (optional, installed in [Vagrant box](https://atlas.hashicorp.com/x7warrior/boxes/ubuntu1404jenkins))

##Used development technologies:
 Back-end:  
 :heavy_check_mark:Java 1.7 
 :heavy_check_mark:Spring Core
 :heavy_check_mark:Lombok Project
 :heavy_check_mark:Google Guava
 :heavy_check_mark:Java Encription
 
 Database:  
 :heavy_check_mark:Hibernate
 :heavy_check_mark:Flyway
 :heavy_check_mark:HSQLDB
 
 Frond-end:  
 :heavy_check_mark:Spring MVC
 :heavy_check_mark:Jackson
 :heavy_check_mark:Jersey
 :heavy_check_mark:Stripes MVC
 :heavy_check_mark:jQuery
 
 Testing:  
 :heavy_check_mark:JUnit
 :heavy_check_mark:TestNG
 :heavy_check_mark:Spring Testing
 :heavy_check_mark:Mockito
 :heavy_check_mark:JWebUnit
 :heavy_check_mark:JBehave
    
 Other:  
 :heavy_check_mark:Log4j
 :heavy_check_mark:Apache Maven
 :heavy_check_mark:Apache Tomcat
 :heavy_check_mark:Apache jMeter load testing tool
 
##Configuring and run project
* Create **ijuke** database.
* Configure ijuke.pom section with your own parameters:
```xml
<properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <surefire.version>2.17</surefire.version>
        <mysql.host>localhost</mysql.host>
        <mysql.user>ijukeuser</mysql.user>
        <mysql.password>ijukeuserpass</mysql.password>
        <mysql.database>ijuke</mysql.database>
        <env.property>localhost</env.property>
        <frontside.hostPort.default>http://localhost:8080</frontside.hostPort.default>
    </properties>
```
* Fill database content with flyway
```shell
mvn flyway:migrate -pl backside
```
* Create backside and frontside jar files 
```shell
mvn install -pl backside, frontside
```
* Deploy **jar**(s) to Tomcat server
* Go to Contact List Page (/frontside/ContactList.action)

OPTIONAL:

* For using Jenkins and SonarQube install Vagrant and necessary box mentioned above.  
Settings of ubuntu1404jenkins VM are in **Vagrantfile** file.