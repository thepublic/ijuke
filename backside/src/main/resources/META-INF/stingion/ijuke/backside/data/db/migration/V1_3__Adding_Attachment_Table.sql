create table `attachment` (
  `id` int not null unique auto_increment,
  `fileName` varchar(250),
  `size` bigint,
  `contentType` varchar(50),
  primary key (`id`)
);
