-- ----- CONTACT -----
insert into `contact` (`first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `birth_day`)
values ('Sophie', 'Mix', 'Hunter', 'sh@isystem.com', '0715558440', '1981-8-8');
insert into `contact` (`first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `birth_day`)
values ('Daniel', 'Corry', 'Greene', 'dg@isystem.com', '0725557763', '1986-6-3');
insert into `contact` (`first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `birth_day`)
values ('Jen', 'Miles', 'Ballou', 'jb@isystem.com', '0725556495', '1982-8-30');
insert into `contact` (`first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `birth_day`)
values ('Sammy', 'John', 'Blair', 'sb@isystem.com', '0725559592', '1981-6-2');
insert into `contact` (`first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `birth_day`)
values ('Betty', 'Brin', 'Stocker', 'bs@isystem.com', '0735558316', '1987-5-22');
insert into `contact` (`first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `birth_day`)
values ('Lou', 'Luise', 'Thompson', 'lt@isystem.com', '0715552765', '1980-8-29');
insert into `contact` (`first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `birth_day`)
values ('Lexi', 'Lind', 'Hawk', 'lh@isystem.org', '0725137211', '1982-5-1');
insert into `contact` (`first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `birth_day`)
values ('George', 'Devidson', 'Wells', 'gw@isystem.com', '0715557689', '1987-5-15');
insert into `contact` (`first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `birth_day`)
values ('Donna', 'Ostin', 'McCallum', 'dm@isystem.com', '0735553432', '1979-12-28');
insert into `contact` (`first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `birth_day`)
values ('Jason', 'Karl', 'Wilson', 'jw@isystem.org', '0715554032', '1978-4-3');

-- ----- BANK -----
insert into `bank` (`name`) values ('Unicredit');
insert into `bank` (`name`) values ('Raiffeisen');
insert into `bank` (`name`) values ('PrivatBank');
insert into `bank` (`name`) values ('UkrSibbank');
insert into `bank` (`name`) values ('Platinum Bank');

-- ----- CREDIT_CARD -----
insert into `credit_card` (`number`, `type`, `bank_id`, `contact_id`)
values(
  1234571277518812,
  'MASTER_CARD',
  (select `id` from `bank` where `name` = 'PrivatBank'),
  (select `id` from `contact` where `email` = 'sh@isystem.com')
);
insert into `credit_card` (`number`, `type`, `bank_id`, `contact_id`)
values(
  6647486058945594,
  'VISA',
  (select `id` from `bank` where `name` = 'Unicredit'),
  (select `id` from `contact` where `email` = 'sh@isystem.com')
);
insert into `credit_card` (`number`, `type`, `bank_id`, `contact_id`)
values(
  2237435131126577,
  'VISA',
  (select `id` from `bank` where `name` = 'Raiffeisen'),
  (select `id` from `contact` where `email` = 'dg@isystem.com')
);
insert into `credit_card` (`number`, `type`, `bank_id`, `contact_id`)
values(
  1123729594138004,
  'MASTER_CARD',
  (select `id` from `bank` where `name` = 'UkrSibbank'),
  (select `id` from `contact` where `email` = 'jb@isystem.com')
);
insert into `credit_card` (`number`, `type`, `bank_id`, `contact_id`)
values(
  6541296068298653,
  'VISA',
  (select `id` from `bank` where `name` = 'Platinum Bank'),
  (select `id` from `contact` where `email` = 'sb@isystem.com')
);
insert into `credit_card` (`number`, `type`, `bank_id`, `contact_id`)
values(
  5923542550577566,
  'MASTER_CARD',
  (select `id` from `bank` where `name` = 'Platinum Bank'),
  (select `id` from `contact` where `email` = 'sb@isystem.com')
);
insert into `credit_card` (`number`, `type`, `bank_id`, `contact_id`)
values(
  7970188838706486,
  'VISA',
  (select `id` from `bank` where `name` = 'Unicredit'),
  (select `id` from `contact` where `email` = 'bs@isystem.com')
);
insert into `credit_card` (`number`, `type`, `bank_id`, `contact_id`)
values(
  2184747417855136,
  'VISA',
  (select `id` from `bank` where `name` = 'PrivatBank'),
  (select `id` from `contact` where `email` = 'bs@isystem.com')
);

-- ----- PRODUCT -----
insert into `product` (`name`, `price`) values ('Laptop Asus x55vd', 480);
insert into `product` (`name`, `price`) values ('Laptop Apple MacBook Air', 1400);
insert into `product` (`name`, `price`) values ('Laptop Dell Inspiron 7720', 1050);
insert into `product` (`name`, `price`) values ('E-book Amazon Kindle 5', 80.50);
insert into `product` (`name`, `price`) values ('E-book Pocketbook Basic', 105.25);
insert into `product` (`name`, `price`) values ('Phone Apple iPhone 5 16GB', 750);
insert into `product` (`name`, `price`) values ('Phone Samsung N7100 Galaxy Note 2', 670);
insert into `product` (`name`, `price`) values ('TV Samsung DM42', 720);

-- ----- ORDERING -----
insert into `ordering` (`description`, `status`, `product_id`, `credit_card_id`)
                values ('Deliver to Kyiv, Melnyka 35/5', 'PENDING',
                        (select `id` from `product` where `name` = 'E-book Amazon Kindle 5'),
                        (select `id` from `credit_card` where `number` = 2184747417855136)
                );
insert into `ordering` (`description`, `status`, `product_id`, `credit_card_id`)
                values ('Deliver to Myshkova, Stynova 15/15 after 18:00', 'PENDING',
                        (select `id` from `product` where `name` = 'Phone Apple iPhone 5 16GB'),
                        (select `id` from `credit_card` where `number` = 7970188838706486)
                );
insert into `ordering` (`description`, `status`, `product_id`, `credit_card_id`)
                values ('Deliver to Kyiv, Strilsova 11/11', 'PENDING',
                        (select `id` from `product` where `name` = 'E-book Amazon Kindle 5'),
                        (select `id` from `credit_card` where `number` = 6541296068298653)
                );
insert into `ordering` (`description`, `status`, `product_id`, `credit_card_id`)
                values ('Cancelled cause of subjective reason', 'CANCELLED',
                        (select `id` from `product` where `name` = 'Phone Samsung N7100 Galaxy Note 2'),
                        (select `id` from `credit_card` where `number` = 5923542550577566)
                );
insert into `ordering` (`description`, `status`, `product_id`, `credit_card_id`)
                values ('Delivered to Kyiv, Smirnova 25/101', 'PERFORMED',
                        (select `id` from `product` where `name` = 'Laptop Asus x55vd'),
                        (select `id` from `credit_card` where `number` = 1234571277518812)
                );

-- ----- SUPPLIER -----
insert into `supplier` (`name`) values ('Technomir');
insert into `supplier` (`name`) values ('Rozetka');
insert into `supplier` (`name`) values ('Foxtrot');
insert into `supplier` (`name`) values ('Comfy');
insert into `supplier` (`name`) values ('Fotos');

-- ----- PRODUCT_SUPPLIER -----
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Laptop Asus x55vd'),
  (select `id` from supplier where `name` = 'Rozetka')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Laptop Asus x55vd'),
  (select `id` from supplier where `name` = 'Foxtrot')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Laptop Asus x55vd'),
  (select `id` from supplier where `name` = 'Comfy')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Laptop Apple MacBook Air'),
  (select `id` from supplier where `name` = 'Rozetka')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Laptop Apple MacBook Air'),
  (select `id` from supplier where `name` = 'Technomir')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Laptop Dell Inspiron 7720'),
  (select `id` from supplier where `name` = 'Technomir')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Laptop Dell Inspiron 7720'),
  (select `id` from supplier where `name` = 'Rozetka')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Laptop Dell Inspiron 7720'),
  (select `id` from supplier where `name` = 'Foxtrot')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'E-book Amazon Kindle 5'),
  (select `id` from supplier where `name` = 'Rozetka')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'E-book Amazon Kindle 5'),
  (select `id` from supplier where `name` = 'Foxtrot')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'E-book Amazon Kindle 5'),
  (select `id` from supplier where `name` = 'Comfy')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'E-book Pocketbook Basic'),
  (select `id` from supplier where `name` = 'Rozetka')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Phone Apple iPhone 5 16GB'),
  (select `id` from supplier where `name` = 'Rozetka')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Phone Apple iPhone 5 16GB'),
  (select `id` from supplier where `name` = 'Foxtrot')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Phone Samsung N7100 Galaxy Note 2'),
  (select `id` from supplier where `name` = 'Rozetka')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Phone Samsung N7100 Galaxy Note 2'),
  (select `id` from supplier where `name` = 'Foxtrot')
);
insert into `product_supplier` (`product_id`, `supplier_id`)
values (
  (select `id` from product where `name` = 'Phone Samsung N7100 Galaxy Note 2'),
  (select `id` from supplier where `name` = 'Comfy')
);
