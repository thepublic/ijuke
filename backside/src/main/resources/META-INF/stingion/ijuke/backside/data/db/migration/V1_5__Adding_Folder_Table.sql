create table `folder` (
  `id` int not null unique auto_increment,
  `name` varchar(50) unique,
  primary key (`id`)
);

alter table `message` add column `folder_id` int;
alter table `message` add foreign key (`folder_id`) references `folder` (`id`);

-- ----- FOLDER -----
insert into `folder` (`name`) values ('Inbox');
insert into `folder` (`name`) values ('Sent');
insert into `folder` (`name`) values ('Trash');

-- ----- MESSAGE -----
update `message` set `folder_id` = 1 where `id` = 1;
update `message` set `folder_id` = 1 where `id` = 2;
update `message` set `folder_id` = 3 where `id` = 3;
