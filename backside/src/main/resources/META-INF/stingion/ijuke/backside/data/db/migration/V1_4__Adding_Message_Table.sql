create table `message` (
  `id` int not null unique auto_increment,
  `from` varchar(50),
  `to` varchar(50),
  `cc` varchar(50),
  `bcc` varchar(50),
  `subject` varchar(50),
  `text` text,
  `date` datetime,
  primary key (`id`)
);

alter table `attachment` add column `message_id` int;
alter table `attachment` add foreign key (`message_id`) references `message` (`id`);

-- ----- MESSAGE -----
insert into `message` (`from`, `to`, `cc`, `subject`, `text`, `date`)
               values ('lh@isystem.org', 'gw@isystem.com', 'dm@isystem.com', 'Hi!', 'How are you? \\n Where are you?',
                       '2015-01-20');
insert into `message` (`from`, `to`, `cc`, `bcc`, `subject`, `text`, `date`)
               values ('gw@isystem.com', 'lh@isystem.org', 'dm@isystem.com', 'sb@isystem.com', 'hello', 'Who are you?',
                       '2014-11-19');
insert into `message` (`from`, `to`, `subject`, `text`, `date`)
               values ('gw@isystem.com', 'lh@isystem.org', 'Ok subject', '?', '2015-01-15');

-- ----- ATTACHMENT -----
insert into `attachment` (`fileName`, `size`, `contentType`, `message_id`)
                  values ('/home/some/int.txt', '128', 'text/plain', 1);
insert into `attachment` (`fileName`, `size`, `contentType`, `message_id`)
                  values ('/home/some/some file.xml', '512', 'application/xml', 1);
insert into `attachment` (`fileName`, `size`, `contentType`, `message_id`)
                  values ('/home/some/another.xml', '512', 'application/xml', 3);

