alter table `contact` add column `gender` enum('MALE','FEMALE') DEFAULT NULL;

update `contact` set `gender` = 'FEMALE' where `email` = 'sh@isystem.com';
update `contact` set `gender` = 'MALE'   where `email` = 'dg@isystem.com';
update `contact` set `gender` = 'FEMALE' where `email` = 'jb@isystem.com';
update `contact` set `gender` = 'MALE'   where `email` = 'sb@isystem.com';
update `contact` set `gender` = 'FEMALE' where `email` = 'bs@isystem.com';
update `contact` set `gender` = 'MALE'   where `email` = 'lt@isystem.com';
update `contact` set `gender` = 'FEMALE' where `email` = 'lh@isystem.org';
update `contact` set `gender` = 'MALE'   where `email` = 'gw@isystem.com';
update `contact` set `gender` = 'FEMALE' where `email` = 'dm@isystem.com';
update `contact` set `gender` = 'MALE'   where `email` = 'jw@isystem.org';
