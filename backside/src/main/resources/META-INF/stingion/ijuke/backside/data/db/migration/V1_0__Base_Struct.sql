create table `bank` (
    `id` int not null unique auto_increment,
    `name` varchar(30) unique,
    primary key (`id`)
);

create table `contact` (
  `id` int not null auto_increment,
  `first_name` varchar(30),
  `middle_name` varchar(30),
  `last_name` varchar(30),
  `birth_day` datetime,
  `email` varchar(30) unique,
  `phone_number` varchar(30),
  primary key (`id`)
);

create table `credit_card` (
  `id` int not null auto_increment,
  `number` bigint unique,
  `type` enum('MASTER_CARD', 'VISA'),
  `bank_id` int,
  `contact_id` int,
  primary key (`id`),
  foreign key (`bank_id`) references `bank` (`id`),
  foreign key (`contact_id`) references `contact` (`id`)
);

create table `product` (
  `id` int not null auto_increment,
  `name` varchar(50) unique,
  `price` double,
  primary key (`id`)
);

create table `ordering` (
  `id` int not null auto_increment,
  `description` varchar(100),
  `status` enum('PENDING', 'PERFORMED', 'CANCELLED'),
  `product_id` int,
  `credit_card_id` int,
  primary key (`id`),
  foreign key (`credit_card_id`) references `credit_card` (`id`),
  foreign key (`product_id`) references `product` (`id`)
);

create table `supplier` (
  `id` int not null auto_increment,
  `name` varchar(50) unique,
  primary key (`id`)
);

create table `product_supplier` (
  `product_id` int,
  `supplier_id` int,
  primary key (`product_id` , `supplier_id`),
  foreign key (`product_id`) references `product` (`id`),
  foreign key (`supplier_id`)  references `supplier` (`id`)
);
