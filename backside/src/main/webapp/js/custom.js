//For description.jsp
var data = {
    "artTitle": "Project Description",
    "artContent": "\"backside\" module is mainly dedicated to learning development technologies, " +
    "primarily related with Java. It's back-end part of \"ijuke\" project."
};

function fillDefaultDescData() {
    $("#desc #artTitle").text(data.artTitle);
    $("#desc #artContent").text(data.artContent);
}

function fillDescData() {
    $.ajax({
        url: 'rest/open/projectDescription',
        type: 'POST',
        dataType: 'json',
        mimeType: 'application/json',
        success: function (dataOut) {
            $("#desc #artTitle").text(dataOut.title);
            $("#desc #artContent").text(dataOut.content);
        },
        error: function (status, er) {//NOSONAR
        }
    });
}
