<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@include file="/WEB-INF/stingion/ijuke/backside/jsp/common/taglibs.jsp" %>
<html>
    <head>
        <title>Description</title>
        <link rel="icon" href="${contextPath}/images/ijuke.png" type="image/x-icon">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="${contextPath}/js/custom.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                fillDescData();
            });
        </script>
    </head>
    <body>
        <article id="desc">
            <h1 id="artTitle"></h1>

            <p id="artContent"></p>
        </article>
    </body>
</html>