$(document).ready(function () {

    module("backside module JS testing");

    test('check default description data', function () {
        var titleExpected = "Project Description";
        var contentExpected = "\"backside\" module is mainly dedicated to learning development technologies, " +
            "primarily related with Java. It's back-end part of \"ijuke\" project.";
        
        var dataActual = data;
        
        equal(dataActual.artTitle, titleExpected, "Title of description");
        equal(dataActual.artContent, contentExpected, "Content of description");
    });

    test("check custom description data", function (assert) {
        assert.expect(2);

        var done = assert.async();

        $.ajax({
            url: '/rest/open/projectDescription',
            type: 'POST',
            dataType: 'json',
            mimeType: 'application/json',
            success: function (data) {
                var titleExpected = "Project Description";
                var contentExpected = "\"backside\" module is mainly dedicated to learning development technologies, primarily related with Java. It's back-end part of \"ijuke\" project.";
                assert.equal(data.title, titleExpected, "Title of description");
                assert.equal(data.content, contentExpected, "Content of description");
                done();
            },
            error: function (status, error) {
                assert.throws(
                    function() {
                        throw "error"
                    },
                    "Ajax request failed (status:" + status + "   error:" + error + ")"
                );
                done();
            }
        });
    });
});
