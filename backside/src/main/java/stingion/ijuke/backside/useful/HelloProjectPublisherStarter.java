/*
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.useful;

import stingion.ijuke.backside.ws.helloproject.rpc.HelloProjectEndpointImpl;
import stingion.ijuke.utils.ws.EndpointPublisher;

/**
 * The class is used for testside module integration tests
 * <p/>
 * //NOSONAR Todo: It's needed to move ws (HelloProject) functionality in separate module (jar)
 */
public class HelloProjectPublisherStarter {

    private HelloProjectPublisherStarter() {
    }

    public static void main(String[] args) {

        EndpointPublisher publisher = new EndpointPublisher();
        //NOSONAR Todo: it's good to change localhost to inet Address (XXX.XXX.XXX.XXX) for testing purpose
        publisher.setEndpointURL("http://localhost:9999/ws/helloProject");
        publisher.setImplementer(new HelloProjectEndpointImpl());
        publisher.start();
    }
}
