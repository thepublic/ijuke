/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.services;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;

@Service("creditCardDTOService")
public class CreditCardDTOService extends AbstractDTOService<CreditCard, CreditCardDTO> {
}
