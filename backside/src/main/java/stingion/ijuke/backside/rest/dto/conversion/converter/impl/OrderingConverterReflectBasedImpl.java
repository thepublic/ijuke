/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.rest.dto.conversion.converter.OrderingConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.impl.AbstractConverterReflectBasedImpl;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

@Service("orderingConverter")
public class OrderingConverterReflectBasedImpl extends AbstractConverterReflectBasedImpl<Ordering, OrderingDTO>
                                               implements OrderingConverter {
}
