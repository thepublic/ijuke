/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.rest.dto.conversion.converter.SupplierConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.impl.AbstractConverterReflectBasedImpl;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

@Service("supplierConverter")
public class SupplierConverterReflectBasedImpl extends AbstractConverterReflectBasedImpl<Supplier, SupplierDTO>
                                               implements SupplierConverter {
}
