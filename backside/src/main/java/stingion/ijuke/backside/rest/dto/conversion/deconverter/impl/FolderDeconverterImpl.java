/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Folder;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.FolderDeconverter;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;

@Service("folderDeconverter")
public class FolderDeconverterImpl extends AbstractDeconverterImpl<FolderDTO, Folder> implements FolderDeconverter {
}
