/**
 * Created by ievgen on 8/30/14 4:04 PM.
 *
 */
@Note(record = "REST functionality")
package stingion.ijuke.backside.rest;

import stingion.ijuke.utils.annotation.Note;