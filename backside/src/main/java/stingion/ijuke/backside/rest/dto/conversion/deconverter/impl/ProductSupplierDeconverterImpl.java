/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.ProductSupplierDeconverter;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;

@Service("productSupplierDeconverter")
public class ProductSupplierDeconverterImpl extends AbstractDeconverterImpl<ProductSupplierDTO, ProductSupplier> implements ProductSupplierDeconverter {
}
