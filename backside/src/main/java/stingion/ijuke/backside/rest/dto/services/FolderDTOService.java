/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.services;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Folder;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;

@Service("folderDTOService")
public class FolderDTOService extends AbstractDTOService<Folder, FolderDTO> {
}
