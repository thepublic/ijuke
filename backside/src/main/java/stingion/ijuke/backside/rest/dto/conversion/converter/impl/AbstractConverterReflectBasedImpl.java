/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import stingion.ijuke.backside.rest.dto.conversion.converter.Converter;
import stingion.ijuke.backside.rest.dto.conversion.utils.ConversionUtil;
import stingion.ijuke.utils.exception.FireRuntimeException;

import java.lang.reflect.ParameterizedType;

public class AbstractConverterReflectBasedImpl<T, DtoT> implements Converter<T, DtoT> {//NOSONAR

    private static final Integer DEFAULT_DEPTH = 1;

    @Autowired
    @Qualifier("conversionUtilToDTO")
    private ConversionUtil convertUtil;

    private Class<T> entityClass;

    private Class<T> entityDtoClass;

    @Override
    public DtoT convert(T entity) {
        return conversion(entity, DEFAULT_DEPTH, true);
    }

    @Override
    public DtoT convert(T entity, int depth) {
        return conversion(entity, depth, true);
    }

    @Override
    public DtoT convertWithLazyObjects(T entity) {
        return conversion(entity, DEFAULT_DEPTH, false);
    }

    @Override
    public DtoT convertWithLazyObjects(T entity, int depth) {
        return conversion(entity, depth, false);
    }

    @Override
    public DtoT convert(T entity, int depth, boolean isLazy) {
        return conversion(entity, depth, isLazy);
    }

    public DtoT conversion(T entity, int depth, boolean isLazy) {
        if (depth < 0)
            throw new IllegalArgumentException("Depth value is less then 0");

        try {
            return (DtoT) convertUtil.convert(entity, getDtoEntityClass().newInstance(), depth, isLazy);
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new FireRuntimeException(ex);
        }
    }

    public Class<T> getEntityClass() {
        if (entityClass == null) {
            ParameterizedType pType = (ParameterizedType) getClass().getGenericSuperclass();
            this.entityClass = (Class<T>) pType.getActualTypeArguments()[0];
        }

        return entityClass;
    }

    public Class<T> getDtoEntityClass() {
        if (entityDtoClass == null) {
            ParameterizedType pType = (ParameterizedType) getClass().getGenericSuperclass();
            this.entityDtoClass = (Class<T>) pType.getActualTypeArguments()[1];
        }

        return entityDtoClass;
    }
}
