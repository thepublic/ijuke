/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter;

import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

public interface BankConverter extends Converter<Bank, BankDTO> {
}
