/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Message;
import stingion.ijuke.backside.rest.dto.conversion.converter.MessageConverter;
import stingion.ijuke.utils.ijuke.dto.MessageDTO;

@Service("messageConverter")
public class MessageConverterReflectBasedImpl extends AbstractConverterReflectBasedImpl<Message, MessageDTO> implements MessageConverter {
}
