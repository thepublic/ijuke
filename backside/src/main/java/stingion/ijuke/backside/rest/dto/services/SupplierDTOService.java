/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.services;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

@Service("supplierDTOService")
public class SupplierDTOService extends AbstractDTOService<Supplier, SupplierDTO> {
}
