/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.services;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;

@Service("productDTOService")
public class ProductDTOService extends AbstractDTOService<Product, ProductDTO> {
}
