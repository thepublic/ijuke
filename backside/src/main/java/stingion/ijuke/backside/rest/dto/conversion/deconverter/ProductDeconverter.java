/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter;

import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;

public interface ProductDeconverter extends Deconverter<ProductDTO, Product> {
}
