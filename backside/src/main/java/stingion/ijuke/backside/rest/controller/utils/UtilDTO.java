package stingion.ijuke.backside.rest.controller.utils;

import org.springframework.beans.factory.annotation.Autowired;
import stingion.ijuke.backside.data.services.AbstractService;
import stingion.ijuke.backside.rest.dto.services.AbstractDTOService;
import stingion.ijuke.utils.exception.FireRuntimeException;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * Provides shortcut methods simplifies work with {@link stingion.ijuke.backside.rest.dto.services.AbstractDTOService}
 * converts methods
 * </p>
 *
 * <p>
 * Created by ievgen on 9/5/14 9:51 PM.
 * </p>
 */
public class UtilDTO<Entity, EntityDTO> {//NOSONAR
    private static final String GET_ID = "getId";

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private AbstractService<Entity> abstractService;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private AbstractDTOService<Entity, EntityDTO> abstractDTOService;

    //NOSONAR todo: Put utils method in separate class
    public Entity entity(Integer entityId) {
        return abstractService.getEntityById(entityId);
    }

    public Entity entity(EntityDTO entityDTO) {
        return abstractDTOService.deconvert(entityDTO);
    }

    public Entity entityWithLazyObjects(Integer entityId) {
        return abstractService.getEntityWithLazyObjectsById(entityId);
    }

    public Entity entityWithLazyObjects(Entity entity) {
        return entityWithLazyObjects(entityReflectionId(entity));
    }

    public Set<Entity> entityWithLazyObjects(Set<Entity> entitiesSet) {
        Set<Entity> entities = new HashSet<>();

        for (Entity entity : entitiesSet) {
            entities.add(entityWithLazyObjects(entity));
        }

        return entities;
    }

    public EntityDTO entityDTO(Entity entity) {
        return abstractDTOService.convert(entity);
    }

    public EntityDTO entityDTO(Integer entityId) {
        return entityDTO(entity(entityId));
    }

    public List<EntityDTO> entityDTO(List<Entity> entities) {
        List<EntityDTO> entitiesDTO = new ArrayList<>();

        for (Entity entity : entities) {
            entitiesDTO.add(entityDTO(entity));
        }

        return entitiesDTO;
    }

    public EntityDTO entityDTOWithLazyObjects(Entity entity) {
        return abstractDTOService.convertWithLazyObjects(entity);
    }

    public EntityDTO entityDTOWithLazyObjects(Integer entityId) {
        return entityDTOWithLazyObjects(entityWithLazyObjects(entityId));
    }

    public List<EntityDTO> entityDTOWithLazyObjects(List<Entity> entities) {
        List<EntityDTO> entitiesDTO = new ArrayList<>();

        for (Entity entity : entities) {
            entitiesDTO.add(entityDTOWithLazyObjects(entity));
        }

        return entitiesDTO;
    }

    public EntityDTO entityDTOWithLazyObjectsReload(Entity entity) {
        return entityDTOWithLazyObjects(entityWithLazyObjects(entity));
    }

    public List<EntityDTO> entityDTOWithLazyObjectsReload(List<Entity> entities) {
        List<EntityDTO> entitiesDTO = new ArrayList<>();

        for (Entity entity : entities) {
            entitiesDTO.add(entityDTOWithLazyObjectsReload(entityWithLazyObjects(entity)));
        }

        return entitiesDTO;
    }

    public EntityDTO entityDTODefByLazy(Entity entity, boolean isLazy) {
        if(!isLazy)
            return entityDTOWithLazyObjectsReload(entity);
        else
            return entityDTO(entity);
    }

    public EntityDTO entityDTODefByLazy(Integer entityId, boolean isLazy) {
        if(!isLazy)
            return entityDTOWithLazyObjectsReload(entityWithLazyObjects(entityId));
        else
            return entityDTO(entity(entityId));
    }

    public List<EntityDTO> entityDTODefByLazy(List<Entity> entities, boolean isLazy) {
        if(!isLazy)
            return entityDTOWithLazyObjectsReload(entities);
        else
            return entityDTO(entities);
    }

    private <T> Integer entityReflectionId(T entity) {
        try {
            return (Integer) entity.getClass().getDeclaredMethod(GET_ID).invoke(entity);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            throw new FireRuntimeException(ex);
        }
    }
}
