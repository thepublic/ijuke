/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.services.BankService;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

@Controller
@RequestMapping(value = {"/database/bank"})
public class BankController extends AbstractDTOController<Bank, BankDTO> {

    @Autowired
    private BankService bankService;

    @RequestMapping(method = RequestMethod.GET, params = "name")
    @ResponseBody
    public BankDTO getEntityDTO(@RequestParam(value = "name") String name,
                                @RequestParam(value = "lazy", defaultValue = "true") Boolean isLazy) {
        return utilDTO.entityDTODefByLazy(bankService.findBankByName(name), isLazy);
    }
}
