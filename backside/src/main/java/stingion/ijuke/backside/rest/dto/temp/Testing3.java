package stingion.ijuke.backside.rest.dto.temp;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;

/**
 * Todo: to be deleted after all
 *
 * Created by ievgen on 8/23/14 7:14 PM.
 */
public class Testing3 {
    public static void main(String[] args) throws InterruptedException, IOException {
//        final ApplicationContext appContext =
//                new ClassPathXmlApplicationContext("META-INF/stingion/ijuke/backside/spring/contexts-bundle.xml");
//
//        BankDTOService bankService = appContext.getBean("bankDTOService", BankDTOService.class);

        ClientConfig config = new DefaultClientConfig();

        String username = "admin";
        String testPass = "nimda";

        Client client = Client.create(config);
        //Add authentication
        client.addFilter(new HTTPBasicAuthFilter(username, testPass));

        WebResource service = client.resource(getBaseURI());

        String input = "{\n" +
                "  \"id\":25,\n" +
                "  \"firstName\":\"Jen\",\n" +
                "  \"middleName\":\"Miles\",\n" +
                "  \"lastName\":\"Ballou\",\n" +
                "  \"email\":\"jb111@isystem.com\",\n" +
                "  \"phoneNumber\":\"0725556493\",\n" +
                "  \"birthDay\":399499200000,\n" +
                "  \"creditCards\":null\n" +
                "}";

        String input2 = "{\n" +
                "  \"id\":25,\n" +
                "  \"firstName\":\"Jen\",\n" +
                "  \"middleName\":\"Miles\",\n" +
                "  \"lastName\":\"Ballou\",\n" +
                "  \"email\":\"jb111@isystem.com\",\n" +
                "  \"phoneNumber\":\"0725556497\",\n" +
                "  \"birthDay\":399499200000,\n" +
                "  \"creditCards\":null\n" +
                "}";

        ClientResponse response = service.path("/contact/save").type("application/json").accept(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, input);

        System.out.println(response);

        Serializable id = (Serializable) new ObjectMapper().readValue(response.getEntity(String.class), Serializable.class);
        System.out.println(id);

        /*
        TimeUnit.SECONDS.sleep(5);

        response = service.path("/contact/saveOrUpdate").type("application/json")
                .post(ClientResponse.class, input2);

        System.out.println(response);
        */


    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8081/rest/database").build();
    }
}
