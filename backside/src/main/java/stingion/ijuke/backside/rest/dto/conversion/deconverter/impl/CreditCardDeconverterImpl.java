/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.CreditCardDeconverter;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;

@Service("creditCardDeconverter")
public class CreditCardDeconverterImpl extends AbstractDeconverterImpl<CreditCardDTO, CreditCard> implements CreditCardDeconverter {
}
