/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.Deconverter;
import stingion.ijuke.backside.rest.dto.conversion.utils.ConversionUtil;
import stingion.ijuke.utils.exception.FireRuntimeException;

import java.lang.reflect.ParameterizedType;

public class AbstractDeconverterImpl<EntityDTO, Entity> implements Deconverter<EntityDTO, Entity> {//NOSONAR

    @Autowired
    @Qualifier("conversionUtilFromDTO")
    private ConversionUtil convertUtil;

    private Class<Entity> entityClass;

    private Class<Entity> entityDtoClass;

    @Override
    public Entity deconvert(EntityDTO entityDTO) {
        try {
            return (Entity) convertUtil.convert(entityDTO, getEntityClass().newInstance());
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new FireRuntimeException(ex);
        }
    }

    public Class<Entity> getEntityDTOClass() {
        if (entityClass == null) {
            ParameterizedType pType = (ParameterizedType) getClass().getGenericSuperclass();
            this.entityClass = (Class<Entity>) pType.getActualTypeArguments()[0];
        }

        return entityClass;
    }

    public Class<Entity> getEntityClass() {
        if (entityDtoClass == null) {
            ParameterizedType pType = (ParameterizedType) getClass().getGenericSuperclass();
            this.entityDtoClass = (Class<Entity>) pType.getActualTypeArguments()[1];
        }

        return entityDtoClass;
    }
}
