package stingion.ijuke.backside.rest.controller.open.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
* Created under not commercial project
*/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Cortege {
    private String title;//NOSONAR
    private String content;//NOSONAR
}
