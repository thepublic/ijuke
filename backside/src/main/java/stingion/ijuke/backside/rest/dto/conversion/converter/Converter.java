/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter;

public interface Converter<T, DtoT> {//NOSONAR

    /**
     * Get DTO Entity without lazy elements with conversion depth 1
     *
     * @param entity Entity for conversion
     * @return DTO Entity
     */
    DtoT convert(T entity);

    /**
     * Get DTO Entity with lazy element with parametrize depth
     *
     * If depth 0 - get only Simple Type (Integer, Double, String, etc.) conversion
     * for top Entity instance
     *
     * @param entity Entity for conversion
     * @param depth depth of penetration for DTO conversion
     * @return DTO Entity
     */
    DtoT convert(T entity, int depth);

    /**
     * Get DTO Entity without lazy elements with conversion depth 1
     *
     * @param entity Entity for conversion
     * @return DTO Entity
     */
    DtoT convertWithLazyObjects(T entity);

    /**
     * Get DTO Entity with lazy element with parametrize depth
     *
     * If depth 0 - get only Simple Type (Integer, Double, String, etc.) conversion
     * for top Entity instance
     *
     * @param entity Entity for conversion
     * @param depth depth of penetration for DTO conversion
     * @return DTO Entity
     */
    DtoT convertWithLazyObjects(T entity, int depth);

    /**
     * Get DTO Entity
     *
     * See also: {@link #convert(Object)}, {@link #convert(Object, int)}, {@link #convertWithLazyObjects(Object)},
     * {@link #convertWithLazyObjects(Object, int)}
     *
     * @param entity Entity for conversion
     * @param depth depth of penetration for DTO conversion
     * @param isLazy if {@code true} then lazy objects will loaded
     * @return DTO Entity
     */
    DtoT convert(T entity, int depth, boolean isLazy);
}
