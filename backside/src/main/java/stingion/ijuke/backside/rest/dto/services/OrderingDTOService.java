/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.services;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

@Service("orderingDTOService")
public class OrderingDTOService extends AbstractDTOService<Ordering, OrderingDTO> {
}
