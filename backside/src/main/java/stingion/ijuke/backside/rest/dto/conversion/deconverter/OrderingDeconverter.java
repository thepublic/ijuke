/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter;

import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

public interface OrderingDeconverter extends Deconverter<OrderingDTO, Ordering> {
}
