/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.SupplierDeconverter;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

@Service("supplierDeconverter")
public class SupplierDeconverterImpl extends AbstractDeconverterImpl<SupplierDTO, Supplier> implements SupplierDeconverter {
}
