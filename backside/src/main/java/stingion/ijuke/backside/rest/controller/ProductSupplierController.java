/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.ProductService;
import stingion.ijuke.backside.data.services.ProductSupplierService;
import stingion.ijuke.backside.data.services.SupplierService;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;
import stingion.ijuke.utils.useful.Reflection;

import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.util.List;

@Controller
@RequestMapping(value = {"/database/productSupplier"})
public class ProductSupplierController extends AbstractDTOController<ProductSupplier, ProductSupplierDTO> {

    @Autowired
    private ProductSupplierService productSupplierService;

    @Autowired
    private ProductService productService;

    @Autowired
    private SupplierService supplierService;

    @RequestMapping(method = RequestMethod.GET, params = {"productId", "supplierId"})
    @ResponseBody
    public ProductSupplierDTO getEntityByProductIdAndSupplierId(
            @RequestParam(value = "productId") Integer productId,
            @RequestParam(value = "supplierId") Integer supplierId) {

        Product product = productService.getEntityById(productId);
        Supplier supplier = supplierService.getEntityById(supplierId);

        return utilDTO.entityDTO(productSupplierService.getEntityByProductAndSupplier(product, supplier));
    }

    /**
     * Forbidden using
     */
    public ProductSupplierDTO getEntityDTO(Integer id, Boolean isLazy) {
        throw new UnsupportedOperationException();
    }

    /**
     * Always return lazy result
     */
    @ResponseBody
    public List<ProductSupplierDTO> getAllEntitiesDTO(Boolean isLazy) {
        return utilDTO.entityDTO(productSupplierService.getEntities());
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteEntityDTO(@RequestBody ProductSupplierDTO entityDTO) {
        Serializable id = Reflection.invokeMethodByName(utilDTO.entity(entityDTO), "getPk");
        productSupplierService.deleteEntity(productSupplierService.getEntityById(id));
    }

    @RequestMapping(value = "update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    @ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
    public void updateEntityDTO(@RequestBody ProductSupplierDTO productSupplierDTO) {
        //Not considered for the entity
    }

    @RequestMapping(value = "saveOrUpdate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    @ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
    public void saveOrUpdateEntityDTO(@RequestBody ProductSupplierDTO productSupplierDTO) {
        //Not considered for the entity
    }
}
