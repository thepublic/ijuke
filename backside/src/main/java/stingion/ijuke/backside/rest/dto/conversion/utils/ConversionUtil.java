/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.utils;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;
import org.hibernate.LazyInitializationException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import stingion.ijuke.utils.exception.FireRuntimeException;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

public class ConversionUtil {

    public enum ConvertDirect {
        TO_DTO, FROM_DTO
    }

    class NoAppropriateConversionTypeException extends RuntimeException {
        NoAppropriateConversionTypeException(String message) {
            super(message);
        }
    }

    final Logger logger = Logger.getLogger(ConversionUtil.class);

    @Getter
    @Setter
    private ConvertDirect direction;

    @Getter
    @Setter
    private Set<Class<? extends Serializable>> simpleTypes;

    @Getter
    @Setter
    private Map<Class<?>, Class<?>> mapper;

    @Contract(value = "null -> fail", pure = true)
    private boolean isProcessSimpleTypes(@NotNull Class<?> inputClass) {
        for (Class clazz : simpleTypes) {
            if (clazz.isAssignableFrom(inputClass))
                return true;
        }

        return false;
    }

    /**
     * Used when convert from EntityDTO ot Entity (depth and lazyLoad don't matter)
     * <p>
     * default depth is {@code -1}; default lazyLoad is {@code false};
     * </p>
     */
    public <T, C> C convert(T t, C c) {
        final int defaultDeep = -1;
        final boolean defaultLazyLoad = false;
        return convert(t, c, defaultDeep, defaultLazyLoad);
    }

    /**
     * Used when convert from Entity to EntityDTO (depth and lazy are important)
     */
    @Contract("null, _, _ -> null")
    public <T, C> C convert(T t, C c, int deep, boolean lazyLoad) {//NOSONAR

        //NOSONAR Todo: maybe it's good to enter Null Object
        if (t == null || direction == ConvertDirect.TO_DTO && deep < 0)
            return null;

        try {
            for (Field field : t.getClass().getDeclaredFields()) {//NOSONAR
                Method currentTGetMethod;
                Method currentConTSetMethod;
                Class<?> param;
                if ((currentTGetMethod = getGetMethodByField(t.getClass(), field.getName())) != null
                        && getGetMethodByField(c.getClass(), field.getName()) != null
                        && getSetMethodByField(t.getClass(), field.getName(), field.getType()) != null) {
                    //Simple type field
                    if ((currentConTSetMethod = //NOSONAR
                            getSetMethodByField(c.getClass(), field.getName(), field.getType())) != null
                            && isProcessSimpleTypes(currentConTSetMethod.getParameterTypes()[0])) {

                        invokeConTSetMethodByTGetMethod(
                                t,
                                currentTGetMethod,
                                c,
                                currentConTSetMethod
                        );

                        continue;
                    }

                    //Set of DTOs
                    if ((currentConTSetMethod = //NOSONAR
                            getSetMethodByField(c.getClass(), field.getName(), field.getType())) != null
                            && Collection.class.isAssignableFrom(currentConTSetMethod.getParameterTypes()[0])) {

                        Collection tCollection = (Collection) currentTGetMethod.invoke(t);

                        //If list is lazy
                        try {//NOSONAR
                            tCollection.iterator().next().getClass();
                        } catch (NullPointerException | LazyInitializationException | NoSuchElementException ex) {
                            logger.error(ex);
                            currentConTSetMethod.invoke(c, new Object[]{null});
                            continue;
                        }

                        if (mapper.containsKey(tCollection.iterator().next().getClass())) {
                            if (direction == ConvertDirect.TO_DTO && lazyLoad) {
                                currentConTSetMethod.invoke(c, new Object[]{null});
                                continue;
                            }

                            Collection collectionDTO = Set.class.isAssignableFrom(tCollection.getClass()) ? newHashSet() :
                                    (List.class.isAssignableFrom(tCollection.getClass()) ? newArrayList() : null);

                            if (collectionDTO != null) {
                                for (Object element : tCollection) {
                                    collectionDTO.add(convert(element, mapper.get(element.getClass()).newInstance(),
                                            deep - 1, lazyLoad));
                                }
                            }

                            currentConTSetMethod.invoke(c, collectionDTO);
                        } else {
                            logger.warn("Intro type", new NoAppropriateConversionTypeException(tCollection.iterator()
                                    .next().getClass().toString()));
                        }

                        continue;
                    }

                    param = mapper.get(field.getType());
                    //DTO type field
                    if ((currentConTSetMethod = getSetMethodByField(c.getClass(),//NOSONAR
                            field.getName(),
                            param)) != null) {

                        // If change "deep - 1" to "deep" all DTO objects will loaded, except collections
                        currentConTSetMethod.invoke(c,
                                convert(currentTGetMethod.invoke(t),
                                        param.newInstance(), deep, lazyLoad));
                    } else if (param == null) {
                        logger.warn("Intro type", new NoAppropriateConversionTypeException(field.getType().toString()));
                    }
                }
            }

            return c;
        } catch (InstantiationException | IllegalAccessException |
                InvocationTargetException ex) {

            throw new FireRuntimeException(ex);
        }
    }


    private <T, C> void invokeConTSetMethodByTGetMethod(T t, Method tMethod, C c, Method conTMethod) {
        try {
            conTMethod.invoke(c, tMethod.invoke(t));
        } catch (IllegalAccessException | InvocationTargetException ex) {
            throw new FireRuntimeException(ex);
        }
    }

    private Method getGetMethodByField(Class<?> clazz, String fieldName) {
        return getMethod(clazz, "get" + WordUtils.capitalize(fieldName));
    }

    private Method getSetMethodByField(Class<?> clazz, String fieldName, Class<?> param) {
        return getMethod(clazz, "set" + WordUtils.capitalize(fieldName), param);
    }

    @Nullable
    private Method getMethod(Class<?> clazz, String methodName, Class<?>... params) {
        try {
            return clazz.getDeclaredMethod(methodName, params);
        } catch (NoSuchMethodException e) {
            logger.error(e);
            return null;
        }
    }
}
