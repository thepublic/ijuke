/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Message;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.MessageDeconverter;
import stingion.ijuke.utils.ijuke.dto.MessageDTO;

@Service("messageDeconverter")
public class MessageDeconverterImpl extends AbstractDeconverterImpl<MessageDTO, Message> implements MessageDeconverter {
}
