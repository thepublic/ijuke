/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

@Controller
@RequestMapping(value = {"/database/supplier"})
public class SupplierController extends AbstractDTOController<Supplier, SupplierDTO> {
}
