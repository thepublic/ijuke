/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter;

public interface Deconverter<EntityDTO, Entity> {//NOSONAR

    Entity deconvert(EntityDTO entityDTO);
}
