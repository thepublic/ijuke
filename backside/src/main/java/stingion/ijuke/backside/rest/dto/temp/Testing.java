package stingion.ijuke.backside.rest.dto.temp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.BankService;
import stingion.ijuke.backside.data.services.ContactService;
import stingion.ijuke.backside.data.services.CreditCardService;
import stingion.ijuke.backside.data.services.OrderingService;
import stingion.ijuke.backside.data.services.ProductService;
import stingion.ijuke.backside.data.services.ProductSupplierService;
import stingion.ijuke.backside.data.services.SupplierService;
import stingion.ijuke.backside.rest.dto.conversion.converter.BankConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.ContactConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.CreditCardConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.OrderingConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.ProductConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.ProductSupplierConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.SupplierConverter;

/**
 * Todo: to be deleted after all
 *
 * Created by ievgen on 8/23/14 7:14 PM.
 */
public class Testing {
    public static void main(String[] args) {
        ApplicationContext appContext =
                new ClassPathXmlApplicationContext("META-INF/stingion/ijuke/backside/spring/contexts-bundle.xml");
        BankService bankService = appContext.getBean("bankService", BankService.class);
        ContactService contactService = appContext.getBean("contactService", ContactService.class);
        CreditCardService creditCardService = appContext.getBean("creditCardService", CreditCardService.class);
        ProductService productService = appContext.getBean("productService", ProductService.class);
        OrderingService orderingService = appContext.getBean("orderingService", OrderingService.class);
        SupplierService supplierService = appContext.getBean("supplierService", SupplierService.class);
        ProductSupplierService productSupplierService = appContext.getBean("productSupplierService",
                ProductSupplierService.class);

        BankConverter bankConverter = appContext.getBean("bankConverter", BankConverter.class);
        ContactConverter contactConverter = appContext.getBean("contactConverter", ContactConverter.class);
        CreditCardConverter creditCardConverter = appContext.getBean("creditCardConverter", CreditCardConverter.class);
        OrderingConverter orderingConverter = appContext.getBean("orderingConverter", OrderingConverter.class);
        ProductConverter productConverter = appContext.getBean("productConverter", ProductConverter.class);
        SupplierConverter supplierConverter = appContext.getBean("supplierConverter", SupplierConverter.class);
        ProductSupplierConverter productSupplierConverter = appContext.getBean("productSupplierConverter",
                                                                                ProductSupplierConverter.class
                                                                               );

        Bank bank = bankService.getEntityWithLazyObjectsById(1);
        Contact contact = contactService.getEntityWithLazyObjectsById(1);
        CreditCard creditCard = creditCardService.getEntityWithLazyObjectsById(1);
        Ordering ordering = orderingService.getEntityWithLazyObjectsById(1);
        Product product = productService.getEntityWithLazyObjectsById(1);
        Supplier supplier = supplierService.getEntityWithLazyObjectsById(1);
        ProductSupplier productSupplier = productSupplierService.getEntities().get(1);

        System.out.println(bankConverter.convert(bank));
        System.out.println(bankConverter.convertWithLazyObjects(bank));

        System.out.println(contactConverter.convert(contact));
        System.out.println(contactConverter.convertWithLazyObjects(contact));

        System.out.println(creditCardConverter.convert(creditCard));
        System.out.println(creditCardConverter.convertWithLazyObjects(creditCard));

        System.out.println(orderingConverter.convert(ordering));
        System.out.println(orderingConverter.convertWithLazyObjects(ordering));

        System.out.println(productConverter.convert(product));
        System.out.println(productConverter.convertWithLazyObjects(product));

        System.out.println(supplierConverter.convert(supplier));
        System.out.println(supplierConverter.convertWithLazyObjects(supplier));

        System.out.println(productSupplierConverter.convert(productSupplier));
        System.out.println(productSupplierConverter.convertWithLazyObjects(productSupplier));
    }
}
