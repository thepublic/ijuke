package stingion.ijuke.backside.rest.controller.open.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.OncePerRequestFilter;
import stingion.ijuke.backside.rest.controller.open.model.CORSAllowedService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/* //NOSONAR Todo:
 1) Inject spring context in filter
 2) Create Options request with success in Ajax request
*/
public class CORSFilter extends OncePerRequestFilter {

    @Autowired
    CORSAllowedService corsAllowedService;
    
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        
        String originHeader = request.getHeader("origin");
        if (originHeader != null && "OPTIONS".equals(request.getMethod())) {
            // CORS "pre-flight" request
            if (corsAllowedService.isAllowed(originHeader)) {
                response.addHeader("Access-Control-Allow-Origin", originHeader);
            }            
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            response.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, x-requested-with, sid, mycustom, smuser");
            response.addHeader("Access-Control-Max-Age", "1800");//30 min
        }
        filterChain.doFilter(request, response);
    }
}