/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import stingion.ijuke.backside.data.model.Folder;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;

@Controller
@RequestMapping(value = {"/database/folder"})
public class FolderController extends AbstractDTOController<Folder, FolderDTO> {
}
