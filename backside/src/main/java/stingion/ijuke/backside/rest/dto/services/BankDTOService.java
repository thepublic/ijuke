/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.services;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

@Service("bankDTOService")
public class BankDTOService extends AbstractDTOService<Bank, BankDTO> {
}
