/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.services.ContactService;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

@Controller
@RequestMapping(value = {"/database/contact"/*//NOSONAR TODO: Create class with mapping values*/})
public class ContactController extends AbstractDTOController<Contact, ContactDTO> {

    @Autowired
    private ContactService contactService;

    @RequestMapping(method = RequestMethod.GET, params = "email")
    @ResponseBody
    public ContactDTO getEntityDTO(@RequestParam(value = "email") String email,
                                   @RequestParam(value = "lazy", defaultValue = "true") Boolean isLazy) {
        return utilDTO.entityDTODefByLazy(contactService.findContactByEmail(email), isLazy);
    }
}
