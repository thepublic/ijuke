/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter;

import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;

public interface CreditCardConverter extends Converter<CreditCard, CreditCardDTO> {
}
