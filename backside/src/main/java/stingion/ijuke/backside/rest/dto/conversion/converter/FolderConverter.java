/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter;

import stingion.ijuke.backside.data.model.Folder;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;

public interface FolderConverter extends Converter<Folder, FolderDTO> {
}
