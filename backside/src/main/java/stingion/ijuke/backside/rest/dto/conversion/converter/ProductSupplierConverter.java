/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter;

import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;

public interface ProductSupplierConverter extends Converter<ProductSupplier, ProductSupplierDTO> {
}
