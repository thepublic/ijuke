/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter;

import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;

public interface ProductConverter extends Converter<Product, ProductDTO> {
}
