/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import stingion.ijuke.backside.data.services.AbstractService;
import stingion.ijuke.backside.rest.controller.utils.UtilDTO;
import stingion.ijuke.utils.useful.Reflection;

import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.util.List;

public class AbstractDTOController<Entity, EntityDTO> {//NOSONAR

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private AbstractService<Entity> abstractService;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected UtilDTO<Entity, EntityDTO> utilDTO;

    @RequestMapping(method = RequestMethod.GET, params = "id")
    @ResponseBody
    public EntityDTO getEntityDTO(@RequestParam(value = "id") Integer id,
                                  @RequestParam(value = "lazy", defaultValue = "true") Boolean isLazy) {
        return utilDTO.entityDTODefByLazy(id, isLazy);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<EntityDTO> getAllEntitiesDTO(@RequestParam(value = "lazy", defaultValue = "true")
                                             Boolean isLazy) {
        return utilDTO.entityDTODefByLazy(abstractService.getEntities(), isLazy);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    @ResponseStatus(value = HttpStatus.OK)
    public void updateEntityDTO(@RequestBody EntityDTO entityDTO) {
        Entity entitySync = abstractService.mergeEntity(utilDTO.entity(entityDTO));
        abstractService.updateEntity(entitySync);
    }

    @RequestMapping(value = "save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    @ResponseBody
    public Serializable saveEntityDTO(@RequestBody EntityDTO entityDTO) {
        return abstractService.createNewEntity(utilDTO.entity(entityDTO));
    }

    @RequestMapping(value = "saveOrUpdate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    @ResponseStatus(value = HttpStatus.OK)
    public void saveOrUpdateEntityDTO(@RequestBody EntityDTO entityDTO) {
        Entity entitySync = abstractService.getEntityById(Reflection.invokeGetId(entityDTO));
        if (entitySync != null)
            abstractService.saveOrUpdateEntity(entitySync);
        else
            abstractService.saveOrUpdateEntity(utilDTO.entity(entityDTO));
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteEntityDTO(@RequestBody EntityDTO entityDTO) {
        Serializable id = Reflection.invokeGetId(utilDTO.entity(entityDTO));
        abstractService.deleteEntity(abstractService.getEntityById(id));
    }
}
