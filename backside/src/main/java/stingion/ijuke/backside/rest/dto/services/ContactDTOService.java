/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.services;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

@Service("contactDTOService")
public class ContactDTOService extends AbstractDTOService<Contact, ContactDTO> {
}
