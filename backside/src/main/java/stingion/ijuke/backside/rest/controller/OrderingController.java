/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

@Controller
@RequestMapping(value = {"/database/ordering"})
public class OrderingController extends AbstractDTOController<Ordering, OrderingDTO> {
}
