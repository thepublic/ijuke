/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.services;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Message;
import stingion.ijuke.utils.ijuke.dto.MessageDTO;

@Service("messageDTOService")
public class MessageDTOService extends AbstractDTOService<Message, MessageDTO> {
}
