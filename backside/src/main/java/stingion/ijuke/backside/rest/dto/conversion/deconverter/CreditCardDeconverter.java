/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter;

import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;

public interface CreditCardDeconverter extends Deconverter<CreditCardDTO, CreditCard> {
}
