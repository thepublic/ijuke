/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter;

import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

public interface SupplierConverter extends Converter<Supplier, SupplierDTO> {
}
