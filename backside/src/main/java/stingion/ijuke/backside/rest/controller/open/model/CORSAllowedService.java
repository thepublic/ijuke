package stingion.ijuke.backside.rest.controller.open.model;

import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;

/**
 * Created under not commercial project
 */
public interface CORSAllowedService {
    boolean isAllowed(String origin);

    HttpHeaders checkAndGetHeaders(HttpServletRequest request);
}
