/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter;

import stingion.ijuke.backside.data.model.Folder;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;

public interface FolderDeconverter extends Deconverter<FolderDTO, Folder> {
}
