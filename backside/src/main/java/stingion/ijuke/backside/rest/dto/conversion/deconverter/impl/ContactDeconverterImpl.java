/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.ContactDeconverter;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

@Service("contactDeconverter")
public class ContactDeconverterImpl extends AbstractDeconverterImpl<ContactDTO, Contact> implements ContactDeconverter {
}
