/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.OrderingDeconverter;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

@Service("orderingDeconverter")
public class OrderingDeconverterImpl extends AbstractDeconverterImpl<OrderingDTO, Ordering> implements OrderingDeconverter {
}
