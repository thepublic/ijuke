/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter;

import stingion.ijuke.backside.data.model.Message;
import stingion.ijuke.utils.ijuke.dto.MessageDTO;

public interface MessageConverter extends Converter<Message, MessageDTO> {
}
