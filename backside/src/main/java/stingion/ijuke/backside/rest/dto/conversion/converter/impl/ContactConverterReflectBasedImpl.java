/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.rest.dto.conversion.converter.ContactConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.impl.AbstractConverterReflectBasedImpl;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

@Service("contactConverter")
public class ContactConverterReflectBasedImpl extends AbstractConverterReflectBasedImpl<Contact, ContactDTO> implements ContactConverter {
}
