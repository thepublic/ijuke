/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import stingion.ijuke.backside.data.model.Message;
import stingion.ijuke.utils.ijuke.dto.MessageDTO;

@Controller
@RequestMapping(value = {"/database/message"})
public class MessageController extends AbstractDTOController<Message, MessageDTO> {
}
