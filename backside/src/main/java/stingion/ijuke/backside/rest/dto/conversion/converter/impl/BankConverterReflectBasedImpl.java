/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.rest.dto.conversion.converter.BankConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.impl.AbstractConverterReflectBasedImpl;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

@Service("bankConverter")
public class BankConverterReflectBasedImpl extends AbstractConverterReflectBasedImpl<Bank, BankDTO> implements BankConverter {
}
