/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller.utils;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.model.Folder;
import stingion.ijuke.backside.data.model.Message;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.utils.ijuke.dto.BankDTO;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;
import stingion.ijuke.utils.ijuke.dto.MessageDTO;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

@Configuration
public class UtilDTOComponents {//NOSONAR

    @Component
    public static class BankUtilDTO extends UtilDTO<Bank, BankDTO> {
    }

    @Component
    public static class ContactUtilDTO extends UtilDTO<Contact, ContactDTO> {
    }

    @Component
    public static class CreditCardUtilDTO extends UtilDTO<CreditCard, CreditCardDTO> {
    }

    @Component
    public static class OrderingUtilDTO extends UtilDTO<Ordering, OrderingDTO> {
    }

    @Component
    public static class ProductUtilDTO extends UtilDTO<Product, ProductDTO> {
    }

    @Component
    public static class SupplierUtilDTO extends UtilDTO<Supplier, SupplierDTO> {
    }

    @Component
    public static class ProductSupplierUtilDTO extends UtilDTO<ProductSupplier, ProductSupplierDTO> {
    }

    @Component
    public static class MessageUtilDTO extends UtilDTO<Message, MessageDTO> {
    }

    @Component
    public static class FolderUtilDTO extends UtilDTO<Folder, FolderDTO> {
    }
}
