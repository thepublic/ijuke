/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter;

import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

public interface ContactConverter extends Converter<Contact, ContactDTO> {
}
