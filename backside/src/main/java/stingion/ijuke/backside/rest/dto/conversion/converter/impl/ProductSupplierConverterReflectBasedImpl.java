/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.rest.dto.conversion.converter.ProductSupplierConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.impl.AbstractConverterReflectBasedImpl;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;

@Service("productSupplierConverter")
public class ProductSupplierConverterReflectBasedImpl extends AbstractConverterReflectBasedImpl<ProductSupplier, ProductSupplierDTO>
                                                      implements ProductSupplierConverter {
}
