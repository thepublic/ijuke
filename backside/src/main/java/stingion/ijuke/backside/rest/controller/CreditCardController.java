/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;

@Controller
@RequestMapping(value = {"/database/creditCard"})
public class CreditCardController extends AbstractDTOController<CreditCard, CreditCardDTO> {
}
