/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter;

import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

public interface OrderingConverter extends Converter<Ordering, OrderingDTO> {
}
