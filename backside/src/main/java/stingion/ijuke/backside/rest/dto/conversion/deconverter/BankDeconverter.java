/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter;

import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

public interface BankDeconverter extends Deconverter<BankDTO, Bank> {
}
