/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Folder;
import stingion.ijuke.backside.rest.dto.conversion.converter.FolderConverter;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;

@Service("folderConverter")
public class FolderConverterReflectBasedImpl extends AbstractConverterReflectBasedImpl<Folder, FolderDTO> implements FolderConverter {
}
