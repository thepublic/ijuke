/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.rest.dto.conversion.converter.CreditCardConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.impl.AbstractConverterReflectBasedImpl;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;

@Service("creditCardConverter")
public class CreditCardConverterReflectBasedImpl extends AbstractConverterReflectBasedImpl<CreditCard, CreditCardDTO> implements CreditCardConverter {
}
