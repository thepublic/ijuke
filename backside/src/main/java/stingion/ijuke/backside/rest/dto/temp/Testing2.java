package stingion.ijuke.backside.rest.dto.temp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.BankService;
import stingion.ijuke.backside.data.services.ContactService;
import stingion.ijuke.backside.data.services.CreditCardService;
import stingion.ijuke.backside.data.services.OrderingService;
import stingion.ijuke.backside.data.services.ProductService;
import stingion.ijuke.backside.data.services.ProductSupplierService;
import stingion.ijuke.backside.data.services.SupplierService;
import stingion.ijuke.backside.rest.dto.services.AbstractDTOService;
import stingion.ijuke.backside.rest.dto.services.BankDTOService;
import stingion.ijuke.backside.rest.dto.services.ContactDTOService;
import stingion.ijuke.backside.rest.dto.services.CreditCardDTOService;
import stingion.ijuke.backside.rest.dto.services.OrderingDTOService;
import stingion.ijuke.backside.rest.dto.services.ProductDTOService;
import stingion.ijuke.backside.rest.dto.services.ProductSupplierDTOService;
import stingion.ijuke.backside.rest.dto.services.SupplierDTOService;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Todo: to be deleted after all
 *
 * Created by ievgen on 8/23/14 7:14 PM.
 */
public class Testing2 {
    public static void main(String[] args) {
        final ApplicationContext appContext =
                new ClassPathXmlApplicationContext("META-INF/stingion/ijuke/backside/spring/contexts-bundle.xml");
        BankService bankService = appContext.getBean("bankService", BankService.class);
        ContactService contactService = appContext.getBean("contactService", ContactService.class);
        CreditCardService creditCardService = appContext.getBean("creditCardService", CreditCardService.class);
        ProductService productService = appContext.getBean("productService", ProductService.class);
        OrderingService orderingService = appContext.getBean("orderingService", OrderingService.class);
        SupplierService supplierService = appContext.getBean("supplierService", SupplierService.class);
        ProductSupplierService productSupplierService = appContext.getBean("productSupplierService",
                ProductSupplierService.class);

        final Bank bank = bankService.getEntityWithLazyObjectsById(1);
        final Contact contact = contactService.getEntityWithLazyObjectsById(1);
        final CreditCard creditCard = creditCardService.getEntityWithLazyObjectsById(1);
        final Ordering ordering = orderingService.getEntityWithLazyObjectsById(1);
        final Product product = productService.getEntityWithLazyObjectsById(1);
        final Supplier supplier = supplierService.getEntityWithLazyObjectsById(1);
        final ProductSupplier productSupplier = productSupplierService.getEntities().get(1);

        BankDTOService bankDTOService = appContext.getBean("bankDTOService", BankDTOService.class);

        Map<? extends AbstractDTOService, Object> DTOServicesMap = new LinkedHashMap() {{
            put(appContext.getBean("bankDTOService", BankDTOService.class), bank);
            put(appContext.getBean("contactDTOService", ContactDTOService.class), contact);
            put(appContext.getBean("creditCardDTOService", CreditCardDTOService.class), creditCard);
            put(appContext.getBean("orderingDTOService", OrderingDTOService.class), ordering);
            put(appContext.getBean("productDTOService", ProductDTOService.class), product);
            put(appContext.getBean("supplierDTOService", SupplierDTOService.class), supplier);
            put(appContext.getBean("productSupplierDTOService", ProductSupplierDTOService.class), productSupplier);
        }};

        for (Object serv : DTOServicesMap.keySet()) {
            System.out.println("--------------------- " + serv.getClass().getSimpleName() + " ---------------------");
            System.out.println(((AbstractDTOService) serv).convert(DTOServicesMap.get(serv)));
            System.out.println(((AbstractDTOService) serv).convert(DTOServicesMap.get(serv), 2));
            System.out.println(((AbstractDTOService) serv).convert(DTOServicesMap.get(serv), 1, true));
            System.out.println(((AbstractDTOService) serv).convertWithLazyObjects(DTOServicesMap.get(serv)));
            System.out.println(((AbstractDTOService) serv).convertWithLazyObjects(DTOServicesMap.get(serv), 1));
        }
    }
}
