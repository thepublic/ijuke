/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.rest.dto.conversion.converter.ProductConverter;
import stingion.ijuke.backside.rest.dto.conversion.converter.impl.AbstractConverterReflectBasedImpl;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;

@Service("productConverter")
public class ProductConverterReflectBasedImpl extends AbstractConverterReflectBasedImpl<Product, ProductDTO>
                                              implements ProductConverter {
}
