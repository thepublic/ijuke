/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;

@Controller
@RequestMapping(value = {"/database/product"})
public class ProductController extends AbstractDTOController<Product, ProductDTO> {
}
