package stingion.ijuke.backside.rest.controller.open.controller;

import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import stingion.ijuke.backside.rest.controller.open.model.CORSAllowedService;
import stingion.ijuke.backside.rest.controller.open.model.Cortege;
import stingion.ijuke.utils.exception.FireRuntimeException;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * Created under not commercial project
 */
@RequestMapping(value = {"/open/projectDescription"})
public class ProjectDescriptionController {

    @Getter
    @Setter
    CORSAllowedService corsAllowedService;

    private File descJsonFile;

    public ProjectDescriptionController(String originsFileRelPath) {
        descJsonFile = new File(getClass().getClassLoader().getResource(originsFileRelPath).getFile());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Cortege> post(HttpServletRequest request) throws IOException {

        if (!corsAllowedService.isAllowed(request.getHeader("origin")))
            throw new FireRuntimeException("AJAX not allowed");

        Cortege cortege = new ObjectMapper().readValue(descJsonFile, Cortege.class);

        return new ResponseEntity<>(cortege, corsAllowedService.checkAndGetHeaders(request), HttpStatus.OK);
    }
}
