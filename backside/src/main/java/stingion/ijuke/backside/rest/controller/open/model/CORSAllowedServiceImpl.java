package stingion.ijuke.backside.rest.controller.open.model;

import lombok.NoArgsConstructor;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpHeaders;
import stingion.ijuke.utils.exception.FireRuntimeException;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * Created under not commercial project
 */
@NoArgsConstructor
public class CORSAllowedServiceImpl implements CORSAllowedService {

    private static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";

    private File originsJsonFile;

    public CORSAllowedServiceImpl(String originsFileRelPath) {
        originsJsonFile = new File(getClass().getClassLoader().getResource(originsFileRelPath).getFile());
    }

    @Override
    public boolean isAllowed(String origin) {
        Set<String> accessControlAllowedDomains = refreshAccessControlAllowedHosts();
        return accessControlAllowedDomains.contains("*") || accessControlAllowedDomains.contains(origin.toLowerCase());
    }

    @Override
    public HttpHeaders checkAndGetHeaders(HttpServletRequest request) {

        HttpHeaders responseHeaders = new HttpHeaders();
        String origin = request.getHeader("origin");

        if (isAllowed(origin))
            responseHeaders.set(ACCESS_CONTROL_ALLOW_ORIGIN, origin);

        return responseHeaders;
    }

    private Set<String> refreshAccessControlAllowedHosts() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(originsJsonFile,
                    mapper.getTypeFactory().constructCollectionType(Set.class, String.class));
        } catch (IOException ex) {
            throw new FireRuntimeException(ex);
        }
    }
}
