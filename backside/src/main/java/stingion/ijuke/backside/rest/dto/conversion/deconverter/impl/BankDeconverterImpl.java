/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.BankDeconverter;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

@Service("bankDeconverter")
public class BankDeconverterImpl extends AbstractDeconverterImpl<BankDTO, Bank> implements BankDeconverter {
}
