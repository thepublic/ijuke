/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.services;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;

@Service("productSupplierDTOService")
public class ProductSupplierDTOService extends AbstractDTOService<ProductSupplier, ProductSupplierDTO> {
}
