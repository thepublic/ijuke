/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.ProductDeconverter;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;

@Service("productDeconverter")
public class ProductDeconverterImpl extends AbstractDeconverterImpl<ProductDTO, Product> implements ProductDeconverter {
}
