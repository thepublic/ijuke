/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.services;

import org.springframework.beans.factory.annotation.Autowired;
import stingion.ijuke.backside.rest.dto.conversion.converter.Converter;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.Deconverter;

public class AbstractDTOService<Entity, EntityDTO> {//NOSONAR

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private Converter<Entity, EntityDTO> converter;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private Deconverter<EntityDTO, Entity> deconverter;

    /**
     * Get DTO Entity without lazy elements with conversion depth 1
     *
     *
     * @param entity Entity for conversion
     * @return DTO Entity
     */
    public EntityDTO convert(Entity entity) {
        return converter.convert(entity);
    }

    /**
     * Get DTO Entity without lazy elements with conversion depth 1
     *
     *
     * @param entity Entity for conversion
     * @return DTO Entity
     */
    public EntityDTO convertWithLazyObjects(Entity entity) {
        return converter.convertWithLazyObjects(entity);
    }

    /**
     * Get DTO Entity
     *
     * See also: {@link #convert(Object)}, {@link #convert(Object, int)}, {@link #convertWithLazyObjects(Object)},
     * {@link #convertWithLazyObjects(Object, int)}
     *
     *
     * @param entity Entity for conversion
     * @param depth depth of penetration for DTO conversion
     * @param isLazy if {@code true} then lazy objects will loaded
     * @return DTO Entity
     */
    public EntityDTO convert(Entity entity, int depth, boolean isLazy) {
        return converter.convert(entity, depth, isLazy);
    }

    /**
     * Get DTO Entity with lazy element with parametrize depth
     *
     * If depth 0 - get only Simple Type (Integer, Double, String, etc.) conversion
     * for top Entity instance
     *
     *
     * @param entity Entity for conversion
     * @param depth depth of penetration for DTO conversion
     * @return DTO Entity
     */
    public EntityDTO convertWithLazyObjects(Entity entity, int depth) {
        return converter.convertWithLazyObjects(entity, depth);
    }

    /**
     * Get DTO Entity with lazy element with parametrize depth
     *
     * If depth 0 - get only Simple Type (Integer, Double, String, etc.) conversion
     * for top Entity instance
     *
     *
     * @param entity Entity for conversion
     * @param depth depth of penetration for DTO conversion
     * @return DTO Entity
     */
    public EntityDTO convert(Entity entity, int depth) {
        return converter.convert(entity, depth);
    }

    /**
     * De-conversion of DTO Entity to Entity
     *
     * @param entityDTO entity for de-convert to database entity
     * @return database entity
     */
    public Entity deconvert(EntityDTO entityDTO) {
        return deconverter.deconvert(entityDTO);
    }
}
