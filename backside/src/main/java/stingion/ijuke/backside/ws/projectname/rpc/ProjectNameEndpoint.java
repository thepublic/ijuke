package stingion.ijuke.backside.ws.projectname.rpc;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 * Created under not commercial project
 *
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface ProjectNameEndpoint {
    
    String getProjectName();
}
