package stingion.ijuke.backside.ws.projectname.rpc;

import lombok.Getter;
import lombok.Setter;

import javax.jws.WebService;

/**
 * Created under not commercial project
 */
@WebService(endpointInterface = "stingion.ijuke.backside.ws.projectname.rpc.ProjectNameEndpoint")
public class ProjectNameEndpointImpl implements ProjectNameEndpoint {
    
    @Getter
    @Setter
    private String projectNameText;
    
    @Override
    public String getProjectName() {
        return projectNameText;
    }
}
