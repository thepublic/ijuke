package stingion.ijuke.backside.ws.helloproject.rpc;

import javax.activation.DataHandler;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 * Created under not commercial project
 */
@WebService
@SOAPBinding(style = Style.RPC)
@HandlerChain(file = "MacValidator-handler-chain.xml")
public interface HelloProjectEndpoint {

    String getHello();

    @WebMethod(operationName = "getHelloOverloaded")
    String getHello(String userName);

    String getHelloWithName(String userName);

    DataHandler downloadModuleImage();

    String uploadModuleImage(SoapFile soapFile);
}
