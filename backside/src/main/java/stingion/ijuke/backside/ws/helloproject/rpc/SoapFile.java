/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.ws.helloproject.rpc;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString(of = {"fileName"})
public class SoapFile {
    private String fileName;//NOSONAR
    private byte[] fileData;//NOSONAR
}
