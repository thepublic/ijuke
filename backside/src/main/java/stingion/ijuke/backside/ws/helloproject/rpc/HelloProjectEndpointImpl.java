package stingion.ijuke.backside.ws.helloproject.rpc;

import org.apache.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.MTOM;

/**
 * Created under not commercial project
 * //NOSONAR Todo: Refactor Test for it to be under Spring context
 *
 */
@WebService(endpointInterface = "stingion.ijuke.backside.ws.helloproject.rpc.HelloProjectEndpoint")
public class HelloProjectEndpointImpl implements HelloProjectEndpoint {

    final Logger logger = Logger.getLogger(HelloProjectEndpointImpl.class);

    public static final String MODULE_IMAGE_FILE_REL_PATH = "META-INF/stingion/ijuke/backside/multimedia/backside.jpg";

    private static final String PROJECT_GREETINGS = "Hello, {1}, this is \"backside\" module of \"ijuke\" project";

    @Override
    public String getHello() {
        return PROJECT_GREETINGS.replace(" {1},", "");
    }

    @Override
    public String getHello(String userName) {
        return getHelloWithName(userName);
    }

    @Override
    public String getHelloWithName(String userName) {
        return PROJECT_GREETINGS.replace("{1}", userName);
    }

    @MTOM
    @Override
    public DataHandler downloadModuleImage() {
        FileDataSource moduleImageDataSource =
                new FileDataSource(getClass().getClassLoader().getResource(MODULE_IMAGE_FILE_REL_PATH).getFile());
        return new DataHandler(moduleImageDataSource);
    }

    @MTOM
    @Override
    public String uploadModuleImage(SoapFile soapFile) {

        if (soapFile != null) {
            /*** The code fragment can be used for file saving ***
             FileOutputStream fos = new FileOutputStream("../tempdata/backside/" + soapFile.getFileName());
             fos.write(soapFile.getFileData());
             fos.flush();
             fos.close();
             System.out.println("Download Completed");

             */
            logger.info(soapFile + " is uploaded");
            return "Upload successful";
        }

        logger.warn(soapFile + " is uploaded failed!", new WebServiceException("Upload Failed!"));
        return "Upload Failed!";
    }
}
