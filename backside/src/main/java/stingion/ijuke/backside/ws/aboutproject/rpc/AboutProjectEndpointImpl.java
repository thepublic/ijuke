package stingion.ijuke.backside.ws.aboutproject.rpc;

import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;
import stingion.ijuke.utils.exception.FireRuntimeException;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.util.List;
import java.util.Map;

/**
 * Created under not commercial project
 */
@WebService(endpointInterface = "stingion.ijuke.backside.ws.aboutproject.rpc.AboutProjectEndpoint")
public class AboutProjectEndpointImpl implements AboutProjectEndpoint {

    static Logger logger = Logger.getLogger(AboutProjectEndpointImpl.class);

    public static final String ACCESS_USER = "TEST_USER";
    public static final String ACCESS_PASS = "TEST_PASS";

    @Resource
    @Setter
    @Getter
    WebServiceContext wsContext;

    private Map<String, String> creds = ImmutableMap.of(ACCESS_USER, ACCESS_PASS);

    @Override
    public String getAboutProject() {
        validation();
        return "backside module is a main back-end part of \"ijuke\" project";
    }

    public void validation() {
        MessageContext mctx = wsContext.getMessageContext();
        Map httpHeaders = (Map) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);

        try {
            String username = (String) ((List) httpHeaders.get("username")).get(0);
            String password = (String) ((List) httpHeaders.get("password")).get(0);

            if (creds.get(username).equals(password))
                return;
        } catch (Exception ex) {
            logger.error(ex);
        }

        throw new FireRuntimeException("Invalid access permission!");
    }
}
