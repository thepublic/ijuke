/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Set;

@Entity
@Table(name = "product", uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
@EqualsAndHashCode(of = "id")
@ToString(of = {"id", "name", "price"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "price")
    private Double price;

    @OneToMany(mappedBy = "product")
    private Set<Ordering> orderings;

    @OneToMany(mappedBy = "pk.product", cascade = CascadeType.ALL)
    private Set<ProductSupplier> productSuppliers;

    public Product(String name, Double price) {
        this.name = name;
        this.price = price;
    }
}
