/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services;

import stingion.ijuke.backside.data.model.Contact;

public interface ContactService extends AbstractService<Contact> {
    Contact findContactByEmail(String email);
}
