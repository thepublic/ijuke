/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao.impl;

import org.springframework.stereotype.Repository;
import stingion.ijuke.backside.data.dao.FolderDao;
import stingion.ijuke.backside.data.model.Folder;

@Repository("folderDao")
public class FolderDaoImpl extends AbstractDaoImpl<Folder> implements FolderDao {
}
