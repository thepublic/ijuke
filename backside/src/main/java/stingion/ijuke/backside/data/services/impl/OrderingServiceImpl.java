/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.data.services.OrderingService;

@Service("orderingService")
public class OrderingServiceImpl extends AbstractServiceImpl<Ordering> implements OrderingService {
}
