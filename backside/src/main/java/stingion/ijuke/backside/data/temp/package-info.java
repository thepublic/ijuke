/**
 * Created by ievgen on 8/23/14 7:13 PM.
 *
 * Todo: Temporary package and its classes for testing. Should be deleted
 */
@Note(record = "Temporary created for testing")
package stingion.ijuke.backside.data.temp;

import stingion.ijuke.utils.annotation.Note;