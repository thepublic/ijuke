/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services;

import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.Supplier;

import java.util.Set;

public interface SupplierService extends AbstractService<Supplier> {
    Supplier findSupplierByName(String name);

    /**
     * Get all products for current supplier
     */
    Set<Product> getProducts(Supplier supplier);
}
