/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services;

import stingion.ijuke.backside.data.model.Ordering;

public interface OrderingService extends AbstractService<Ordering> {
}
