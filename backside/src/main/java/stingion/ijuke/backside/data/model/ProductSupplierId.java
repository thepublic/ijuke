/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ProductSupplierId implements Serializable {

    private static final long serialVersionUID = -9120607274421816301L;

    @ManyToOne
    private Product product;

    @ManyToOne
    private Supplier supplier;
}
