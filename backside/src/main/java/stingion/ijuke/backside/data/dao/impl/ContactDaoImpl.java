/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import stingion.ijuke.backside.data.dao.ContactDao;
import stingion.ijuke.backside.data.model.Contact;

import java.util.List;

@Repository("contactDao")
public class ContactDaoImpl extends AbstractDaoImpl<Contact> implements ContactDao {

    @Override
    public Contact findByEmail(String email) {
        final String queryWithNativeSqlString = "SELECT * FROM contact c WHERE c.email = :inputEmail";

        Query q = getSession().createSQLQuery(queryWithNativeSqlString).addEntity(Contact.class);
        q.setParameter("inputEmail", email);
        List byEmailList = q.list();

        return (Contact) (!byEmailList.isEmpty() ? byEmailList.get(0) : null);
    }
}
