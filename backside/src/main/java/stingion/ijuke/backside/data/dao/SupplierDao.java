/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao;

import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.Supplier;

import java.util.Set;

public interface SupplierDao extends Dao<Supplier> {
    Supplier findByName(String name);

    Set<Product> getProducts(Supplier supplier);
}
