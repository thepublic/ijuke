/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services;

import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.model.CreditCard;

import java.util.List;

public interface CreditCardService extends AbstractService<CreditCard> {

    List<CreditCard> findCreditCardsByContact(Contact contact);

    List<CreditCard> findCreditCardsByBank(Bank bank);

    List<CreditCard> findCreditCardsByBankName(String bankName);

    List<CreditCard> findCreditCardsByContactEmail(String email);
}
