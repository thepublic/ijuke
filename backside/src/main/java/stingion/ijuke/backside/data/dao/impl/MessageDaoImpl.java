/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao.impl;

import org.springframework.stereotype.Repository;
import stingion.ijuke.backside.data.dao.MessageDao;
import stingion.ijuke.backside.data.model.Message;

@Repository("messageDao")
public class MessageDaoImpl extends AbstractDaoImpl<Message> implements MessageDao {
}
