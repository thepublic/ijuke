/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import stingion.ijuke.backside.data.model.util.ModelConstants;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Set;

@NamedQueries({
        @NamedQuery(
                name = "findBankByName",
                query = "FROM Bank WHERE name = :inputName"
        )
})
@Entity
@Table(name = "bank", uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
@EqualsAndHashCode(of = "id")
@ToString(of = {"id", "name"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "common_entity")
public class Bank {

    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", unique = true)
    @Length(min = ModelConstants.MIN_BANK_NAME_LENGTH, max = ModelConstants.MAX_BANK_NAME_LENGTH)
    private String name;

    @OneToMany(mappedBy = "bank")
    private Set<CreditCard> creditCards;

    public Bank(String name) {
        this.name = name;
    }
}
