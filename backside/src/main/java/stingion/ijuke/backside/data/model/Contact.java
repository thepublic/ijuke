/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import stingion.ijuke.utils.ijuke.modeltypes.Gender;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "contact")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString(of = {"id", "firstName", "middleName", "lastName", "email", "phoneNumber", "birthDay"})
@NoArgsConstructor
@AllArgsConstructor
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "common_entity")
public class Contact {
    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "birth_day")
    private Date birthDay;

    @OneToMany(mappedBy = "contact")
    private Set<CreditCard> creditCards;

    @Column(columnDefinition = "enum('MALE','FEMALE')")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    public Date getBirthDay() {
        return dateClone(this.birthDay);
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = dateClone(birthDay);
    }

    /**
     * Return clone of variable {@code date}. if {@code date == null} then return {@code null}
     *
     * @param date Var for cloning
     * @return Clone
     */
    private static Date dateClone(Date date) {
        return date != null ? (Date) date.clone() : null;
    }
}
