/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import stingion.ijuke.utils.ijuke.modeltypes.CardType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "credit_card")
@EqualsAndHashCode(of = "id")
@ToString(of = {"id", "number", "type", "bank", "contact"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class CreditCard {

    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue
    private Integer id;

    @Column(name = "number", unique = true)
    private Long number;

    @Column(columnDefinition = "enum('MASTER_CARD','VISA')")
    @Enumerated(EnumType.STRING)
    private CardType type;

    @ManyToOne
    @JoinColumn(name = "bank_id")
    private Bank bank;

    @ManyToOne
    @JoinColumn(name = "contact_id")
    private Contact contact;

    @OneToMany(mappedBy = "creditCard")
    private Set<Ordering> orderings;

}
