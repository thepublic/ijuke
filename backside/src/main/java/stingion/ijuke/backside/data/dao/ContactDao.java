/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao;

import stingion.ijuke.backside.data.model.Contact;

public interface ContactDao extends Dao<Contact> {
    public Contact findByEmail(String email);
}
