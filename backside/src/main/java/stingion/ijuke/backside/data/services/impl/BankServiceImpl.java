/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stingion.ijuke.backside.data.dao.BankDao;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.services.BankService;

@Service("bankService")
public class BankServiceImpl extends AbstractServiceImpl<Bank> implements BankService {
    @Autowired
    @Setter
    private BankDao bankDao;

    @Transactional
    @Override
    public Bank findBankByName(String name) {
        return bankDao.findByName(name);
    }
}
