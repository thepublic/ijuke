/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao.impl;

import com.google.common.collect.Sets;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import stingion.ijuke.backside.data.dao.SupplierDao;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.Supplier;

import java.util.List;
import java.util.Set;

@Repository("supplierDao")
public class SupplierDaoImpl extends AbstractDaoImpl<Supplier> implements SupplierDao {

    @Override
    public Supplier findByName(String name) {
        final String queryString = "FROM Supplier WHERE name = :inputName";

        Query q = getSession().createQuery(queryString);
        q.setParameter("inputName", name);
        List byNameList = q.list();

        return (Supplier) (!byNameList.isEmpty() ? byNameList.get(0) : null);
    }


    @Override
    public Set<Product> getProducts(Supplier supplier) {
        String queryString = "SELECT distinct ps.pk.product FROM ProductSupplier ps WHERE ps.pk.supplier.id = :inputId";

        Query q = getSession().createQuery(queryString);
        q.setParameter("inputId", supplier.getId());

        return Sets.newHashSet(q.list());
    }
}
