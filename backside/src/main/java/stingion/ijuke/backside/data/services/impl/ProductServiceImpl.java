/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stingion.ijuke.backside.data.dao.ProductDao;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.ProductService;


import java.util.Set;

@Service("productService")
public class ProductServiceImpl extends AbstractServiceImpl<Product> implements ProductService {
    @Autowired
    @Setter
    private ProductDao productDao;

    @Override
    @Transactional
    public Product findProductByName(String name) {
        return productDao.findByName(name);
    }

    @Transactional
    @Override
    public Set<Supplier> getSuppliers(Product product) {
        return productDao.getSuppliers(product);
    }
}
