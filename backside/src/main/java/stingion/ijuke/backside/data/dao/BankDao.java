/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao;

import stingion.ijuke.backside.data.model.Bank;

public interface BankDao extends Dao<Bank> {
    public Bank findByName(String name);
}
