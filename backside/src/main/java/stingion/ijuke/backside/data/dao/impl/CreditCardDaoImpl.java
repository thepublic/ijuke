/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import stingion.ijuke.backside.data.dao.CreditCardDao;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.model.CreditCard;

import java.util.List;

@Repository("creditCardDao")
public class CreditCardDaoImpl extends AbstractDaoImpl<CreditCard> implements CreditCardDao {

    @Override
    public List<CreditCard> findByContact(Contact contact) {
        Criteria criteria = getSession().createCriteria(CreditCard.class);
        criteria.add(Restrictions.eq("contact", contact));

        return (List<CreditCard>) criteria.list();
    }

    @Override
    public List<CreditCard> findByBank(Bank bank) {
        final String queryString = "FROM CreditCard WHERE bank = :bank";

        Query q = getSession().createQuery(queryString);
        q.setParameter("bank", bank);

        return (List<CreditCard>) q.list();
    }

    @Override
    public List<CreditCard> findByBankName(String bankName) {
        final String queryString = "SELECT cc FROM CreditCard cc JOIN cc.bank b " +
                "ON b.name = :bankName";

        Query q = getSession().createQuery(queryString);
        q.setParameter("bankName", bankName);

        return (List<CreditCard>) q.list();
    }

    @Override
    public List<CreditCard> findByContactEmail(String email) {
        final String queryString = "SELECT cc FROM CreditCard cc JOIN cc.contact c " +
                "ON c.email = :email";

        Query q = getSession().createQuery(queryString);
        q.setParameter("email", email);

        return (List<CreditCard>) q.list();
    }
}
