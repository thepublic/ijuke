/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import stingion.ijuke.backside.data.dao.Dao;
import stingion.ijuke.backside.data.services.AbstractService;


import java.io.Serializable;
import java.util.List;

@Transactional
public class AbstractServiceImpl<T> implements AbstractService<T> {

    @Autowired
    @Setter
    private Dao<T> abstractDao;

    @SuppressWarnings("unchecked")
    @Override
    public Serializable createNewEntity(T t) {
        return abstractDao.create(t);
    }

    @SuppressWarnings("unchecked")
    @Override
    public T getEntityById(Serializable id) {
        return abstractDao.get(id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void updateEntity(T t) {
        abstractDao.update(t);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void saveOrUpdateEntity(T t) {
        abstractDao.saveOrUpdate(t);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteEntity(T t) {
        abstractDao.delete(t);
    }

    @Override
    public List<T> getEntities() {
        return abstractDao.findAll();
    }

    @Override
    public T getEntityWithLazyObjectsById(Serializable id) {
        return abstractDao.getWithLazyObjects(id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public T mergeEntity(T t) {
        return abstractDao.merge(t);
    }
}
