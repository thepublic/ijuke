/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Folder;
import stingion.ijuke.backside.data.services.FolderService;

@Service("folderService")
public class FolderServiceImpl extends AbstractServiceImpl<Folder> implements FolderService {
}
