/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stingion.ijuke.backside.data.dao.ContactDao;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.services.ContactService;

@Service("contactService")
public class ContactServiceImpl extends AbstractServiceImpl<Contact> implements ContactService {
    @Autowired
    @Setter
    private ContactDao contactDao;

    @Transactional
    @Override
    public Contact findContactByEmail(String email) {
        return contactDao.findByEmail(email);
    }
}
