/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import org.springframework.stereotype.Service;
import stingion.ijuke.backside.data.model.Message;
import stingion.ijuke.backside.data.services.MessageService;

@Service("messageService")
public class MessageServiceImpl extends AbstractServiceImpl<Message> implements MessageService {
}
