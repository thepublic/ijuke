/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao;

import java.io.Serializable;
import java.util.List;

public interface Dao<T> {
    T get(Serializable id);

    T load(Serializable id);

    List<T> findAll();

    Serializable create(T t);

    void update(T t);

    void saveOrUpdate(T t);

    void delete(T t);

    T getWithLazyObjects(Serializable id);
    
    T merge(T t);
}
