/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "message")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString(of = {"id", "from", "to", "cc", "bcc", "subject", "text", "date"})
@NoArgsConstructor
@AllArgsConstructor
public class Message {

    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "[from]")
    private String from;

    @Column(name = "[to]")
    private String to;

    @Column(name = "cc")
    private String cc;

    @Column(name = "bcc")
    private String bcc;

    @Column(name = "subject")
    private String subject;

    @Column(name = "[text]", columnDefinition = "text")
    private String text;

    @Column(name = "date")
    private Date date;

    @ElementCollection
    @CollectionTable(name = "attachment", joinColumns = @JoinColumn(name = "message_id"))
    private List<Attachment> attachments = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "folder_id")
    private Folder folder;

    public Date getDate() {
        return dateClone(this.date);
    }

    public void setDate(Date birthDay) {
        this.date = dateClone(birthDay);
    }

    /**
     * Return clone of variable {@code date}. if {@code date == null} then return {@code null}
     *
     * @param date Var for cloning
     * @return Clone
     */
    private static Date dateClone(Date date) {
        return date != null ? (Date) date.clone() : null;
    }
}
