/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stingion.ijuke.backside.data.dao.CreditCardDao;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.services.CreditCardService;

import java.util.List;

@Service("creditCardService")
public class CreditCardServiceImpl extends AbstractServiceImpl<CreditCard> implements CreditCardService {
    @Autowired
    @Setter
    private CreditCardDao creditCardDao;

    @Transactional
    @Override
    public List<CreditCard> findCreditCardsByContact(Contact contact) {
        return creditCardDao.findByContact(contact);
    }

    @Transactional
    @Override
    public List<CreditCard> findCreditCardsByBank(Bank bank) {
        return creditCardDao.findByBank(bank);
    }

    @Transactional
    @Override
    public List<CreditCard> findCreditCardsByBankName(String bankName) {
        return creditCardDao.findByBankName(bankName);
    }

    @Transactional
    @Override
    public List<CreditCard> findCreditCardsByContactEmail(String email) {
        return creditCardDao.findByContactEmail(email);
    }
}
