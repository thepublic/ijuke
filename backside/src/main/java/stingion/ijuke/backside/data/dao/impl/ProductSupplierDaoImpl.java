/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import stingion.ijuke.backside.data.dao.ProductSupplierDao;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;

import java.util.List;

@Repository("productSupplierDao")
public class ProductSupplierDaoImpl extends AbstractDaoImpl<ProductSupplier> implements ProductSupplierDao {

    @Override
    public ProductSupplier getByProductAndSupplier(Product product, Supplier supplier) {

        String queryString = "SELECT ps FROM ProductSupplier ps " +
                "WHERE ps.pk.product.id = :inputProductId " +
                "AND   ps.pk.supplier.id = :inputSupplierId";

        Query q = getSession().createQuery(queryString);
        q.setParameter("inputProductId", product.getId());
        q.setParameter("inputSupplierId", supplier.getId());
        List productSuppliersList = q.list();

        return (ProductSupplier) (!productSuppliersList.isEmpty() ? productSuppliersList.get(0) : null);
    }

    @Override
    public void update(ProductSupplier productSupplier) {
        throw new UnsupportedOperationException("Not implemented method");
    }


    @Override
    public void saveOrUpdate(ProductSupplier productSupplier) {
        throw new UnsupportedOperationException("Not implemented method");
    }
}
