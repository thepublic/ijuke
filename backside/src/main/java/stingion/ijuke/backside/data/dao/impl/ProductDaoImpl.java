/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao.impl;

import com.google.common.collect.Sets;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import stingion.ijuke.backside.data.dao.ProductDao;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.Supplier;

import java.util.List;
import java.util.Set;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDaoImpl<Product> implements ProductDao {

    @Override
    public Product findByName(String name) {
        final String queryString = "FROM Product WHERE name = :inputName";

        Query q = getSession().createQuery(queryString);
        q.setParameter("inputName", name);
        List byNameList = q.list();

        return (Product) (!byNameList.isEmpty() ? byNameList.get(0) : null);
    }


    @Override
    public Set<Supplier> getSuppliers(Product product) {
        String queryString = "SELECT distinct ps.pk.supplier FROM ProductSupplier ps WHERE ps.pk.product.id = :inputId";

        Query q = getSession().createQuery(queryString);
        q.setParameter("inputId", product.getId());

        return Sets.newHashSet(q.list());
    }
}
