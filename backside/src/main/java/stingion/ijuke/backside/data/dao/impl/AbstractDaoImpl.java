/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao.impl;

import lombok.Setter;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import stingion.ijuke.backside.data.dao.Dao;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@Component("abstractDao")
public class AbstractDaoImpl<T> implements Dao<T> {

    @Setter
    private Class<T> entityClass;

    @Autowired
    @Setter
    private SessionFactory sessionFactory;

    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public T get(Serializable id) {
        return (T) getSession().get(getEntityClass(), id);
    }

    @Override
    public T load(Serializable id) {
        return (T) getSession().load(getEntityClass(), id);
    }

    @Override
    public List<T> findAll() {
        return getSession().createQuery("from " + getDomainClassName()).list();
    }

    public String getDomainClassName() {
        return getEntityClass().getName();
    }

    @Override
    public Serializable create(T t) {
        return getSession().save(t);
    }

    @Override
    public void update(T t) {
        getSession().update(t);
    }    

    @Override
    public void saveOrUpdate(T t) {
        getSession().saveOrUpdate(t);
    }

    @Override
    public void delete(T t) {
        getSession().delete(t);
    }

    @Override
    public T merge(T t) {
        return (T) getSession().merge(t);
    }

    @Override
    public T getWithLazyObjects(Serializable id) {
        T entity = get(id);

        for (Method method : entity.getClass().getDeclaredMethods()) {
            if (method.getParameterTypes().length == 0)
                try {
                    Hibernate.initialize(method.invoke(entity));
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new RuntimeException(e); //NOSONAR
                }
        }

        return entity;
    }

    public Class<T> getEntityClass() {
        if (entityClass == null) {
            ParameterizedType pType = (ParameterizedType) getClass().getGenericSuperclass();
            this.entityClass = (Class<T>) pType.getActualTypeArguments()[0];
        }

        return entityClass;
    }
}
