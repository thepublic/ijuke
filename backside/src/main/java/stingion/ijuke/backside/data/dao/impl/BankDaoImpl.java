/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import stingion.ijuke.backside.data.dao.BankDao;
import stingion.ijuke.backside.data.model.Bank;

import java.util.List;

@Repository("bankDao")
public class BankDaoImpl extends AbstractDaoImpl<Bank> implements BankDao {

    @Override
    public Bank findByName(String name) {
        Query q = getSession().getNamedQuery("findBankByName");
        q.setParameter("inputName", name);
        List byNameList = q.list();

        return (Bank) (!byNameList.isEmpty() ? byNameList.get(0) : null);
    }
}
