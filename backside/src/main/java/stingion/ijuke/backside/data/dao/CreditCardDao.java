/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao;

import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.model.CreditCard;

import java.util.List;

public interface CreditCardDao extends Dao<CreditCard> {
    public List<CreditCard> findByContact(Contact contact);

    public List<CreditCard> findByBank(Bank bank);

    public List<CreditCard> findByBankName(String bankName);

    public List<CreditCard> findByContactEmail(String email);
}
