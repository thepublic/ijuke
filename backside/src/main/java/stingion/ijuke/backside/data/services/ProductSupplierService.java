/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services;

import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;

public interface ProductSupplierService extends AbstractService<ProductSupplier> {
    /**
     * Get ProductSupplier object by Product and Supplier
     */
    ProductSupplier getEntityByProductAndSupplier(Product product, Supplier supplier);

    /**
     * Operation isn't supported
     */
    @Override
    void updateEntity(ProductSupplier productSupplier);

    /**
     * Operation isn't supported
     */
    @Override
    void saveOrUpdateEntity(ProductSupplier productSupplier);
}
