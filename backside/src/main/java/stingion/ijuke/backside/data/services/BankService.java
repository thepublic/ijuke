/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services;

import stingion.ijuke.backside.data.model.Bank;

public interface BankService extends AbstractService<Bank> {
    Bank findBankByName(String name);
}
