package stingion.ijuke.backside.data.temp;

import stingion.ijuke.utils.annotation.Note;

@Note(record = "It's created temporary for testing @Note annotation")
public @interface TempAnnotation {
}
