/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services;

import stingion.ijuke.backside.data.model.Folder;

public interface FolderService extends AbstractService<Folder> {
}
