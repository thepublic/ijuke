/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao;

import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;

public interface ProductSupplierDao extends Dao<ProductSupplier> {
    ProductSupplier getByProductAndSupplier(Product product, Supplier supplier);

    /**
     * Operation isn't supported
     */
    @Override
    void update(ProductSupplier productSupplier);

    /**
     * Operation isn't supported
     */
    @Override
    void saveOrUpdate(ProductSupplier productSupplier);
}
