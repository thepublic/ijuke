/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services;

import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.Supplier;

import java.util.Set;

public interface ProductService extends AbstractService<Product> {
    Product findProductByName(String name);

    /**
     * Get all suppliers for current product
     */
    Set<Supplier> getSuppliers(Product product);
}
