/**
 * Created by ievgen on 8/8/14 10:36 PM.
 *
 */
@Note(record = "Operated date of project")
package stingion.ijuke.backside.data;

import stingion.ijuke.utils.annotation.Note;