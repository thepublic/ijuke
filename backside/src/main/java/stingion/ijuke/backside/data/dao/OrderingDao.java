/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao;

import stingion.ijuke.backside.data.model.Ordering;

public interface OrderingDao extends Dao<Ordering> {
}
