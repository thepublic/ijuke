/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stingion.ijuke.backside.data.dao.SupplierDao;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.SupplierService;


import java.util.Set;

@Service("supplierService")
public class SupplierServiceImpl extends AbstractServiceImpl<Supplier> implements SupplierService {
    @Autowired
    @Setter
    private SupplierDao supplierDao;

    @Override
    @Transactional
    public Supplier findSupplierByName(String name) {
        return supplierDao.findByName(name);
    }

    @Transactional
    @Override
    public Set<Product> getProducts(Supplier supplier) {
        return supplierDao.getProducts(supplier);
    }
}
