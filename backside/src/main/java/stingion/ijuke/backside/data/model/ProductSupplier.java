/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "product_supplier")
@AssociationOverrides({
        @AssociationOverride(name = "pk.product", joinColumns = @JoinColumn(name = "product_id")),
        @AssociationOverride(name = "pk.supplier", joinColumns = @JoinColumn(name = "supplier_id"))
})
@EqualsAndHashCode(of = "pk")
@ToString(of = {"pk"})
@Getter
@Setter
@NoArgsConstructor
public class ProductSupplier {

    @EmbeddedId
    private ProductSupplierId pk = new ProductSupplierId();

    public ProductSupplier(Product product, Supplier supplier) {
        pk.setProduct(product);
        pk.setSupplier(supplier);
    }
}
