/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services;

import java.io.Serializable;
import java.util.List;

public interface AbstractService<T> {
    Serializable createNewEntity(T t);

    T getEntityById(Serializable id);

    void updateEntity(T t);

    void saveOrUpdateEntity(T t);

    void deleteEntity(T t);

    List<T> getEntities();

    T getEntityWithLazyObjectsById(Serializable id);

    T mergeEntity(T t);
}
