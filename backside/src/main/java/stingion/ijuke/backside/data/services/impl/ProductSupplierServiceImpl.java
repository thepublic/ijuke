/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stingion.ijuke.backside.data.dao.ProductSupplierDao;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.ProductSupplierService;

@Service("productSupplierService")
@Transactional
public class ProductSupplierServiceImpl extends AbstractServiceImpl<ProductSupplier> implements ProductSupplierService {
    @Autowired
    @Setter
    private ProductSupplierDao productSupplierDao;

    @Override
    public ProductSupplier getEntityByProductAndSupplier(Product product, Supplier supplier) {
        return productSupplierDao.getByProductAndSupplier(product, supplier);
    }
}
