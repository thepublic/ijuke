package stingion.ijuke.backside.data.model.util;

/**
 * Created under not commercial project
 */
public class ModelConstants {
    public static final int MIN_BANK_NAME_LENGTH = 5;
    public static final int MAX_BANK_NAME_LENGTH = 25;

    private ModelConstants() {
    }
}
