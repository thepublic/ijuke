/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao;

import stingion.ijuke.backside.data.model.Folder;

public interface FolderDao extends Dao<Folder> {
}
