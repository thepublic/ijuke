/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao.impl;

import org.springframework.stereotype.Repository;
import stingion.ijuke.backside.data.dao.OrderingDao;
import stingion.ijuke.backside.data.model.Ordering;

@Repository("orderingDao")
public class OrderingDaoImpl extends AbstractDaoImpl<Ordering> implements OrderingDao {
}
