/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.dao;

import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.Supplier;

import java.util.Set;

public interface ProductDao extends Dao<Product> {
    Product findByName(String name);

    Set<Supplier> getSuppliers(Product product);
}
