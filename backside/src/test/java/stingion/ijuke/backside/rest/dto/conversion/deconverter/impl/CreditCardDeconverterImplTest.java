/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.data.services.CreditCardService;
import stingion.ijuke.backside.rest.dto.conversion.converter.CreditCardConverter;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.CreditCardDeconverter;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class CreditCardDeconverterImplTest {

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private CreditCardConverter creditCardConverter;

    @Autowired
    private CreditCardDeconverter creditCardDeconverter;

    @Test
    public void testDeconvert() {
        CreditCard creditCard = creditCardService.getEntityWithLazyObjectsById(1);
        CreditCardDTO creditCardDTO = creditCardConverter.convertWithLazyObjects(creditCard);
        CreditCard creditCardDeconv = creditCardDeconverter.deconvert(creditCardDTO);

        assertEquals(creditCard.getId(), creditCardDeconv.getId());
        assertEquals(creditCard.getNumber(), creditCardDeconv.getNumber());
        assertEquals(creditCard.getType(), creditCardDeconv.getType());
        assertEquals(creditCard.getBank(), creditCardDeconv.getBank());
        assertEquals(creditCard.getContact(), creditCardDeconv.getContact());

        Iterator<Ordering> orderingCreditCardIterator = creditCard.getOrderings().iterator();
        Iterator<Ordering> orderingCreditCardDeconvIterator = creditCardDeconv.getOrderings().iterator();

        while (orderingCreditCardIterator.hasNext() && orderingCreditCardDeconvIterator.hasNext()) {
            Ordering orderingCreditCard = orderingCreditCardIterator.next();
            Ordering orderingCreditCardDeconv = orderingCreditCardDeconvIterator.next();

            assertEquals(orderingCreditCard.getId(),      orderingCreditCardDeconv.getId());
            assertEquals(orderingCreditCard.getDescription(),  orderingCreditCardDeconv.getDescription());
            assertEquals(orderingCreditCard.getStatus(),    orderingCreditCardDeconv.getStatus());
            assertEquals(orderingCreditCard.getProduct(),    orderingCreditCardDeconv.getProduct());
            assertEquals(orderingCreditCard.getCreditCard(), orderingCreditCardDeconv.getCreditCard());
        }
    }
}
