/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import stingion.ijuke.backside.data.services.AbstractService;
import stingion.ijuke.backside.rest.controller.util.ControllerEntityDataConteiner;
import stingion.ijuke.backside.rest.dto.services.AbstractDTOService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @param <E> Entity
 * @param <D> EntityDTO
 */
public class AbstractControllerTesting<E, D> {

    private static final String ID_NAME = "id";
    private static final String LAZY_NAME = "lazy";
    public static final String SAVE_NAME = "/save";
    public static final String UPDATE_NAME = "/update";
    public static final String SAVE_OR_UPDATE_NAME = "/saveOrUpdate";
    public static final String DELETE_NAME = "/delete";

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private ControllerEntityDataConteiner.ControllerEntityData<E> controllerEntityData;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private AbstractService<E> entityService;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private AbstractDTOService<E, D> abstractDTOService;

    @Before
    public void setup() throws SQLException, IOException, URISyntaxException {//NOSONAR
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void testEntitiesList() throws Exception {//NOSONAR
        mockMvc.perform(get(controllerEntityData.getEntityURL()))
                .andExpect(content().string(controllerEntityData.getExpectedEntitiesList()));
    }

    @Test
    public void testEntityById() throws Exception {//NOSONAR
        mockMvc.perform(get(controllerEntityData.getEntityURL())
                .param(ID_NAME, String.valueOf(controllerEntityData.getEntityId())))
                .andExpect(content().string(controllerEntityData.getExpectedEntityTrueLazy()));
    }

    @Test
    public void testEntityByIdAndTrueLazy() throws Exception {//NOSONAR
        mockMvc.perform(get(controllerEntityData.getEntityURL())
                .param(ID_NAME, String.valueOf(controllerEntityData.getEntityId()))
                .param(LAZY_NAME, String.valueOf(true)))
                .andExpect(content().string(controllerEntityData.getExpectedEntityTrueLazy()));
    }

    @Test
    public void testEntityByIdAndFalseLazy() throws Exception {//NOSONAR

        if (checkContentOfEntityData(controllerEntityData.getExpectedEntity()))
            return;

        if (controllerEntityData.getExpectedEntityAlternatives() != null)
            for (String expectedEntity : controllerEntityData.getExpectedEntityAlternatives()) {
                if (checkContentOfEntityData(expectedEntity))
                    return;
            }
        throw new Exception("No matches" + mockMvc.perform(get(controllerEntityData.getEntityURL())//NOSONAR
                .param(ID_NAME, String.valueOf(controllerEntityData.getEntityId()))
                .param(LAZY_NAME, String.valueOf(false)))
                .andReturn().getResponse().getContentAsString());
    }

    private boolean checkContentOfEntityData(String expectedEntity) throws Exception {//NOSONAR
        return mockMvc.perform(get(controllerEntityData.getEntityURL())
                .param(ID_NAME, String.valueOf(controllerEntityData.getEntityId()))
                .param(LAZY_NAME, String.valueOf(false)))
                .andReturn().getResponse().getContentAsString().equals(expectedEntity);
    }

    @Test
    @Transactional
    public void testSaveEntity() throws Exception {//NOSONAR

        int entitiesCountBefore = entityService.getEntities().size();
        mockMvc.perform(post(controllerEntityData.getEntityURL() + SAVE_NAME)
                .contentType(MediaType.APPLICATION_JSON).content(controllerEntityData.getEntity()))
                .andExpect(status().isOk()).andReturn();
        int entitiesCountAfter = entityService.getEntities().size();

        assertEquals(entitiesCountBefore + 1, entitiesCountAfter);
    }

    @Test
    @Transactional
    public void testUpdateEntity() throws Exception {//NOSONAR
        int entitiesCountBefore = entityService.getEntities().size();
        mockMvc.perform(post(controllerEntityData.getEntityURL() + UPDATE_NAME)
                .contentType(MediaType.APPLICATION_JSON).content(controllerEntityData.getEntity()))
                .andExpect(status().isOk()).andReturn();
        int entitiesCountAfter = entityService.getEntities().size();

        assertEquals(entitiesCountBefore, entitiesCountAfter);
    }

    @Test
    @Transactional
    public void testSaveOrUpdateEntity() throws Exception {//NOSONAR
        int entitiesCountBefore = entityService.getEntities().size();
        mockMvc.perform(post(controllerEntityData.getEntityURL() + SAVE_OR_UPDATE_NAME)
                .contentType(MediaType.APPLICATION_JSON).content(controllerEntityData.getEntity()))
                .andExpect(status().isOk()).andReturn();
        int entitiesCountAfter = entityService.getEntities().size();

        assertEquals(entitiesCountBefore, entitiesCountAfter);
    }

    @Test
    @Transactional
    public void testDeleteEntity() throws Exception {//NOSONAR

        Util util = new Util(entityService, abstractDTOService, mockMvc, controllerEntityData.getEntityURL());

        String entityJson = util.saveEntityAndGetJson(controllerEntityData.getEntity());

        int entitiesCountBefore = entityService.getEntities().size();

        mockMvc.perform(post(controllerEntityData.getEntityURL() + DELETE_NAME)
                .contentType(MediaType.APPLICATION_JSON).content(entityJson))
                .andExpect(status().isOk()).andReturn();
        int entitiesCountAfter = entityService.getEntities().size();

        assertEquals(entitiesCountBefore - 1, entitiesCountAfter);
    }

    public static class Util<Entity, EntityDTO> { //NOSONAR
        private AbstractService<Entity> entityService;
        private AbstractDTOService<Entity, EntityDTO> entityDTOService;
        private MockMvc mockMvc;
        private String entityURL;

        public Util(AbstractService<Entity> entityService, AbstractDTOService<Entity, EntityDTO> entityDTOService,
                    MockMvc mockMvc, String entityURL) {
            this.entityService = entityService;
            this.entityDTOService = entityDTOService;
            this.mockMvc = mockMvc;
            this.entityURL = entityURL;
        }

        public String saveEntityAndGetJson(String entityJson) throws Exception {//NOSONAR

            List<Entity> befAddElementList = entityService.getEntities();
            mockMvc.perform(post(entityURL + SAVE_NAME)
                    .contentType(MediaType.APPLICATION_JSON).content(entityJson))
                    .andExpect(status().isOk()).andReturn();
            List<Entity> afterAddElementList = entityService.getEntities();
            Entity entityForDel = getDistElementJson(befAddElementList, afterAddElementList);

            return new ObjectMapper().writeValueAsString(entityDTOService.convert(entityForDel));
        }

        public Entity getDistElementJson(List<Entity> befor, List<Entity> after) {
            for (Entity distElement : after) {
                if (!befor.contains(distElement)) {
                    return distElement;
                }
            }
            return null;
        }
    }
}
