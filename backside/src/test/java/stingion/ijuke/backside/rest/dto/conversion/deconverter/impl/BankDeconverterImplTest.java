/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.services.BankService;
import stingion.ijuke.backside.rest.dto.conversion.converter.BankConverter;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.BankDeconverter;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class BankDeconverterImplTest {

    @Autowired
    private BankService bankService;

    @Autowired
    private BankConverter bankConverter;

    @Autowired
    private BankDeconverter bankDeconverter;

    @Test
    public void testDeconvert() {
        Bank bank = bankService.getEntityWithLazyObjectsById(1);
        BankDTO bankDTO = bankConverter.convertWithLazyObjects(bank);
        Bank bankDeconv = bankDeconverter.deconvert(bankDTO);

        assertEquals(bank.getId(), bankDeconv.getId());
        assertEquals(bank.getName(), bankDeconv.getName());

        Iterator<CreditCard> creditCardBankIterator = bank.getCreditCards().iterator();
        Iterator<CreditCard> creditCardBankDeconvIterator = bankDeconv.getCreditCards().iterator();

        while (creditCardBankIterator.hasNext() && creditCardBankDeconvIterator.hasNext()) {
            CreditCard creditCardBank = creditCardBankIterator.next();
            CreditCard creditCardBankDeconv = creditCardBankDeconvIterator.next();

            assertEquals(creditCardBank.getId(),      creditCardBankDeconv.getId());
            assertEquals(creditCardBank.getNumber(),  creditCardBankDeconv.getNumber());
            assertEquals(creditCardBank.getType(),    creditCardBankDeconv.getType());
            assertEquals(creditCardBank.getBank(),    creditCardBankDeconv.getBank());
            assertEquals(creditCardBank.getContact(), creditCardBankDeconv.getContact());
        }
    }
}
