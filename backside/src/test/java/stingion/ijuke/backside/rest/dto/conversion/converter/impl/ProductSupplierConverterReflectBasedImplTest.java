/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.ProductService;
import stingion.ijuke.backside.data.services.ProductSupplierService;
import stingion.ijuke.backside.data.services.SupplierService;
import stingion.ijuke.backside.rest.dto.conversion.converter.ProductSupplierConverter;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Map;

@Test
@ContextConfiguration("classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class ProductSupplierConverterReflectBasedImplTest extends AbstractConverterReflectBasedImplTesting<ProductSupplier, ProductSupplierDTO> {

    private static final String EXPECTED_RESULT = "ProductSupplierDTO(pk=ProductSupplierIdDTO(product=ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=null, productSuppliers=null), supplier=SupplierDTO(id=1, name=Mistin, productSuppliers=null)))";

    private static final int DEPTH = 0;

    private static final boolean IS_LAZY = true;

    private static final Map<String, String> EXPECTED_PRODUCT_SUPPLIER_DTO_TO_STRING_MAP = ImmutableMap.of(
        "convertByEntity",                        EXPECTED_RESULT,
        "convertByEntityWithLazyObjects",         EXPECTED_RESULT,
        "convertByEntityAndDepth",                EXPECTED_RESULT,
        "convertByEntityWithLazyObjectsAndDepth", EXPECTED_RESULT,
        "convertByEntityAndDepthAndLazy",         EXPECTED_RESULT
    );

    @Autowired
    private ProductSupplierConverter productSupplierConverter;

    @Autowired
    private ProductService productService;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private ProductSupplierService productSupplierService;


    @BeforeMethod(firstTimeOnly = true)
    public void initHSQLDB() throws SQLException, IOException, URISyntaxException {
        Product product = productService.getEntityWithLazyObjectsById(1);
        Supplier supplier = supplierService.getEntityWithLazyObjectsById(1);
        super.setEntity(productSupplierService.getEntityByProductAndSupplier(product, supplier));
        super.setDepth(DEPTH);
        super.setLazy(IS_LAZY);
        super.setExpectedEntityDTOToStringMap(EXPECTED_PRODUCT_SUPPLIER_DTO_TO_STRING_MAP);
        super.setEntityConverter(productSupplierConverter);
    }
}
