/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.data.services.OrderingService;
import stingion.ijuke.backside.rest.dto.conversion.converter.OrderingConverter;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.OrderingDeconverter;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class OrderingDeconverterImplTest {

    @Autowired
    private OrderingService orderingService;

    @Autowired
    private OrderingConverter orderingConverter;

    @Autowired
    private OrderingDeconverter orderingDeconverter;

    @Test
    public void testDeconvert() {
        Ordering ordering = orderingService.getEntityWithLazyObjectsById(1);
        OrderingDTO orderingDTO = orderingConverter.convertWithLazyObjects(ordering);
        Ordering orderingDeconv = orderingDeconverter.deconvert(orderingDTO);

        assertEquals(ordering.getId(), orderingDeconv.getId());
        assertEquals(ordering.getDescription(), orderingDeconv.getDescription());
        assertEquals(ordering.getStatus(), orderingDeconv.getStatus());
        assertEquals(ordering.getProduct(), orderingDeconv.getProduct());
        assertEquals(ordering.getCreditCard(), orderingDeconv.getCreditCard());
    }
}
