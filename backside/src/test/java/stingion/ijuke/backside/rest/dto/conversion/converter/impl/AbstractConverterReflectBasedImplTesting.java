/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import lombok.Setter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;
import stingion.ijuke.backside.rest.dto.conversion.converter.Converter;

import java.util.List;
import java.util.Map;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

@ContextConfiguration("classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class AbstractConverterReflectBasedImplTesting<Entity, EntityDTO> extends AbstractTestNGSpringContextTests {//NOSONAR

    @Setter
    private int depth;

    @Setter
    private boolean isLazy;

    @Setter
    private Map<String, String> expectedEntityDTOToStringMap;

    @Setter
    private Entity entity;

    @Setter
    private Converter<Entity, EntityDTO> entityConverter;

    @Setter
    private List<String> convertByEntityWithLazyObjectsAlternatives;

    private String expectedEntityDTOToString;

    private String  actualEntityDTOToString;

    @Test
    public void testConvertByEntity() {
        expectedEntityDTOToString = expectedEntityDTOToStringMap.get("convertByEntity");
        actualEntityDTOToString = entityConverter.convert(entity).toString();

        assertEquals(expectedEntityDTOToString, actualEntityDTOToString);
    }


    @Test
    public void testConvertByEntityWithLazyObjects() {
        expectedEntityDTOToString = expectedEntityDTOToStringMap.get("convertByEntityWithLazyObjects");
        actualEntityDTOToString = entityConverter.convertWithLazyObjects(entity).toString();

        assertTrue(expectedEntityDTOToString.equals(actualEntityDTOToString) ||
                (convertByEntityWithLazyObjectsAlternatives != null
                        && convertByEntityWithLazyObjectsAlternatives.contains(actualEntityDTOToString)));
    }

    @Test
    public void testConvertByEntityAndDepth() {
        expectedEntityDTOToString = expectedEntityDTOToStringMap.get("convertByEntityAndDepth");
        actualEntityDTOToString = entityConverter.convert(entity, depth).toString();

        assertEquals(expectedEntityDTOToString, actualEntityDTOToString);
    }


    @Test
    public void testConvertByEntityWithLazyObjectsAndDepth() {
        expectedEntityDTOToString = expectedEntityDTOToStringMap.get("convertByEntityWithLazyObjectsAndDepth");
        actualEntityDTOToString = entityConverter.convertWithLazyObjects(entity, depth).toString();

        assertEquals(expectedEntityDTOToString, actualEntityDTOToString);
    }

    @Test
    public void testConvertByEntityAndDepthAndLazy() {
        expectedEntityDTOToString = expectedEntityDTOToStringMap.get("convertByEntityAndDepthAndLazy");
        actualEntityDTOToString = entityConverter.convert(entity, depth, isLazy).toString();

        assertEquals(expectedEntityDTOToString, actualEntityDTOToString);
    }
}
