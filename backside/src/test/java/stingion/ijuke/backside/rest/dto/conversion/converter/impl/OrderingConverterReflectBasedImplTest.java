/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.data.services.OrderingService;
import stingion.ijuke.backside.rest.dto.conversion.converter.OrderingConverter;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Map;

@Test
@ContextConfiguration("classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class OrderingConverterReflectBasedImplTest extends AbstractConverterReflectBasedImplTesting<Ordering, OrderingDTO> {

    private static final String EXPECTED_WORD = "OrderingDTO(id=1, description=Deliver to Kharkiv, Melnyka 15/65, status=PENDING, product=ProductDTO(id=4, name=E-book Amazon Kindle 6, price=90.25, orderings=null, productSuppliers=null), creditCard=CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE), orderings=null))";

    private static final int ORDERING_ID = 1;

    private static final int DEPTH = 0;

    private static final boolean IS_LAZY = true;

    private static final Map<String, String> EXPECTED_ORDERING_DTO_TO_STRING_MAP = ImmutableMap.of(
        "convertByEntity",                        EXPECTED_WORD,
        "convertByEntityWithLazyObjects",         EXPECTED_WORD,
        "convertByEntityAndDepth",                EXPECTED_WORD,
        "convertByEntityWithLazyObjectsAndDepth", EXPECTED_WORD,
        "convertByEntityAndDepthAndLazy",         EXPECTED_WORD
    );

    @Autowired
    private OrderingConverter orderingConverter;

    @Autowired
    private OrderingService orderingService;


    @BeforeMethod(firstTimeOnly = true)
    public void initHSQLDB() throws SQLException, IOException, URISyntaxException {
        super.setEntity(orderingService.getEntityWithLazyObjectsById(ORDERING_ID));
        super.setDepth(DEPTH);
        super.setLazy(IS_LAZY);
        super.setExpectedEntityDTOToStringMap(EXPECTED_ORDERING_DTO_TO_STRING_MAP);
        super.setEntityConverter(orderingConverter);
    }
}
