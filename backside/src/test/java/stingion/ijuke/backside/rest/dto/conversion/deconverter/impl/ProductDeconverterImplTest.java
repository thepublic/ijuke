/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.services.ProductService;
import stingion.ijuke.backside.rest.dto.conversion.converter.ProductConverter;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.ProductDeconverter;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class ProductDeconverterImplTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductConverter productConverter;

    @Autowired
    private ProductDeconverter productDeconverter;

    @Test
    public void testDeconvert() {
        Product product = productService.getEntityWithLazyObjectsById(1);
        ProductDTO productDTO = productConverter.convertWithLazyObjects(product);
        Product productDeconv = productDeconverter.deconvert(productDTO);

        assertEquals(product.getId(), productDeconv.getId());
        assertEquals(product.getName(), productDeconv.getName());
        assertEquals(product.getPrice(), productDeconv.getPrice());

        Iterator<Ordering> orderingProductIterator = product.getOrderings().iterator();
        Iterator<Ordering> orderingProductDeconvIterator = productDeconv.getOrderings().iterator();

        while (orderingProductIterator.hasNext() && orderingProductDeconvIterator.hasNext()) {
            Ordering orderingProduct = orderingProductIterator.next();
            Ordering orderingProductDeconv =orderingProductDeconvIterator.next();

            assertEquals(orderingProduct.getId(),          orderingProductDeconv.getId());
            assertEquals(orderingProduct.getDescription(), orderingProductDeconv.getDescription());
            assertEquals(orderingProduct.getStatus(),      orderingProductDeconv.getStatus());
            assertEquals(orderingProduct.getProduct(),     orderingProductDeconv.getProduct());
            assertEquals(orderingProduct.getCreditCard(),  orderingProductDeconv.getCreditCard());
        }

        Iterator<ProductSupplier> productSupplierProductIterator = product.getProductSuppliers().iterator();
        Iterator<ProductSupplier> productSupplierDeconvIterator = productDeconv.getProductSuppliers().iterator();

        while (productSupplierProductIterator.hasNext() && productSupplierDeconvIterator.hasNext()) {
            ProductSupplier productSupplierProduct = productSupplierProductIterator.next();
            ProductSupplier productSupplierProductDeconv = productSupplierDeconvIterator.next();

            assertEquals(productSupplierProduct.getPk().getProduct(),  productSupplierProductDeconv.getPk().getProduct());
            assertEquals(productSupplierProduct.getPk().getSupplier(), productSupplierProductDeconv.getPk().getSupplier());
        }
    }
}
