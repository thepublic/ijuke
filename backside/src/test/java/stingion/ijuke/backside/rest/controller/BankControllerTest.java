/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({
        "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml",
        "classpath:META-INF/stingion/ijuke/backside/spring-web/mvc-dispatcher-servlet.xml"
})
public class BankControllerTest extends AbstractControllerTesting<Bank, BankDTO> {
}
