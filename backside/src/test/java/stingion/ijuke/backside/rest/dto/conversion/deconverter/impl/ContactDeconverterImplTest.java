/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.services.ContactService;
import stingion.ijuke.backside.rest.dto.conversion.converter.ContactConverter;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.ContactDeconverter;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class ContactDeconverterImplTest {

    @Autowired
    private ContactService contactService;

    @Autowired
    private ContactConverter contactConverter;

    @Autowired
    private ContactDeconverter contactDeconverter;

    @Test
    public void testDeconvert() {
        Contact contact = contactService.getEntityWithLazyObjectsById(1);
        ContactDTO contactDTO = contactConverter.convertWithLazyObjects(contact);
        Contact contactDeconv = contactDeconverter.deconvert(contactDTO);

        assertEquals(contact.getId(), contactDeconv.getId());
        assertEquals(contact.getFirstName(), contactDeconv.getFirstName());
        assertEquals(contact.getMiddleName(), contactDeconv.getMiddleName());
        assertEquals(contact.getLastName(), contactDeconv.getLastName());
        assertEquals(contact.getEmail(), contactDeconv.getEmail());
        assertEquals(contact.getPhoneNumber(), contactDeconv.getPhoneNumber());
        assertEquals(contact.getBirthDay(), contactDeconv.getBirthDay());

        Iterator<CreditCard> creditCardContactIterator = contact.getCreditCards().iterator();
        Iterator<CreditCard> creditCardContactDeconvIterator = contactDeconv.getCreditCards().iterator();

        while (creditCardContactIterator.hasNext() && creditCardContactDeconvIterator.hasNext()) {
            CreditCard creditCardContact = creditCardContactIterator.next();
            CreditCard creditCardContactDeconv = creditCardContactDeconvIterator.next();

            assertEquals(creditCardContact.getId(),      creditCardContactDeconv.getId());
            assertEquals(creditCardContact.getNumber(),  creditCardContactDeconv.getNumber());
            assertEquals(creditCardContact.getType(),    creditCardContactDeconv.getType());
            assertEquals(creditCardContact.getBank(),    creditCardContactDeconv.getBank());
            assertEquals(creditCardContact.getContact(), creditCardContactDeconv.getContact());
        }
    }
}
