/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({
        "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml",
        "classpath:META-INF/stingion/ijuke/backside/spring-web/mvc-dispatcher-servlet.xml",
        "classpath:META-INF/stingion/ijuke/backside/spring-web/spring-security.xml"
})
public class CheckLoginTest {

    private static final String REST_URL = "/rest/database/bank";
    private static final String USERNAME = "admin";
    private static final String TEMP_PASS = "nimda";

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private Filter springSecurityFilterChain;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .addFilters(this.springSecurityFilterChain).build();
    }

    @Test
    public void testAccessGranted() throws Exception {//NOSONAR
        mockMvc.perform(get(REST_URL).with(httpBasic(USERNAME, TEMP_PASS)))
                .andExpect(status().isNotFound())
                .andExpect(authenticated().withUsername(USERNAME));

        mockMvc.perform(post("/j_spring_security_check").param("j_username", USERNAME).param("j_password", TEMP_PASS))
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void testAccessDenied() throws Exception {//NOSONAR
        mockMvc.perform(get(REST_URL).with(user(USERNAME).roles("DENIED")))
                .andExpect(status().isForbidden());

        mockMvc.perform(post("/j_spring_security_check").param("j_username", USERNAME).param("j_password", TEMP_PASS + "invalid"))
                .andExpect(redirectedUrl("/spring_security_login?login_error"));
    }
}
