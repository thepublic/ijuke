/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller.util;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Configuration;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.Supplier;

import java.util.List;

@Configuration
public class ControllerEntityDataConteiner {//NOSONAR

    @Getter
    @Setter
    public static class ControllerEntityData<T> {//NOSONAR
        private int entityId;//NOSONAR
        private String entityURL;//NOSONAR
        private String entity;//NOSONAR
        private String expectedEntitiesList;//NOSONAR
        private String expectedEntity;//NOSONAR
        private String expectedEntityTrueLazy;//NOSONAR

        private List<String> expectedEntityAlternatives;//NOSONAR
    }

    public static class ControllerBankData extends ControllerEntityData<Bank> {
    }

    public static class ControllerContactData extends ControllerEntityData<Contact> {
    }

    public static class ControllerOrderingData extends ControllerEntityData<Ordering> {
    }

    public static class ControllerCreditCardData extends ControllerEntityData<CreditCard> {
    }

    public static class ControllerProductData extends ControllerEntityData<Product> {
    }

    public static class ControllerSupplierData extends ControllerEntityData<Supplier> {
    }
}
