/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.services.CreditCardService;
import stingion.ijuke.backside.rest.dto.conversion.converter.CreditCardConverter;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Map;

@Test
@ContextConfiguration("classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class CreditCardConverterReflectBasedImplTest extends AbstractConverterReflectBasedImplTesting<CreditCard, CreditCardDTO> {

    private static final int CREDIT_CARD_ID = 1;

    private static final int DEPTH = 0;

    private static final boolean IS_LAZY = true;

    private static final Map<String, String> EXPECTED_CREDIT_CARD_DTO_TO_STRING_MAP = ImmutableMap.of(
        "convertByEntity",                        "CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE), orderings=null)",//NOSONAR
        "convertByEntityWithLazyObjects",         "CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE), orderings=[OrderingDTO(id=1, description=Deliver to Kharkiv, Melnyka 15/65, status=PENDING, product=ProductDTO(id=4, name=E-book Amazon Kindle 6, price=90.25, orderings=null, productSuppliers=null), creditCard=CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE), orderings=[null]))])",
        "convertByEntityAndDepth",                "CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE), orderings=null)",
        "convertByEntityWithLazyObjectsAndDepth", "CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE), orderings=[null])",
        "convertByEntityAndDepthAndLazy",         "CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE), orderings=null)"
    );

    @Autowired
    private CreditCardConverter creditCardConverter;

    @Autowired
    private CreditCardService creditCardService;


    @BeforeMethod(firstTimeOnly = true)
    public void initHSQLDB() throws SQLException, IOException, URISyntaxException {
        super.setEntity(creditCardService.getEntityWithLazyObjectsById(CREDIT_CARD_ID));
        super.setDepth(DEPTH);
        super.setLazy(IS_LAZY);
        super.setExpectedEntityDTOToStringMap(EXPECTED_CREDIT_CARD_DTO_TO_STRING_MAP);
        super.setEntityConverter(creditCardConverter);
    }
}
