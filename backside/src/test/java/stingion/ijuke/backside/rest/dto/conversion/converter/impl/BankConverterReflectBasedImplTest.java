/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.services.BankService;
import stingion.ijuke.backside.rest.dto.conversion.converter.BankConverter;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Test
@ContextConfiguration("classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class BankConverterReflectBasedImplTest extends AbstractConverterReflectBasedImplTesting<Bank, BankDTO> {

    private static final int BANK_ID = 1;

    private static final int DEPTH = 0;

    private static final boolean IS_LAZY = true;

    private static final Map<String, String> EXPECTED_BANK_DTO_TO_STRING_MAP = ImmutableMap.of(
        "convertByEntity",                        "BankDTO(id=1, name=Imperial Bank, creditCards=null)",//NOSONAR
        "convertByEntityWithLazyObjects",         "BankDTO(id=1, name=Imperial Bank, creditCards=[CreditCardDTO(id=3, number=1123729594138004, type=MASTER_CARD, bank=BankDTO(id=1, name=Imperial Bank, creditCards=[null]), contact=ContactDTO(id=2, firstName=Ann, middleName=John, lastName=Qurie, email=ajqurie@ijuke.org, phoneNumber=0727789495, birthDay=1993-09-28 00:00:00.0, creditCards=null, gender=FEMALE), orderings=null), CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=[null]), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE), orderings=null)])",
        "convertByEntityAndDepth",                "BankDTO(id=1, name=Imperial Bank, creditCards=null)",
        "convertByEntityWithLazyObjectsAndDepth", "BankDTO(id=1, name=Imperial Bank, creditCards=[null])",
        "convertByEntityAndDepthAndLazy",         "BankDTO(id=1, name=Imperial Bank, creditCards=null)"
    );

    private static final List<String> CONVERT_BY_ENTITY_WITH_LAZY_OBJECTS_ALTERNATIVES =
            ImmutableList.of("BankDTO(id=1, name=Imperial Bank, creditCards=[CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=[null]), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE), orderings=null), CreditCardDTO(id=3, number=1123729594138004, type=MASTER_CARD, bank=BankDTO(id=1, name=Imperial Bank, creditCards=[null]), contact=ContactDTO(id=2, firstName=Ann, middleName=John, lastName=Qurie, email=ajqurie@ijuke.org, phoneNumber=0727789495, birthDay=1993-09-28 00:00:00.0, creditCards=null, gender=FEMALE), orderings=null)])");

    @Autowired
    private BankConverter bankConverter;

    @Autowired
    private BankService bankService;

    @BeforeMethod
    public void init() throws SQLException, IOException, URISyntaxException {
        super.setEntity(bankService.getEntityWithLazyObjectsById(BANK_ID));
        super.setDepth(DEPTH);
        super.setLazy(IS_LAZY);
        super.setExpectedEntityDTOToStringMap(EXPECTED_BANK_DTO_TO_STRING_MAP);
        super.setEntityConverter(bankConverter);
        super.setConvertByEntityWithLazyObjectsAlternatives(CONVERT_BY_ENTITY_WITH_LAZY_OBJECTS_ALTERNATIVES);
    }
}
