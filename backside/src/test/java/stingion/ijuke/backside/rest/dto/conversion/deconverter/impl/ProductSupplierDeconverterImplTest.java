/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.services.ProductService;
import stingion.ijuke.backside.data.services.ProductSupplierService;
import stingion.ijuke.backside.data.services.SupplierService;
import stingion.ijuke.backside.rest.dto.conversion.converter.ProductSupplierConverter;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.ProductSupplierDeconverter;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class ProductSupplierDeconverterImplTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private ProductSupplierService productSupplierService;

    @Autowired
    private ProductSupplierConverter productSupplierConverter;

    @Autowired
    private ProductSupplierDeconverter productSupplierDeconverter;

    @Test
    public void testDeconvert() {
        ProductSupplier productSupplier = productSupplierService.getEntityByProductAndSupplier(
                                                                productService.getEntityById(1),
                                                                supplierService.getEntityById(1));

        ProductSupplierDTO productSupplierDTO = productSupplierConverter.convertWithLazyObjects(productSupplier);
        ProductSupplier productSupplierDeconv = productSupplierDeconverter.deconvert(productSupplierDTO);

        assertEquals(productSupplier.getPk().getProduct(), productSupplierDeconv.getPk().getProduct());
        assertEquals(productSupplier.getPk().getSupplier(), productSupplierDeconv.getPk().getSupplier());
    }
}
