/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.SupplierService;
import stingion.ijuke.backside.rest.dto.conversion.converter.SupplierConverter;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Test
@ContextConfiguration("classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class SupplierConverterReflectBasedImplTest extends AbstractConverterReflectBasedImplTesting<Supplier, SupplierDTO> {

    private static final int SUPPLIER_ID = 1;

    private static final int DEPTH = 0;

    private static final boolean IS_LAZY = true;

    private static final Map<String, String> EXPECTED_SUPPLIER_DTO_TO_STRING_MAP = ImmutableMap.of(
        "convertByEntity",                        "SupplierDTO(id=1, name=Mistin, productSuppliers=null)",//NOSONAR
        "convertByEntityWithLazyObjects",         "SupplierDTO(id=1, name=Mistin, productSuppliers=[ProductSupplierDTO(pk=ProductSupplierIdDTO(product=ProductDTO(id=3, name=Laptop Dell Inspiron 7550, price=950.0, orderings=null, productSuppliers=null), supplier=SupplierDTO(id=1, name=Mistin, productSuppliers=[null]))), ProductSupplierDTO(pk=ProductSupplierIdDTO(product=ProductDTO(id=2, name=Laptop Apple MacBook Fly, price=1500.0, orderings=null, productSuppliers=null), supplier=SupplierDTO(id=1, name=Mistin, productSuppliers=[null]))), ProductSupplierDTO(pk=ProductSupplierIdDTO(product=ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=null, productSuppliers=null), supplier=SupplierDTO(id=1, name=Mistin, productSuppliers=[null])))])",
        "convertByEntityAndDepth",                "SupplierDTO(id=1, name=Mistin, productSuppliers=null)",
        "convertByEntityWithLazyObjectsAndDepth", "SupplierDTO(id=1, name=Mistin, productSuppliers=[null])",
        "convertByEntityAndDepthAndLazy",         "SupplierDTO(id=1, name=Mistin, productSuppliers=null)"
    );

    private static final List<String> CONVERT_BY_ENTITY_WITH_LAZY_OBJECTS_ALTERNATIVES =
            ImmutableList.of("SupplierDTO(id=1, name=Mistin, productSuppliers=[ProductSupplierDTO(pk=ProductSupplierIdDTO(product=ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=null, productSuppliers=null), supplier=SupplierDTO(id=1, name=Mistin, productSuppliers=[null]))), ProductSupplierDTO(pk=ProductSupplierIdDTO(product=ProductDTO(id=3, name=Laptop Dell Inspiron 7550, price=950.0, orderings=null, productSuppliers=null), supplier=SupplierDTO(id=1, name=Mistin, productSuppliers=[null]))), ProductSupplierDTO(pk=ProductSupplierIdDTO(product=ProductDTO(id=2, name=Laptop Apple MacBook Fly, price=1500.0, orderings=null, productSuppliers=null), supplier=SupplierDTO(id=1, name=Mistin, productSuppliers=[null])))])");

    @Autowired
    private SupplierConverter supplierConverter;

    @Autowired
    private SupplierService supplierService;


    @BeforeMethod(firstTimeOnly = true)
    public void initHSQLDB() throws SQLException, IOException, URISyntaxException {
        super.setEntity(supplierService.getEntityWithLazyObjectsById(SUPPLIER_ID));
        super.setDepth(DEPTH);
        super.setLazy(IS_LAZY);
        super.setExpectedEntityDTOToStringMap(EXPECTED_SUPPLIER_DTO_TO_STRING_MAP);
        super.setEntityConverter(supplierConverter);
        super.setConvertByEntityWithLazyObjectsAlternatives(CONVERT_BY_ENTITY_WITH_LAZY_OBJECTS_ALTERNATIVES);
    }
}
