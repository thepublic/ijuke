/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.deconverter.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.SupplierService;
import stingion.ijuke.backside.rest.dto.conversion.converter.SupplierConverter;
import stingion.ijuke.backside.rest.dto.conversion.deconverter.SupplierDeconverter;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class SupplierDeconverterImplTest {

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private SupplierConverter supplierConverter;

    @Autowired
    private SupplierDeconverter supplierDeconverter;

    @Test
    public void testDeconvert() {
        Supplier supplier = supplierService.getEntityWithLazyObjectsById(1);
        SupplierDTO supplierDTO = supplierConverter.convertWithLazyObjects(supplier);
        Supplier supplierDeconv = supplierDeconverter.deconvert(supplierDTO);

        assertEquals(supplier.getId(), supplierDeconv.getId());
        assertEquals(supplier.getName(), supplierDeconv.getName());

        Iterator<ProductSupplier> productSupplierProductIterator = supplier.getProductSuppliers().iterator();
        Iterator<ProductSupplier> productSupplierDeconvIterator = supplierDeconv.getProductSuppliers().iterator();

        while (productSupplierProductIterator.hasNext() && productSupplierDeconvIterator.hasNext()) {
            ProductSupplier productSupplierProduct = productSupplierProductIterator.next();
            ProductSupplier productSupplierProductDeconv = productSupplierDeconvIterator.next();

            assertEquals(productSupplierProduct.getPk().getProduct(),  productSupplierProductDeconv.getPk().getProduct());
            assertEquals(productSupplierProduct.getPk().getSupplier(), productSupplierProductDeconv.getPk().getSupplier());
        }
    }
}
