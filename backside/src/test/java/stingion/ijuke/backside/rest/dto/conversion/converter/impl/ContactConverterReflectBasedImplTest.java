/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.services.ContactService;
import stingion.ijuke.backside.rest.dto.conversion.converter.ContactConverter;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Test
@ContextConfiguration("classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class ContactConverterReflectBasedImplTest extends AbstractConverterReflectBasedImplTesting<Contact, ContactDTO> {

    private static final int CONTACT_ID = 1;

    private static final int DEPTH = 0;

    private static final boolean IS_LAZY = true;

    private static final Map<String, String> EXPECTED_CONTACT_DTO_TO_STRING_MAP = ImmutableMap.of(
        "convertByEntity",                        "ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE)",//NOSONAR
        "convertByEntityWithLazyObjects",         "ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=[CreditCardDTO(id=4, number=6541296068298653, type=VISA, bank=BankDTO(id=4, name=Plaza, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=[null], gender=MALE), orderings=null), CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=[null], gender=MALE), orderings=null)], gender=MALE)",
        "convertByEntityAndDepth",                "ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE)",
        "convertByEntityWithLazyObjectsAndDepth", "ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=[null], gender=MALE)",
        "convertByEntityAndDepthAndLazy",         "ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE)"
    );

    private static final List<String> CONVERT_BY_ENTITY_WITH_LAZY_OBJECTS_ALTERNATIVES =
            ImmutableList.of("ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=[CreditCardDTO(id=1, number=6647486058945594, type=VISA, bank=BankDTO(id=1, name=Imperial Bank, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=[null], gender=MALE), orderings=null), CreditCardDTO(id=4, number=6541296068298653, type=VISA, bank=BankDTO(id=4, name=Plaza, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=[null], gender=MALE), orderings=null)], gender=MALE)");

    @Autowired
    private ContactConverter contactConverter;

    @Autowired
    private ContactService contactService;


    @BeforeMethod(firstTimeOnly = true)
    public void initHSQLDB() throws SQLException, IOException, URISyntaxException {
        super.setEntity(contactService.getEntityWithLazyObjectsById(CONTACT_ID));
        super.setDepth(DEPTH);
        super.setLazy(IS_LAZY);
        super.setExpectedEntityDTOToStringMap(EXPECTED_CONTACT_DTO_TO_STRING_MAP);
        super.setEntityConverter(contactConverter);
        super.setConvertByEntityWithLazyObjectsAlternatives(CONVERT_BY_ENTITY_WITH_LAZY_OBJECTS_ALTERNATIVES);
    }
}
