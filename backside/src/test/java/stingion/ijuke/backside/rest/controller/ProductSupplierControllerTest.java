/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.controller;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import stingion.ijuke.backside.data.services.ProductSupplierService;
import stingion.ijuke.backside.rest.dto.services.ProductSupplierDTOService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({
        "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml",
        "classpath:META-INF/stingion/ijuke/backside/spring-web/mvc-dispatcher-servlet.xml"
})
@TransactionConfiguration
public class ProductSupplierControllerTest {

    private static final int PRODUCT_ID = 1;
    private static final int SUPPLIER_ID = 1;

    private static final String PRODUCT_SUPPLIER_URL = "/database/productSupplier";

    private static final String PRODUCT_SUPPLIER_FOR_SAVING = "{\"pk\":{\"product\":{\"id\":4,\"name\":\"E-book Amazon Kindle 6\",\"price\":90.25,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":1,\"name\":\"Mistin\",\"productSuppliers\":null}}}";
    private static final String EXPECTED_PRODUCT_SUPPLIER_LIST = "[{\"pk\":{\"product\":{\"id\":1,\"name\":\"Phone Acer V370\",\"price\":180.5,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":1,\"name\":\"Mistin\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":1,\"name\":\"Phone Acer V370\",\"price\":180.5,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":2,\"name\":\"Highelectro\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":1,\"name\":\"Phone Acer V370\",\"price\":180.5,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":3,\"name\":\"Nikita Borinko\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":2,\"name\":\"Laptop Apple MacBook Fly\",\"price\":1500.0,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":1,\"name\":\"Mistin\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":2,\"name\":\"Laptop Apple MacBook Fly\",\"price\":1500.0,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":2,\"name\":\"Highelectro\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":3,\"name\":\"Laptop Dell Inspiron 7550\",\"price\":950.0,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":1,\"name\":\"Mistin\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":3,\"name\":\"Laptop Dell Inspiron 7550\",\"price\":950.0,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":3,\"name\":\"Nikita Borinko\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":4,\"name\":\"E-book Amazon Kindle 6\",\"price\":90.25,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":2,\"name\":\"Highelectro\",\"productSuppliers\":null}}}]";
    private static final String EXPECTED_PRODUCT_SUPPLIER_TRUE_LAZY_LIST = "[{\"pk\":{\"product\":{\"id\":1,\"name\":\"Phone Acer V370\",\"price\":180.5,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":1,\"name\":\"Mistin\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":1,\"name\":\"Phone Acer V370\",\"price\":180.5,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":2,\"name\":\"Highelectro\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":1,\"name\":\"Phone Acer V370\",\"price\":180.5,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":3,\"name\":\"Nikita Borinko\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":2,\"name\":\"Laptop Apple MacBook Fly\",\"price\":1500.0,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":1,\"name\":\"Mistin\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":2,\"name\":\"Laptop Apple MacBook Fly\",\"price\":1500.0,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":2,\"name\":\"Highelectro\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":3,\"name\":\"Laptop Dell Inspiron 7550\",\"price\":950.0,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":1,\"name\":\"Mistin\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":3,\"name\":\"Laptop Dell Inspiron 7550\",\"price\":950.0,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":3,\"name\":\"Nikita Borinko\",\"productSuppliers\":null}}},{\"pk\":{\"product\":{\"id\":4,\"name\":\"E-book Amazon Kindle 6\",\"price\":90.25,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":2,\"name\":\"Highelectro\",\"productSuppliers\":null}}}]";
    private static final String EXPECTED_PRODUCT_SUPPLIER_BY_PRODUCT_ID_AND_SUPPLIER_ID = "{\"pk\":{\"product\":{\"id\":1,\"name\":\"Phone Acer V370\",\"price\":180.5,\"orderings\":null,\"productSuppliers\":null},\"supplier\":{\"id\":1,\"name\":\"Mistin\",\"productSuppliers\":null}}}";

    @Autowired
    private ProductSupplierService productSupplierService;

    @Autowired
    private ProductSupplierDTOService productSupplierDTOService;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() throws SQLException, IOException, URISyntaxException {//NOSONAR
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void testProductSupplierList() throws Exception {//NOSONAR
        mockMvc.perform(get(PRODUCT_SUPPLIER_URL)).andExpect(content().string(EXPECTED_PRODUCT_SUPPLIER_LIST));
    }

    @Test
    public void testProductSupplierTrueLazyList() throws Exception {//NOSONAR
        mockMvc.perform(get(PRODUCT_SUPPLIER_URL).param("lazy", String.valueOf("true")))
                .andExpect(content().string(EXPECTED_PRODUCT_SUPPLIER_TRUE_LAZY_LIST));
    }

    @Test
    public void testProductSupplierByProductIdAndSupplierIdList() throws Exception {//NOSONAR
        mockMvc.perform(get(PRODUCT_SUPPLIER_URL)
                .param("productId", String.valueOf(PRODUCT_ID))
                .param("supplierId", String.valueOf(SUPPLIER_ID)))
                .andExpect(content().string(EXPECTED_PRODUCT_SUPPLIER_BY_PRODUCT_ID_AND_SUPPLIER_ID));
    }

    @Test
    @Transactional
    public void testSaveEntity() throws Exception {//NOSONAR

        int entitiesCountBefore = productSupplierService.getEntities().size();
        mockMvc.perform(post(PRODUCT_SUPPLIER_URL + AbstractControllerTesting.SAVE_NAME)
                .contentType(MediaType.APPLICATION_JSON).content(PRODUCT_SUPPLIER_FOR_SAVING))
                .andExpect(status().isOk()).andReturn();
        int entitiesCountAfter = productSupplierService.getEntities().size();

        assertEquals(entitiesCountBefore + 1, entitiesCountAfter);
    }

    @Test
    public void testUpdateEntity() throws Exception {//NOSONAR

        mockMvc.perform(post(PRODUCT_SUPPLIER_URL + AbstractControllerTesting.UPDATE_NAME)
                .contentType(MediaType.APPLICATION_JSON).content(PRODUCT_SUPPLIER_FOR_SAVING))
                .andExpect(status().isMethodNotAllowed()).andReturn();
    }

    @Test
    public void testSaveOrUpdateEntity() throws Exception {//NOSONAR

        mockMvc.perform(post(PRODUCT_SUPPLIER_URL + AbstractControllerTesting.SAVE_OR_UPDATE_NAME)
                .contentType(MediaType.APPLICATION_JSON).content(PRODUCT_SUPPLIER_FOR_SAVING))
                .andExpect(status().isMethodNotAllowed()).andReturn();
    }

    @Ignore//NOSONAR Todo: fix test for it not to be ignored
    @Test
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void testDeleteEntity() throws Exception {//NOSONAR

        int entitiesCountBefore = productSupplierService.getEntities().size();
        String entityJson = new ObjectMapper().writeValueAsString(
                productSupplierDTOService.convert(productSupplierService.getEntities().get(0))
        );
        mockMvc.perform(post(PRODUCT_SUPPLIER_URL + AbstractControllerTesting.DELETE_NAME)
                .contentType(MediaType.APPLICATION_JSON).content(entityJson))
                .andExpect(status().isOk()).andReturn();

        int entitiesCountAfter = productSupplierService.getEntities().size();

        assertEquals(entitiesCountBefore - 1, entitiesCountAfter);
    }
}
