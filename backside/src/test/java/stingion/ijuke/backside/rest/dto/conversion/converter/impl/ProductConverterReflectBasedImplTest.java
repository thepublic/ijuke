/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.rest.dto.conversion.converter.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.services.ProductService;
import stingion.ijuke.backside.rest.dto.conversion.converter.ProductConverter;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Map;

@Test
@ContextConfiguration("classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class ProductConverterReflectBasedImplTest extends AbstractConverterReflectBasedImplTesting<Product, ProductDTO> {

    private static final int PRODUCT_ID = 1;

    private static final int DEPTH = 0;

    private static final boolean IS_LAZY = true;

    private static final Map<String, String> EXPECTED_PRODUCT_DTO_TO_STRING_MAP = ImmutableMap.of(
        "convertByEntity",                        "ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=null, productSuppliers=null)",//NOSONAR
        "convertByEntityWithLazyObjects",         "ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=[OrderingDTO(id=2, description=Deliver to Kharkiv, Martova 15, status=PENDING, product=ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=[null], productSuppliers=[null]), creditCard=CreditCardDTO(id=4, number=6541296068298653, type=VISA, bank=BankDTO(id=4, name=Plaza, creditCards=null), contact=ContactDTO(id=1, firstName=Ivan, middleName=Spiridonovich, lastName=Kvas, email=ivansk@ijuke.org, phoneNumber=0727176495, birthDay=1982-09-28 12:52:00.0, creditCards=null, gender=MALE), orderings=null))], productSuppliers=[ProductSupplierDTO(pk=ProductSupplierIdDTO(product=ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=[null], productSuppliers=[null]), supplier=SupplierDTO(id=1, name=Mistin, productSuppliers=null))), ProductSupplierDTO(pk=ProductSupplierIdDTO(product=ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=[null], productSuppliers=[null]), supplier=SupplierDTO(id=2, name=Highelectro, productSuppliers=null))), ProductSupplierDTO(pk=ProductSupplierIdDTO(product=ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=[null], productSuppliers=[null]), supplier=SupplierDTO(id=3, name=Nikita Borinko, productSuppliers=null)))])",
        "convertByEntityAndDepth",                "ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=null, productSuppliers=null)",
        "convertByEntityWithLazyObjectsAndDepth", "ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=[null], productSuppliers=[null])",
        "convertByEntityAndDepthAndLazy",         "ProductDTO(id=1, name=Phone Acer V370, price=180.5, orderings=null, productSuppliers=null)"
    );

    @Autowired
    private ProductConverter productConverter;

    @Autowired
    private ProductService productService;


    @BeforeMethod(firstTimeOnly = true)
    public void initHSQLDB() throws SQLException, IOException, URISyntaxException {
        super.setEntity(productService.getEntityWithLazyObjectsById(PRODUCT_ID));
        super.setDepth(DEPTH);
        super.setLazy(IS_LAZY);
        super.setExpectedEntityDTOToStringMap(EXPECTED_PRODUCT_DTO_TO_STRING_MAP);
        super.setEntityConverter(productConverter);
    }
}
