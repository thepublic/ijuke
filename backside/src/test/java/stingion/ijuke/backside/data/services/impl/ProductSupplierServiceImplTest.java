/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import com.google.common.collect.Sets;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.ProductService;
import stingion.ijuke.backside.data.services.ProductSupplierService;
import stingion.ijuke.backside.data.services.SupplierService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
@TransactionConfiguration
public class ProductSupplierServiceImplTest {

    @Autowired
    private ProductSupplierService productSupplierService;

    @Autowired
    private ProductService productService;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private Product defaultProduct1;

    @Autowired
    private Product defaultProduct2;

    @Autowired
    private Supplier defaultSupplier1;

    @Autowired
    private Supplier defaultSupplier2;

    @Autowired
    private Supplier defaultSupplier3;

    private ProductSupplier product1Supplier1;

    @Before
    public void init() throws SQLException, IOException, URISyntaxException, NoSuchMethodException {//NOSONAR

        supplierService.createNewEntity(defaultSupplier1);
        supplierService.createNewEntity(defaultSupplier2);
        supplierService.createNewEntity(defaultSupplier3);


        product1Supplier1 = new ProductSupplier(defaultProduct1, defaultSupplier1);
        ProductSupplier product1Supplier2 = new ProductSupplier(defaultProduct1, defaultSupplier2);
        ProductSupplier product1Supplier3 = new ProductSupplier(defaultProduct1, defaultSupplier3);
        defaultProduct1.setProductSuppliers(Sets.newHashSet(
                product1Supplier1, product1Supplier2, product1Supplier3
        ));

        ProductSupplier product2Supplier1 = new ProductSupplier(defaultProduct2, defaultSupplier1);
        ProductSupplier product2Supplier3 = new ProductSupplier(defaultProduct2, defaultSupplier3);
        defaultProduct2.setProductSuppliers(Sets.newHashSet(
                product2Supplier1, product2Supplier3
        ));

        productService.createNewEntity(defaultProduct1);
    }

    @After
    public void destroy() {
        productService.deleteEntity(defaultProduct1);

        supplierService.deleteEntity(defaultSupplier1);
        supplierService.deleteEntity(defaultSupplier2);
        supplierService.deleteEntity(defaultSupplier3);
    }

    @Test
    @Transactional
    public void testCreateNewEntity() {
        int productSupplierCountBefore = productSupplierService.getEntities().size();
        productService.createNewEntity(defaultProduct2);
        int productSupplierCountAfter = productSupplierService.getEntities().size();

        assertEquals(productSupplierCountBefore + 2, productSupplierCountAfter);
    }

    @Test
    public void testGetEntityById() {
        assertEquals(product1Supplier1, productSupplierService.getEntityById(product1Supplier1.getPk()));
    }

    @Test(expected = UnsupportedOperationException.class)
    @Transactional
    public void testUpdateEntity() {
        productSupplierService.updateEntity(product1Supplier1);
    }

    @Test(expected = UnsupportedOperationException.class)
    @Transactional
    public void testSaveOrUpdateEntity() {
        productSupplierService.saveOrUpdateEntity(product1Supplier1);
    }

    @Test
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void testDeleteEntity() {
        int productSupplierCountBefore = productSupplierService.getEntities().size();
        productSupplierService.deleteEntity(product1Supplier1);
        int productSupplierCountAfter = productSupplierService.getEntities().size();

        assertEquals(productSupplierCountBefore - 1, productSupplierCountAfter);
    }

    @Test
    public void testGetEntities() {
        assertTrue(productSupplierService.getEntities().contains(product1Supplier1));
    }

    @Test
    public void testGetEntityByProductAndSupplier() {
        assertEquals(product1Supplier1,
                     productSupplierService.getEntityByProductAndSupplier(defaultProduct1, defaultSupplier1));
    }
}
