/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.services.ProductService;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class ProductServiceImplTest extends AbstractServiceImplTesting<Product> {

    private static final String PROPERTY_NAME = "name";

    @Autowired
    private ProductService productService;

    @Autowired
    private Product defaultProduct1;

    @Autowired
    private Product defaultProduct2;

    @Autowired
    private Product defaultProduct3;

    @Before
    public void init() throws NoSuchMethodException {
        productService.createNewEntity(defaultProduct1);

        // Init super class for checking base functionality
        super.setDefaultEntity1(defaultProduct1);
        super.setDefaultEntity2(defaultProduct2);
        super.setDefaultEntity3(defaultProduct3);
        super.setEntityService(productService);
        super.setProperty(PROPERTY_NAME, String.class);
    }

    @Test
    public void testFindProductByName() throws Exception {
        assertEquals(defaultProduct1, productService.findProductByName(defaultProduct1.getName()));
    }

    @After
    public void destroy() {
        productService.deleteEntity(defaultProduct1);
    }
}
