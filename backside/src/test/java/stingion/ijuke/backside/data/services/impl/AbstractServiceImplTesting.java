/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import lombok.Setter;
import org.apache.commons.lang3.text.WordUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import stingion.ijuke.backside.data.services.AbstractService;

import javax.lang.model.type.PrimitiveType;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
@TransactionConfiguration(defaultRollback = false)
public class AbstractServiceImplTesting<T> {

    private static final String GET_ID_METHOD_NAME_DEFAULT = "getId";

    private String getIdMethodName = GET_ID_METHOD_NAME_DEFAULT;

    @Setter
    private AbstractService<T> entityService;

    @Setter
    private T defaultEntity1;

    @Setter
    private T defaultEntity2;

    @Setter
    private T defaultEntity3;

    private Class<T> entityClass;

    private Method getMethod;

    private Method setMethod;

    /**
     * Entity property for using of its setter and getter method
     *
     * @param propertyName property name
     * @param propertyType property type
     */
    public void setProperty(String propertyName, Class<?> propertyType) throws NoSuchMethodException {
        getMethod = getEntityClass().getMethod("get" + WordUtils.capitalize(propertyName));
        setMethod = getEntityClass().getMethod("set" + WordUtils.capitalize(propertyName), propertyType);
    }

    /**
     * Name of id property of entity for use its get method
     */
    public void setEntityIdName(String idName) {
        this.getIdMethodName = "get" + WordUtils.capitalize(idName);
    }

    public Class<T> getEntityClass() {
        if (entityClass == null) {
            ParameterizedType pType = (ParameterizedType) getClass().getGenericSuperclass();
            this.entityClass = (Class<T>) pType.getActualTypeArguments()[0];
        }

        return entityClass;
    }

    private Serializable invokeGetIdMethod(Object entity) throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException {
        return (Serializable) getEntityClass().getMethod(getIdMethodName).invoke(entity);
    }

    @Test
    @Transactional
    @Rollback
    public void testCreateNewEntity() {
        int entitiesCountBefore = entityService.getEntities().size();
        entityService.createNewEntity(defaultEntity2);
        int entitiesCountAfter = entityService.getEntities().size();

        assertEquals(entitiesCountBefore + 1, entitiesCountAfter);
    }

    @Test
    public void testGetEntityById() throws Exception {//NOSONAR
        assertEquals(defaultEntity1, entityService.getEntityById(invokeGetIdMethod(defaultEntity1)));
    }

    @Test
    @Transactional
    @Rollback
    public void testUpdateEntity() throws Exception {//NOSONAR
        setMethod.invoke(defaultEntity1, getMethod.invoke(defaultEntity2));

        entityService.updateEntity(defaultEntity1);
        assertEquals(defaultEntity1, entityService.getEntityById(invokeGetIdMethod(defaultEntity1)));
        assertEquals(getMethod.invoke(defaultEntity2),
                getMethod.invoke(entityService.getEntityById(invokeGetIdMethod(defaultEntity1))));
    }

    @Test
    @Transactional
    @Rollback
    public void testSaveOrUpdateEntity() throws Exception {//NOSONAR
        int entitiesCountBefore = entityService.getEntities().size();
        setMethod.invoke(defaultEntity1, getMethod.invoke(defaultEntity2));
        entityService.saveOrUpdateEntity(defaultEntity1);
        int entitiesCountAfter = entityService.getEntities().size();

        assertEquals(getMethod.invoke(defaultEntity2),
                getMethod.invoke(entityService.getEntityById(invokeGetIdMethod(defaultEntity1))));
        assertEquals(entitiesCountBefore, entitiesCountAfter);

        entitiesCountBefore = entityService.getEntities().size();
        entityService.saveOrUpdateEntity(defaultEntity3);
        entitiesCountAfter = entityService.getEntities().size();

        assertEquals(entitiesCountBefore + 1, entitiesCountAfter);
    }

    @Test
    @Transactional
    @Rollback
    public void testDeleteEntity() {
        int entitiesCountBefore = entityService.getEntities().size();
        entityService.deleteEntity(defaultEntity1);
        int entitiesCountAfter = entityService.getEntities().size();

        assertEquals(entitiesCountBefore - 1, entitiesCountAfter);
    }

    @Test
    public void testGetEntities() {
        assertTrue(entityService.getEntities().contains(defaultEntity1));
    }

    @Test
    public void testGetEntityWithLazyObjectsById() throws Exception {//NOSONAR
        T entityWithLazyObjects = entityService.getEntityWithLazyObjectsById(invokeGetIdMethod(defaultEntity1));
        for (Method method : getEntityClass().getDeclaredMethods()) {
            Object methodInvocationResult;
            if (method.getParameterTypes().length == 0 &&
                    !((methodInvocationResult = method.invoke(entityWithLazyObjects)) instanceof PrimitiveType) &&//NOSONAR
                    !(methodInvocationResult instanceof Enum)) {
                methodInvocationResult.getClass().getDeclaredMethod("toString").invoke(methodInvocationResult);
            }
        }
    }

    @Test
    @Transactional
    @Rollback
    public void testMergeEntity() {
        int entitiesCountBefore = entityService.getEntities().size();

        T entityMerged = entityService.mergeEntity(defaultEntity1);
        entityService.mergeEntity(entityMerged);

        int entitiesCountAfter = entityService.getEntities().size();

        assertEquals(entitiesCountBefore, entitiesCountAfter);
    }
}
