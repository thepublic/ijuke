/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.model.Ordering;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.services.CreditCardService;
import stingion.ijuke.backside.data.services.OrderingService;
import stingion.ijuke.backside.data.services.ProductService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class OrderingServiceImplTest extends AbstractServiceImplTesting<Ordering> {

    private static final String PROPERTY_NAME = "description";

    @Autowired
    private ProductService productService;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private OrderingService orderingService;

    @Autowired
    private Product defaultProduct1;

    @Autowired
    private Product defaultProduct2;

    @Autowired
    private Product defaultProduct3;

    @Autowired
    private CreditCard defaultCreditCard1;

    @Autowired
    private CreditCard defaultCreditCard2;

    @Autowired
    private CreditCard defaultCreditCard3;

    @Autowired
    private Ordering defaultOrdering1;

    @Autowired
    private Ordering defaultOrdering2;

    @Autowired
    private Ordering defaultOrdering3;

    @Before
    public void init() throws SQLException, IOException, URISyntaxException, NoSuchMethodException {

        productService.createNewEntity(defaultProduct1);
        productService.createNewEntity(defaultProduct2);
        productService.createNewEntity(defaultProduct3);

        creditCardService.createNewEntity(defaultCreditCard1);
        creditCardService.createNewEntity(defaultCreditCard2);
        creditCardService.createNewEntity(defaultCreditCard3);

        defaultOrdering1.setProduct(defaultProduct2);
        defaultOrdering1.setCreditCard(defaultCreditCard3);

        defaultOrdering2.setProduct(defaultProduct1);
        defaultOrdering2.setCreditCard(defaultCreditCard1);

        defaultOrdering3.setProduct(defaultProduct3);
        defaultOrdering3.setCreditCard(defaultCreditCard2);

        orderingService.createNewEntity(defaultOrdering1);

        // Init super class for checking base functionality
        super.setDefaultEntity1(defaultOrdering1);
        super.setDefaultEntity2(defaultOrdering2);
        super.setDefaultEntity3(defaultOrdering3);
        super.setEntityService(orderingService);
        super.setProperty(PROPERTY_NAME, String.class);
    }

    @After
    public void destroy() {
        orderingService.deleteEntity(defaultOrdering1);

        productService.deleteEntity(defaultProduct1);
        productService.deleteEntity(defaultProduct2);
        productService.deleteEntity(defaultProduct3);

        creditCardService.deleteEntity(defaultCreditCard1);
        creditCardService.deleteEntity(defaultCreditCard2);
        creditCardService.deleteEntity(defaultCreditCard3);
    }
}
