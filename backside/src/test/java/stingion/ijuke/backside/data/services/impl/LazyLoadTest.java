/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.ProductSupplier;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.ProductService;
import stingion.ijuke.backside.data.services.SupplierService;

import java.util.Set;

import static org.testng.AssertJUnit.assertEquals;

@Test
@ContextConfiguration("classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class LazyLoadTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ProductService productService;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private Product defaultProduct1;

    @Autowired
    private Product defaultProduct2;

    @Autowired
    private Product defaultProduct3;

    @Autowired
    private Supplier defaultSupplier1;

    @Autowired
    private Supplier defaultSupplier2;

    @Autowired
    private Supplier defaultSupplier3;


    @BeforeGroups(groups = {"suppliersOfProduct", "productsOfSupplier"})
    public void init() throws NoSuchMethodException {

        supplierService.createNewEntity(defaultSupplier1);
        supplierService.createNewEntity(defaultSupplier2);
        supplierService.createNewEntity(defaultSupplier3);


        ProductSupplier product1Supplier1 = new ProductSupplier(defaultProduct1, defaultSupplier1);
        ProductSupplier product1Supplier2 = new ProductSupplier(defaultProduct1, defaultSupplier2);
        ProductSupplier product1Supplier3 = new ProductSupplier(defaultProduct1, defaultSupplier3);
        defaultProduct1.setProductSuppliers(Sets.newHashSet(
                product1Supplier1, product1Supplier2, product1Supplier3
        ));

        ProductSupplier product2Supplier1 = new ProductSupplier(defaultProduct2, defaultSupplier1);
        ProductSupplier product2Supplier3 = new ProductSupplier(defaultProduct2, defaultSupplier3);
        defaultProduct2.setProductSuppliers(Sets.newHashSet(
                product2Supplier1, product2Supplier3
        ));


        productService.createNewEntity(defaultProduct1);
        productService.createNewEntity(defaultProduct2);
        productService.createNewEntity(defaultProduct3);
    }

    @AfterGroups(groups = {"suppliersOfProduct", "productsOfSupplier"})
    public void destroy() throws NoSuchMethodException {

        productService.deleteEntity(defaultProduct1);
        productService.deleteEntity(defaultProduct2);
        productService.deleteEntity(defaultProduct3);

        supplierService.deleteEntity(defaultSupplier1);
        supplierService.deleteEntity(defaultSupplier2);
        supplierService.deleteEntity(defaultSupplier3);
    }

    @Test(groups = "suppliersOfProduct")
    public void testGetSuppliers() throws Exception {
        Set<Supplier> expectedDefaultProduct1Suppliers = Sets.newHashSet(defaultSupplier1, defaultSupplier2,
                defaultSupplier3);
        Set<Supplier> actualDefaultProduct1Suppliers = productService.getSuppliers(defaultProduct1);
        assertEquals(expectedDefaultProduct1Suppliers, actualDefaultProduct1Suppliers);

        Set<Supplier> expectedDefaultProduct2Suppliers = Sets.newHashSet(defaultSupplier1, defaultSupplier3);
        Set<Supplier> actualDefaultProduct2Suppliers = productService.getSuppliers(defaultProduct2);
        assertEquals(expectedDefaultProduct2Suppliers, actualDefaultProduct2Suppliers);

        Set<Supplier> expectedDefaultProduct3Suppliers = Sets.newHashSet();
        Set<Supplier> actualDefaultProduct3Suppliers = productService.getSuppliers(defaultProduct3);
        assertEquals(expectedDefaultProduct3Suppliers, actualDefaultProduct3Suppliers);
    }

    @Test(groups = "productsOfSupplier")
    public void testGetProducts() throws Exception {
        Set<Product> expectedDefaultSupplier1Products = Sets.newHashSet(defaultProduct1, defaultProduct2);
        Set<Product> actualDefaultSupplier1Products = supplierService.getProducts(defaultSupplier1);
        assertEquals(expectedDefaultSupplier1Products, actualDefaultSupplier1Products);

        Set<Product> expectedDefaultSupplier2Products = Sets.newHashSet(defaultProduct1);
        Set<Product> actualDefaultSupplier2Products = supplierService.getProducts(defaultSupplier2);
        assertEquals(expectedDefaultSupplier2Products, actualDefaultSupplier2Products);

        Set<Product> expectedDefaultSupplier3Products = Sets.newHashSet(defaultProduct1, defaultProduct2);
        Set<Product> actualDefaultSupplier3Products = supplierService.getProducts(defaultSupplier3);
        assertEquals(expectedDefaultSupplier3Products, actualDefaultSupplier3Products);
    }
}
