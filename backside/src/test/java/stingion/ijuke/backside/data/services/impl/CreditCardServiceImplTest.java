/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.model.CreditCard;
import stingion.ijuke.backside.data.services.BankService;
import stingion.ijuke.backside.data.services.ContactService;
import stingion.ijuke.backside.data.services.CreditCardService;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class CreditCardServiceImplTest extends AbstractServiceImplTesting<CreditCard> {

    private static final String PROPERTY_NAME = "number";

    @Autowired
    private BankService bankService;

    @Autowired
    private ContactService contactService;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private Bank defaultBank1;

    @Autowired
    private Bank defaultBank2;

    @Autowired
    private Bank defaultBank3;

    @Autowired
    private Contact defaultContact1;

    @Autowired
    private Contact defaultContact2;

    @Autowired
    private Contact defaultContact3;

    @Autowired
    private CreditCard defaultCreditCard1;

    @Autowired
    private CreditCard defaultCreditCard2;

    @Autowired
    private CreditCard defaultCreditCard3;

    @Before
    public void init() throws NoSuchMethodException {

        bankService.createNewEntity(defaultBank1);
        bankService.createNewEntity(defaultBank2);
        bankService.createNewEntity(defaultBank3);

        contactService.createNewEntity(defaultContact1);
        contactService.createNewEntity(defaultContact2);
        contactService.createNewEntity(defaultContact3);

        defaultCreditCard1.setBank(defaultBank3);
        defaultCreditCard1.setContact(defaultContact1);

        defaultCreditCard2.setBank(defaultBank1);
        defaultCreditCard2.setContact(defaultContact3);

        defaultCreditCard3.setBank(defaultBank2);
        defaultCreditCard3.setContact(defaultContact3);

        creditCardService.createNewEntity(defaultCreditCard1);

        // Init super class for checking base functionality
        super.setDefaultEntity1(defaultCreditCard1);
        super.setDefaultEntity2(defaultCreditCard2);
        super.setDefaultEntity3(defaultCreditCard3);
        super.setEntityService(creditCardService);
        super.setProperty(PROPERTY_NAME, Long.class);
    }

    @Test
    public void testFindCreditCardsByContact() throws Exception {
        assertEquals(1, creditCardService.findCreditCardsByContact(defaultContact1).size());
        assertEquals(defaultCreditCard1, creditCardService.findCreditCardsByContact(defaultContact1).get(0));
    }

    @Test
    public void testFindCreditCardsByBank() throws Exception {
        assertEquals(1, creditCardService.findCreditCardsByBank(defaultBank3).size());
        assertEquals(defaultCreditCard1, creditCardService.findCreditCardsByBank(defaultBank3).get(0));
    }

    @Test
    public void testFindCreditCardsByBankName() throws Exception {
        assertEquals(1, creditCardService.findCreditCardsByBankName(defaultBank3.getName()).size());
        assertEquals(defaultCreditCard1, creditCardService.findCreditCardsByBankName(defaultBank3.getName()).get(0));
    }

    @Test
    public void testFindCreditCardsByContactEmail() throws Exception {
        assertEquals(1, creditCardService.findCreditCardsByContactEmail(defaultContact1.getEmail()).size());
        assertEquals(defaultCreditCard1,
                creditCardService.findCreditCardsByContactEmail(defaultContact1.getEmail()).get(0));
    }

    @After
    public void destroy() {
        creditCardService.deleteEntity(defaultCreditCard1);

        bankService.deleteEntity(defaultBank1);
        bankService.deleteEntity(defaultBank2);
        bankService.deleteEntity(defaultBank3);

        contactService.deleteEntity(defaultContact1);
        contactService.deleteEntity(defaultContact2);
        contactService.deleteEntity(defaultContact3);
    }
}
