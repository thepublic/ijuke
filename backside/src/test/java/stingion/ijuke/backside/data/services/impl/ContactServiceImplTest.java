/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.Contact;
import stingion.ijuke.backside.data.services.ContactService;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class ContactServiceImplTest extends AbstractServiceImplTesting<Contact> {

    private static final String PROPERTY_NAME = "lastName";

    @Autowired
    private ContactService contactService;

    @Autowired
    private Contact defaultContact1;

    @Autowired
    private Contact defaultContact2;

    @Autowired
    private Contact defaultContact3;

    @Before
    public void init() throws NoSuchMethodException {
        contactService.createNewEntity(defaultContact1);

        // Init super class for checking base functionality
        super.setDefaultEntity1(defaultContact1);
        super.setDefaultEntity2(defaultContact2);
        super.setDefaultEntity3(defaultContact3);
        super.setEntityService(contactService);
        super.setProperty(PROPERTY_NAME, String.class);
    }

    @Test
    public void testFindContactByEmail() throws Exception {
        assertEquals(defaultContact1, contactService.findContactByEmail(defaultContact1.getEmail()));
    }

    @After
    public void destroy() {
        contactService.deleteEntity(defaultContact1);
    }
}
