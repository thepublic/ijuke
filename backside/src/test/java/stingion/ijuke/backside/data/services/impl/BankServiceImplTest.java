/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.Bank;
import stingion.ijuke.backside.data.services.BankService;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class BankServiceImplTest extends AbstractServiceImplTesting<Bank> {

    private static final String PROPERTY_NAME = "name";

    @Autowired
    private BankService bankService;

    @Autowired
    private Bank defaultBank1;

    @Autowired
    private Bank defaultBank2;

    @Autowired
    private Bank defaultBank3;

    @Before
    public void init() throws NoSuchMethodException {

        bankService.createNewEntity(defaultBank1);

        // Init super class for checking base functionality
        super.setDefaultEntity1(defaultBank1);
        super.setDefaultEntity2(defaultBank2);
        super.setDefaultEntity3(defaultBank3);
        super.setEntityService(bankService);
        super.setProperty(PROPERTY_NAME, String.class);
    }

    @Test
    public void testFindBankByName() throws Exception {
        assertEquals(defaultBank1, bankService.findBankByName(defaultBank1.getName()));
    }

    @After
    public void destroy() {
        bankService.deleteEntity(defaultBank1);
    }
}
