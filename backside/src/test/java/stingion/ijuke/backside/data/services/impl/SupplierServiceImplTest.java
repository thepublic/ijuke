/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.backside.data.services.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.backside.data.model.Product;
import stingion.ijuke.backside.data.model.Supplier;
import stingion.ijuke.backside.data.services.ProductService;
import stingion.ijuke.backside.data.services.SupplierService;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/contexts-bundle.xml")
public class SupplierServiceImplTest extends AbstractServiceImplTesting<Supplier> {

    private static final String PROPERTY_NAME = "name";

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private ProductService productService;

    @Autowired
    private Supplier defaultSupplier1;

    @Autowired
    private Supplier defaultSupplier2;

    @Autowired
    private Supplier defaultSupplier3;

    @Autowired
    private Product defaultProduct1;

    @Before
    public void init() throws NoSuchMethodException {

        supplierService.createNewEntity(defaultSupplier1);

        // Init super class for checking base functionality
        super.setDefaultEntity1(defaultSupplier1);
        super.setDefaultEntity2(defaultSupplier2);
        super.setDefaultEntity3(defaultSupplier3);
        super.setEntityService(supplierService);
        super.setProperty(PROPERTY_NAME, String.class);
    }

    @Test
    public void testFindSupplierByName() throws Exception {
        assertEquals(defaultSupplier1, supplierService.findSupplierByName(defaultSupplier1.getName()));
    }

    @After
    public void destroy() {
        supplierService.deleteEntity(defaultSupplier1);
    }
}
