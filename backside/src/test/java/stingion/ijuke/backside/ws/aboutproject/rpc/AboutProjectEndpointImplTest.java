package stingion.ijuke.backside.ws.aboutproject.rpc;

import com.google.common.collect.Lists;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/webservices.xml")
public class AboutProjectEndpointImplTest extends TestCase {

    @Autowired
    AboutProjectEndpoint aboutProjectEndpoint;

    @Before
    public void mockInit() {
        WebServiceContext wsContext = ((AboutProjectEndpointImpl) aboutProjectEndpoint).getWsContext();
        MessageContext mctx = mock(MessageContext.class);
        Map httpHeaders = mock(Map.class);

        when(wsContext.getMessageContext()).thenReturn(mctx);
        when(mctx.get(MessageContext.HTTP_REQUEST_HEADERS)).thenReturn(httpHeaders);
        when(httpHeaders.get("username")).thenReturn(Lists.newArrayList(AboutProjectEndpointImpl.ACCESS_USER));
        when(httpHeaders.get("password")).thenReturn(Lists.newArrayList(AboutProjectEndpointImpl.ACCESS_PASS));
    }

    @Test
    public void testGetAboutProject() throws Exception {
        String expected = "backside module is a main back-end part of \"ijuke\" project";
        String actual = aboutProjectEndpoint.getAboutProject();
        assertEquals(expected, actual);
    }
}
