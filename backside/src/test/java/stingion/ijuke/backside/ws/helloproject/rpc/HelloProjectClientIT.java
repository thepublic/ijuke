package stingion.ijuke.backside.ws.helloproject.rpc;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.utils.ws.EndpointPublisher;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created under not commercial project
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/webservices.xml")
public class HelloProjectClientIT extends HelloProjectEndpointImplTest {

    @Autowired
    private EndpointPublisher publisher;
    
    @Value("${hello.project.endpoint.namespaceURI}")
    private String namespaceURI;

    @Value("${hello.project.endpoint.localPart}")
    private String localPart;

    @Before
    public void initClass() throws Exception {

        publisher.start();

        URL url = new URL(publisher.getEndpointURL() + "?wsdl");
        //1st argument service URI, refer to wsdl document above
        //2nd argument is service name, refer to wsdl document above
        QName qname = new QName(namespaceURI, localPart);
        Service service = Service.create(url, qname);

        service.setHandlerResolver(new HandlerResolver() {
            @Override
            public List<Handler> getHandlerChain(PortInfo portInfo) {
                List<Handler> handlerChain = new ArrayList<>();
                MacAddressInjectHandler hh = new MacAddressInjectHandler();
                handlerChain.add(hh);
                return handlerChain;
            }
        });

        helloProjectEndpoint = service.getPort(HelloProjectEndpoint.class);
    }

    @After
    public void closeClass() throws Exception {
        publisher.stop();
    }

    @Ignore
    @Test
    @Override
    public void testGetHelloWithParams() {
        //It's overrided cause of not unique method names
    }
}
