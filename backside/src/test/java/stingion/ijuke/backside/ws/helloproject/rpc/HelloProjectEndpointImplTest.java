package stingion.ijuke.backside.ws.helloproject.rpc;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.activation.DataHandler;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/webservices.xml")
public class HelloProjectEndpointImplTest {

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected HelloProjectEndpoint helloProjectEndpoint;

    @Test
    public void testGetHello() {
        String expected = "Hello, this is \"backside\" module of \"ijuke\" project";
        String actual = helloProjectEndpoint.getHello();

        assertEquals(expected, actual);
    }

    @Test
    public void testGetHelloWithParams() {
        String expected = "Hello, USER, this is \"backside\" module of \"ijuke\" project";
        String actual = helloProjectEndpoint.getHello("USER");

        assertEquals(expected, actual);
    }

    @Test
    public void testGetHelloWithName() {
        String expected = "Hello, USER, this is \"backside\" module of \"ijuke\" project";
        String actual = helloProjectEndpoint.getHelloWithName("USER");

        assertEquals(expected, actual);
    }

    @Test
    public void testDownloadModuleImage() throws IOException {
        Path imageFilePath = Paths.get(getClass().getClassLoader()
                .getResource(HelloProjectEndpointImpl.MODULE_IMAGE_FILE_REL_PATH).getPath());
        byte[] imageDataExpected = Files.readAllBytes(imageFilePath);

        DataHandler downloadModuleImage = helloProjectEndpoint.downloadModuleImage();
        InputStream dataHandlerInputStream = downloadModuleImage.getInputStream();
        byte[] imageDataActual = IOUtils.toByteArray(dataHandlerInputStream);

        assertArrayEquals(imageDataExpected, imageDataActual);
    }

    @Test
    public void testUploadModuleImage() throws IOException {
        String imageFileNameForUpload = "uploadImage.png";
        Path imageFilePath = Paths.get(getClass().getClassLoader()
                .getResource(HelloProjectEndpointImpl.MODULE_IMAGE_FILE_REL_PATH).getPath());
        byte[] imageFileDataForUpload = Files.readAllBytes(imageFilePath);

        SoapFile soapFile = new SoapFile(imageFileNameForUpload, imageFileDataForUpload);

        String actualResponse = helloProjectEndpoint.uploadModuleImage(soapFile);
        String expectedResponse = "Upload successful";

        assertEquals(expectedResponse, actualResponse);
    }
}
