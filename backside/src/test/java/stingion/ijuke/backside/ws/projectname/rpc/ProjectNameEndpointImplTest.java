package stingion.ijuke.backside.ws.projectname.rpc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/backside/spring-test/webservices.xml")
public class ProjectNameEndpointImplTest {

    @Autowired
    ProjectNameEndpoint projectNameEndpoint;

    @Test
    public void testGetProjectName() {
        String expected = "this is backside module of ijuke project";
        String actual = projectNameEndpoint.getProjectName();
        assertEquals(expected, actual);
    }
}