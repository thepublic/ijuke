alter table contact add column gender varchar(15) DEFAULT NULL;

update contact set gender = 'MALE' where email = 'ivansk@ijuke.org';
update contact set gender = 'FEMALE' where email = 'ajqurie@ijuke.org';
