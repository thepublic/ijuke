create table message (
  id int not null identity,
  "from" varchar(50),
  "to" varchar(50),
  cc varchar(50),
  bcc varchar(50),
  subject varchar(50),
  "text" longvarchar,
  date timestamp
);

alter table attachment add column message_id int;
alter table attachment add foreign key (message_id) references message;

-- -- ----- MESSAGE -----
insert into message ("from", "to", subject, "text", date)
               values ('ivansk@ijuke.org', 'ajqurie@ijuke.org', 'Hi, Ann!', 'How are you? \\n Where are you?',
                       '2015-01-10 12:52:00.000000');
insert into message ("from", "to", subject, "text", date)
               values ('ivansk@ijuke.org', 'ajqurie@ijuke.org', 'Hi, Ann!', 'Go for a walk!',
                       '2015-01-20 13:00:00.000000');
insert into message ("from", "to", subject, "text", date)
               values ('ajqurie@ijuke.org', 'ivansk@ijuke.org', 'Hi, Kvas!', 'Ok',
                       '2015-01-20 13:30:00.000000');
-- -- ----- ATTACHMENT -----
insert into attachment (fileName, size, contentType, message_id)
                values ('/home/some/int for test.txt', '128000', 'text/plain', 1);
insert into attachment (fileName, size, contentType, message_id)
                values ('/home/some/some file for test too.xml', '512', 'application/xml', 1);
insert into attachment (fileName, size, contentType, message_id)
                values ('/home/some/another.xml', '128', 'application/xml', 2);
