/* Fill bank */
insert into bank values (1, 'Imperial Bank');
insert into bank values (3, 'Cristal');
insert into bank (name) values ('Plaza');

/* Fill contact */
insert into contact values (1, 'Ivan', 'Spiridonovich', 'Kvas', '1982-09-28 12:52:00.000000', 'ivansk@ijuke.org', '0727176495');
insert into contact (first_name, middle_name, last_name, birth_day, email, phone_number)
values ('Ann', 'John', 'Qurie', '1993-09-28 00:00:00.000000', 'ajqurie@ijuke.org', '0727789495');

/* Fill credit_card */
insert into credit_card (id, number, type, bank_id, contact_id)
values(
  1,
  6647486058945594,
  'VISA',
  (select id from bank where name = 'Imperial Bank'),
  (select id from contact where email = 'ivansk@ijuke.org')
);
insert into credit_card (number, type, bank_id, contact_id)
values(
  2237435131126577,
  'VISA',
  (select id from bank where name = 'Cristal'),
  (select id from contact where email = 'ajqurie@ijuke.org')
);
insert into credit_card (number, type, bank_id, contact_id)
values(
  1123729594138004,
  'MASTER_CARD',
  (select id from bank where name = 'Imperial Bank'),
  (select id from contact where email = 'ajqurie@ijuke.org')
);
insert into credit_card (number, type, bank_id, contact_id)
values(
  6541296068298653,
  'VISA',
  (select id from bank where name = 'Plaza'),
  (select id from contact where email = 'ivansk@ijuke.org')
);

/* Fill product */
insert into product (id, name, price) values (1, 'Phone Acer V370', 180.50);
insert into product (name, price) values ('Laptop Apple MacBook Fly', 1500);
insert into product (name, price) values ('Laptop Dell Inspiron 7550', 950);
insert into product (name, price) values ('E-book Amazon Kindle 6', 90.25);

/* Fill ordering */
insert into ordering (id, description, status, product_id, credit_card_id)
values (1, 'Deliver to Kharkiv, Melnyka 15/65', 'PENDING',
        (select id from product where name = 'E-book Amazon Kindle 6'),
        (select id from credit_card where number = 6647486058945594)
);
insert into ordering (description, status, product_id, credit_card_id)
values ('Deliver to Kharkiv, Martova 15', 'PENDING',
        (select id from product where name = 'Phone Acer V370'),
        (select id from credit_card where number = 6541296068298653)
);
insert into ordering (description, status, product_id, credit_card_id)
values ('Deliver to Kharkiv, Martova 15', 'CANCELLED',
        (select id from product where name = 'Laptop Apple MacBook Fly'),
        (select id from credit_card where number = 6541296068298653)
);
insert into ordering (description, status, product_id, credit_card_id)
values ('Deliver to Odessa, Kosara 22/11', 'PERFORMED',
        (select id from product where name = 'Laptop Apple MacBook Fly'),
        (select id from credit_card where number = 1123729594138004)
);

/* Fill ordering */
insert into supplier (id, name) values (1, 'Mistin');
insert into supplier (name) values ('Highelectro');
insert into supplier (name) values ('Nikita Borinko');

/* Fill product_supplier */

insert into product_supplier (product_id, supplier_id)
values (
  (select id from product where name  = 'Phone Acer V370'),
  (select id from supplier where name = 'Mistin')
);
insert into product_supplier (product_id, supplier_id)
values (
  (select id from product where name  = 'Phone Acer V370'),
  (select id from supplier where name = 'Nikita Borinko')
);
insert into product_supplier (product_id, supplier_id)
values (
  (select id from product where name  = 'Phone Acer V370'),
  (select id from supplier where name = 'Highelectro')
);

insert into product_supplier (product_id, supplier_id)
values (
  (select id from product where name  = 'Laptop Apple MacBook Fly'),
  (select id from supplier where name = 'Mistin')
);
insert into product_supplier (product_id, supplier_id)
values (
  (select id from product where name  = 'Laptop Apple MacBook Fly'),
  (select id from supplier where name = 'Highelectro')
);

insert into product_supplier (product_id, supplier_id)
values (
  (select id from product where name  = 'Laptop Dell Inspiron 7550'),
  (select id from supplier where name = 'Mistin')
);
insert into product_supplier (product_id, supplier_id)
values (
  (select id from product where name  = 'Laptop Dell Inspiron 7550'),
  (select id from supplier where name = 'Nikita Borinko')
);

insert into product_supplier (product_id, supplier_id)
values (
  (select id from product where name  = 'E-book Amazon Kindle 6'),
  (select id from supplier where name = 'Highelectro')
);
