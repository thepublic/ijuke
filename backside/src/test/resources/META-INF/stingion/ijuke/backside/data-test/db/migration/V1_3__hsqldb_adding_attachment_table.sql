create table attachment (
  id int not null identity,
  fileName varchar(250),
  size bigint,
  contentType varchar(50)
);
