create table bank (
  id int not null identity,
  name varchar(30),
  unique (name)
);

create table contact (
  id int not null identity,
  first_name varchar(30),
  middle_name varchar(30),
  last_name varchar(30),
  birth_day timestamp,
  email varchar(30),
  phone_number varchar(30),
  unique (email)
);

create table credit_card (
  id int not null identity,
  number bigint,
  type varchar(15),
  last_name varchar(30),
  bank_id int,
  contact_id int,
  foreign key (bank_id) references bank,
  foreign key (contact_id) references contact,
  unique (number)
);

create table product (
  id int not null identity,
  name varchar(50),
  price double,
  unique (name)
);

create table ordering (
  id int not null identity,
  status varchar(15),
  description varchar(100),
  product_id int,
  credit_card_id int,
  foreign key (product_id) references product,
  foreign key (credit_card_id) references credit_card
);

create table supplier (
  id int not null identity,
  name varchar(30),
  unique (name)
);

create table product_supplier (
  product_id int,
  supplier_id int,
  primary key (product_id, supplier_id),
  foreign key (product_id) references product,
  foreign key (supplier_id)  references supplier
);
