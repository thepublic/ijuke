<%@include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:layout-render name="/WEB-INF/jsp/common/layout_folders.jsp" title="Message List" currentSection="MESSAGE_LIST">
    <s:layout-component name="body">
        Message list goes here

        <br/>
        <s:link beanclass="stingion.ijuke.frontside.web.action.MessageDetailsActionBean">
            Message details
        </s:link>

    </s:layout-component>
</s:layout-render>
