<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE log4j:configuration PUBLIC "-//LOGGER"
"http://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/xml/doc-files/log4j.dtd">

<html>
    <head>
        <title>View source - ${actionBean.filename}</title>
        <link rel="shortcut icon" href="${contextPath}/images/ijuke.png" type="image/x-icon">
    </head>
    <body>
        <p><u>View source - ${actionBean.filename}</u></p>
        <pre>${fn:escapeXml(actionBean.source)}</pre>
    </body>
</html>
