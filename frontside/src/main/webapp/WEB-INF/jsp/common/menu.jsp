<%@include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<c:forEach var="section" items="${actionBean.sections}">
    <c:choose>
        <c:when test="${section eq actionBean.currentSection}"><!-- (1) -->
            <span class="currentSection">${section.text}</span>
        </c:when>
        <c:otherwise>
            <s:link beanclass="${section.beanClass}" class="sectionLink"><!-- (2) -->
                ${section.text}
            </s:link>
        </c:otherwise>
    </c:choose>
</c:forEach>
