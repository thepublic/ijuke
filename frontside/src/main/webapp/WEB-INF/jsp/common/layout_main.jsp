<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-definition>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

    <html>
        <head>
            <link rel="icon" href="${contextPath}/images/ijuke.png" type="image/x-icon">
            <link rel="shortcut icon" href="${contextPath}/images/ijuke.png" type="image/x-icon">
            <title>${title}</title>
            <link rel="stylesheet" type="text/css" href="${contextPath}/styles/css/style.css">
        </head>

        <body>
            <div id="panel">
                <div id="header">
                    <span class="title">${title}</span>
                      <span class="menu">
                        <s:layout-component name="menu">
                            Welcome to Ijuke
                        </s:layout-component>
                      </span>
                </div>

                <s:messages/>

                <div id="body">
                    <s:layout-component name="body"/>
                </div>

                <div class="clear"></div>

                <div id="footer">
                    <div class="padded">
                        <!-- Footer goes here. -->

                        <!-- View source links just for convenience -->
                        <!--| -->
                        Source:
                        <s:link beanclass="stingion.ijuke.frontside.web.action.ViewSourceActionBean">
                            <s:param name="filename" value="<%= request.getRequestURI() %>"/>
                            JSP
                        </s:link> |
                        <s:link beanclass="stingion.ijuke.frontside.web.action.ViewSourceActionBean">
                            <s:param name="filename" value="${actionBean.getClass().name}"/>
                            Action Bean
                        </s:link>
                    </div>
                </div>
            </div>
        </body>

    </html>

</s:layout-definition>
