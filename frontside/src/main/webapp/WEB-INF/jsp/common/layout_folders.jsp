<%@include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<jsp:useBean class="stingion.ijuke.frontside.stripesbook.view.FoldersViewHelper" id="folders"/>

<s:layout-definition>
    <s:layout-render name="/WEB-INF/jsp/common/layout_menu.jsp" title="${title}" currentSection="${currentSection}">
        <s:layout-component name="body">

            <div id="folders">

                <d:table name="${folders.folders}">
                    <d:column property="name"/>
                    <d:column property="numberOfMessages" title="Messages"/>
                </d:table>

            </div>

            <div id="main">
                ${body}
            </div>
        </s:layout-component>
    </s:layout-render>
</s:layout-definition>
