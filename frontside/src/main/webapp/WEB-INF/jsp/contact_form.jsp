<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_menu.jsp" title="Contact Information" currentSection="CONTACT_LIST">
    <s:layout-component name="body">

        <s:form beanclass="stingion.ijuke.frontside.web.action.ContactFormActionBean">

            <s:errors/>

            <div><s:hidden name="contact.id"/></div>
            <table class="form">
                <tr>
                    <td><s:label for="contact.firstName"/>:</td>
                    <td><s:text name="contact.firstName"/></td>
                </tr>
                <tr>
                    <td><s:label for="contact.middleName"/>:</td>
                    <td><s:text name="contact.middleName"/></td>
                </tr>
                <tr>
                    <td><s:label for="contact.lastName"/>:</td>
                    <td><s:text name="contact.lastName"/></td>
                </tr>
                <tr>
                    <td><s:label for="contact.email"/>:</td>
                    <td><s:text name="contact.email"/></td>
                </tr>

                <tr>
                    <td><s:label for="contact.phoneNumber"/>:</td>
                    <td><s:text name="contact.phoneNumber" formatType="phoneNumber.parens"/></td>
                </tr>
                <tr>
                    <td><s:label for="contact.birthDay"/>:</td>
                    <td><s:text name="contact.birthDay" formatPattern="dd/MM/yyyy"/></td>
                </tr>
                <tr>
                    <td><s:label for="contact.gender"/>:</td>
                    <td class="field">
                        <c:forEach var="gender" items="${actionBean.genders}">
                            <s:radio name="contact.gender" value="${gender}"/>${gender}
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <s:submit name="save" value="Save"/>
                        <s:submit name="cancel" value="Cancel"/>
                    </td>
                </tr>
            </table>
        </s:form>

    </s:layout-component>
</s:layout-render>
