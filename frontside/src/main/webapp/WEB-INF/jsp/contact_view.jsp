<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_menu.jsp" title="Contact Information" currentSection="CONTACT_LIST">
    <s:layout-component name="body">
        <table class="view">
            <tr>
                <td class="label"><s:label for="contact.firstName"/>:</td>
                <td class="value">${actionBean.contact.firstName}</td>
            </tr>
            <tr>
                <td class="label"><s:label for="contact.middleName"/>:</td>
                <td class="value">${actionBean.contact.middleName}</td>
            </tr>
            <tr>
                <td class="label"><s:label for="contact.lastName"/>:</td>
                <td class="value">${actionBean.contact.lastName}</td>
            </tr>
            <tr>
                <td class="label"><s:label for="contact.email"/>:</td>
                <td class="value">${actionBean.contact.email}</td>
            </tr>
            <tr>
                <td class="label"><s:label for="contact.phoneNumber"/>:</td>
                <td class="value"><s:format value="${actionBean.contact.phoneNumber}"
                                            formatType="phoneNumber.parens"/></td>
            </tr>
            <tr>
                <td class="label"><s:label for="contact.birthDay"/>:</td>
                <td class="value">
                    <s:format value="${actionBean.contact.birthDay}" formatPattern="dd/MM/yyyy"/>
                </td>
            </tr>
            <tr>
                <td class="label"><s:label for="contact.gender"/>:</td>
                <td class="value">${actionBean.contact.gender}</td>
            </tr>
        </table>
        <p>
            <s:link beanclass="stingion.ijuke.frontside.web.action.ContactFormActionBean">
                <s:param name="contactId" value="${actionBean.contact.id}"/>
                Update
            </s:link> |
            <s:link beanclass="stingion.ijuke.frontside.web.action.ContactListActionBean">
                Back to List
            </s:link>
        </p>
    </s:layout-component>
</s:layout-render>
