<%@include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/jsp/common/layout_menu.jsp" title="Contact List" currentSection="CONTACT_LIST">
    <s:layout-component name="body">

        <s:link beanclass="stingion.ijuke.frontside.web.action.ContactFormActionBean">
            Create a New Contact
        </s:link>

        <d:table name="${actionBean.contacts}" id="contact" requestURI="" defaultsort="1" summary="Contact List Table">
            <d:column title="First name"  property="firstName"  sortable="true"/>
            <d:column title="Middle name" property="middleName" sortable="true"/>
            <d:column title="Last name"   property="lastName"   sortable="true"/>
            <d:column title="Email"       property="email"      sortable="true"/>
            <d:column title="Action">
                <s:link beanclass="stingion.ijuke.frontside.web.action.ContactListActionBean" event="view">
                    <s:param name="contactId" value="${contact.id}"/>
                    View
                </s:link> |
                <s:link beanclass="stingion.ijuke.frontside.web.action.ContactFormActionBean">
                    <s:param name="contactId" value="${contact.id}"/>
                    Update
                </s:link> |
                <s:link beanclass="stingion.ijuke.frontside.web.action.ContactListActionBean" event="delete"
                        onclick="return confirm('Delete ${contact}?');">
                    <s:param name="contactId" value="${contact.id}"/>
                    Delete
                </s:link>
            </d:column>
        </d:table>

    </s:layout-component>
</s:layout-render>
