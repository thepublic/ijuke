//For description.jsp
if (typeof(CONTEXT_ROOT) === 'undefined')//NOSONAR
    var CONTEXT_ROOT = "";

var FRONTSIDE_DESC_JSON_URL = CONTEXT_ROOT + '/js/frontside_description.json';

$(document).ready(function () {
    fillJsonData();
});

function fillJsonData() {
    $.getJSON(FRONTSIDE_DESC_JSON_URL, function (json) {
        $("#desc #artTitle").text(json.title);
        $("#desc #artContent").text(json.content);
    });
}
