package stingion.ijuke.frontside.stripesbook.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created under not commercial project
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Folder extends ModelBase {

    private String name;//NOSONAR
    private int numberOfMessages;//NOSONAR
}

