package stingion.ijuke.frontside.stripesbook.view;

import stingion.ijuke.frontside.stripesbook.dao.FolderDao;
import stingion.ijuke.frontside.stripesbook.model.Folder;
import stingion.ijuke.frontside.stripesbook.model.mock.MockFolderDao;

import java.util.List;

/**
 * Created under not commercial project
 */
public class FoldersViewHelper {
    private final FolderDao folderDao = MockFolderDao.getInstance();

    public List<Folder> getFolders() {
        return folderDao.read();
    }
}
