/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.stripesbook.model.mock;

import stingion.ijuke.frontside.stripesbook.dao.FolderDao;
import stingion.ijuke.frontside.stripesbook.model.Folder;
import stingion.ijuke.utils.exception.FireRuntimeException;

public class MockFolderDao extends MockDao<Folder> implements FolderDao
{
    private static final MockFolderDao instance = new MockFolderDao();

    private final Folder inbox = new Folder("Inbox", 24);
    private final Folder sent  = new Folder("Sent", 12);
    private final Folder trash = new Folder("Trash", 4);

    private Folder[] folders = { inbox, sent, trash };

    private MockFolderDao() {
        try {
            for (Folder folder : folders) {
                save(folder);
            }
        }
        catch (Exception exc) {
            throw new FireRuntimeException(exc);
        }
    }

    public static MockFolderDao getInstance() {
        return instance;
    }
}

