package stingion.ijuke.frontside.stripesbook.model.mock;

import stingion.ijuke.frontside.stripesbook.dao.Dao;
import stingion.ijuke.frontside.stripesbook.model.ModelBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created under not commercial project
 */
public abstract class MockDao<T extends ModelBase> implements Dao<T>
{
    private final List<T> list = new ArrayList<>();
    private final Map<Integer,T> map = new HashMap<>();
    private int nextId = 1;

    public void save(T object) {
        if (object != null) {
            if (object.getId() == null) {
                int id = nextId++;
                object.setId(id);
                list.add(object);
            }
            else {
                Integer id = object.getId();
                list.set(list.indexOf(map.get(id)), object);
            }
            map.put(object.getId(), object);
        }
    }

    public List<T> read() {
        List<T> objects = new ArrayList<>(list.size());
        for (T object : list) {
            T copy = copyObject(object);
            if (copy != null) {
                objects.add(copy);
            }
            else {
                objects.add(object);
            }
        }
        return objects;
    }

    public T read(Integer id) {
        T original = map.get(id);
        T copy = copyObject(original);
        return copy != null ? copy : original;
    }

    public void delete(Integer id) {
        T object = map.get(id);
        list.remove(object);
        map.remove(id);
    }

    protected T copyObject(T object) {
        return null;
    }
}
