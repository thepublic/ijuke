package stingion.ijuke.frontside.stripesbook.dao;

import stingion.ijuke.frontside.stripesbook.model.Folder;

/**
 * Created under not commercial project
 */
public interface FolderDao extends Dao<Folder> {
}
