package stingion.ijuke.frontside.stripesbook.dao;

import java.util.List;

/**
 * Created under not commercial project
 */
public interface Dao<T> {
    public List<T> read();
    public T read(Integer id);
    public void save(T t);
    public void delete(Integer id);
}
