package stingion.ijuke.frontside.web.action;

import lombok.Getter;
import lombok.Setter;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringWriter;

/**
 * Created under not commercial project
 */
public class ViewSourceActionBean extends BaseActionBean {

    private static final String WEB_INF_PREFIX = "/WEB-INF/";
    private static final String CLASSES_PREFIX = "classes/";
    private static final String JAVA_SUFFIX = ".java";

    @SpringBean("view_source.jsp")
    private String viewSource;

    @Getter
    @Setter
    private String filename;

    @Getter
    private String source;

    public Resolution view() throws Exception {
        String file = filename;
        if (file.startsWith("/")) {
            file = file.substring(file.indexOf('/', 1));
        }
        else {
            file = file.replaceAll("\\.", "/");
            file = CLASSES_PREFIX + file + JAVA_SUFFIX;
        }

        if (!file.contains(WEB_INF_PREFIX))
            file = WEB_INF_PREFIX + file;

        String path = getContext().getServletContext()
                .getRealPath(file);

        BufferedReader reader = new BufferedReader(new FileReader(path));

        StringWriter writer = new StringWriter();

        String nextLine;
        while ((nextLine = reader.readLine()) != null) {
            writer.write(nextLine);
            writer.write('\n');
        }
        reader.close();
        source = writer.toString();
        return new ForwardResolution(viewSource);
    }
}

