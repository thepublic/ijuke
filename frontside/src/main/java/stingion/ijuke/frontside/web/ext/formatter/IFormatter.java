package stingion.ijuke.frontside.web.ext.formatter;

import stingion.ijuke.frontside.web.ext.formatter.CanNotFormatException;

/**
 * Created under not commercial project
 */
public interface IFormatter<T> {
    String format(String formatType, T input) throws CanNotFormatException;
}
