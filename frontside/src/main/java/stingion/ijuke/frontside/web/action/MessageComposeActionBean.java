package stingion.ijuke.frontside.web.action;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 * Created under not commercial project
 */
public class MessageComposeActionBean extends BaseActionBean {

    @SpringBean("message_compose.jsp")
    private String messageCompose;

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(messageCompose);
    }
}
