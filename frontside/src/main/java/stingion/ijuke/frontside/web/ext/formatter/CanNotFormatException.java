package stingion.ijuke.frontside.web.ext.formatter;

/**
 * Created under not commercial project
 */
public class CanNotFormatException extends Exception {

    public CanNotFormatException() {
        super("Can't format input date with formatType");
    }
}
