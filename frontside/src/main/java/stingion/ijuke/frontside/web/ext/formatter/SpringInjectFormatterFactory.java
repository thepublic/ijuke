package stingion.ijuke.frontside.web.ext.formatter;

import net.sourceforge.stripes.controller.StripesFilter;
import net.sourceforge.stripes.format.DefaultFormatterFactory;
import net.sourceforge.stripes.format.Formatter;
import net.sourceforge.stripes.integration.spring.SpringHelper;

import javax.servlet.ServletContext;
import java.util.Locale;

/**
 * Created under not commercial project
 */
public class SpringInjectFormatterFactory extends DefaultFormatterFactory {

    @Override
    public Formatter<?> getInstance(Class<? extends Formatter<?>> clazz,
                                    String formatType, String formatPattern, Locale locale) throws Exception {
        Formatter<?> formatter = super.getInstance(clazz, formatType, formatPattern, locale);
        ServletContext servletContext = StripesFilter.getConfiguration().getServletContext();
        SpringHelper.injectBeans(formatter, servletContext);
        return formatter;
    }
}
