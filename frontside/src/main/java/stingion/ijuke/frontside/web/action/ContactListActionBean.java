package stingion.ijuke.frontside.web.action;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.SimpleMessage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

import java.util.List;

public class ContactListActionBean extends ContactBaseActionBean {

    @SpringBean("contact_list.jsp")
    private String contactList;

    @SpringBean("contact_view.jsp")
    private String contactView;

    public Resolution view() {
        return new ForwardResolution(contactView);
    }

    public Resolution delete() {
        ContactDTO deleted = getContact();
        contactService.delete(deleted);
        getContext().getMessages().add(new SimpleMessage("Deleted {0}.", deleted));
        return new RedirectResolution(getClass());
    }

    @DefaultHandler
    public Resolution list() {
        return new ForwardResolution(contactList);
    }

    public List<ContactDTO> getContacts() {
        return contactService.getAll();
    }
}
