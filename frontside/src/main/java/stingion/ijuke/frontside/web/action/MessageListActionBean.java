package stingion.ijuke.frontside.web.action;

import lombok.Getter;
import lombok.Setter;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import stingion.ijuke.frontside.data.services.FolderService;
import stingion.ijuke.utils.exception.NotImplementedException;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;
import stingion.ijuke.utils.ijuke.dto.MessageDTO;

import java.util.List;

/**
 * Created under not commercial project
 */
public class MessageListActionBean extends BaseActionBean {

    @SpringBean("restFolderService")
    @Getter
    protected FolderService folderService;

    @SpringBean("message_list.jsp")
    private String messageList;

    @Validate(required = true, on = {"delete", "moveToFolder"})
    @Getter
    @Setter
    private List<MessageDTO> selectedMessages;

    @Validate(required = true, on = "moveToFolder")
    @Getter
    @Setter
    private FolderDTO selectedFolder;

    @DefaultHandler
    public Resolution list() {
        return new ForwardResolution(messageList);
    }

    public Resolution delete() {
        for (MessageDTO message : selectedMessages) {//NOSONAR
            throw new NotImplementedException();
            //folderService.deleteMessage(message);//NOSONAR Todo
        }
        return new RedirectResolution(getClass());
    }

    public Resolution moveToFolder() {
        for (MessageDTO message : selectedMessages) {//NOSONAR
            throw new NotImplementedException();
            //folderService.addMessage(message, selectedFolder);//NOSONAR Todo
        }
        return new RedirectResolution(getClass());
    }
}
