/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.web.action;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.SimpleMessage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.SimpleError;
import net.sourceforge.stripes.validation.ValidationErrors;
import net.sourceforge.stripes.validation.ValidationMethod;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;
import stingion.ijuke.utils.ijuke.modeltypes.Gender;

public class ContactFormActionBean extends ContactBaseActionBean {

    @SpringBean("contact_form.jsp")
    private String form;

    @DefaultHandler
    public Resolution form() {
        return new ForwardResolution(form);
    }

    @ValidationMethod(on = "save")
    public void validateEmailUnique(ValidationErrors errors) {
        String email = getContact().getEmail();
        ContactDTO other = getContactService().getContactByEmail(email);
        if (other != null && !other.equals(getContact())) {
            errors.add("contact.email", new SimpleError("{1} is already used by {2}.", other));
        }
    }

    public Resolution save() {
        ContactDTO contact = getContact();

        String saveOrUpdateWord = contact.getId() == null ? "create" : "update";

        if ("create".equals(saveOrUpdateWord))
            getContactService().create(contact);
        else
            getContactService().update(contact);

        getContext().getMessages().add(new SimpleMessage("{0} has been " + saveOrUpdateWord + "d" + ".", contact));
        return new RedirectResolution(ContactListActionBean.class);
    }

    @DontValidate
    public Resolution cancel() {
        getContext().getMessages().add(new SimpleMessage("Action cancelled."));
        return new RedirectResolution(ContactListActionBean.class);
    }

    public Gender[] getGenders() {
        return Gender.values();
    }
}
