package stingion.ijuke.frontside.web.ext.formatter.util;

/**
 * Created under not commercial project
 */
public class UniqueFormatTypeName {

    public static final String PHONE_NUMBER_PARENS = "phonenumber.parens";
    public static final String PHONE_NUMBER_DASHES = "phonenumber.dashes";
    public static final String PHONE_NUMBER_NUMBER = "phonenumber.number";

    private UniqueFormatTypeName(){
    }
}
