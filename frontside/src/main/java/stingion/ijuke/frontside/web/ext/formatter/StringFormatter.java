package stingion.ijuke.frontside.web.ext.formatter;

import net.sourceforge.stripes.format.Formatter;
import net.sourceforge.stripes.integration.spring.SpringBean;
import org.apache.log4j.Logger;

import java.util.Locale;

/**
 * Created under not commercial project
 */
public class StringFormatter implements Formatter<String> {

    static final Logger log = Logger.getLogger(StringFormatter.class);
    /**
     * All String formatter should be added to the array
     */
    @SpringBean("stringFormatters")
    private IFormatter<String>[] formatters;

    private String formatType;

    @Override
    public void setFormatType(String formatType) {
        this.formatType = formatType;
    }

    @Override
    public String format(String phoneNumber) {

        for (IFormatter<String> formatter : formatters) {
            try {
                return formatter.format(formatType, phoneNumber);
            } catch (CanNotFormatException e) {
                log.error(e);
            }
        }
        return phoneNumber;
    }

    @Override
    public void init() {
        //Should be implemented in interface
    }

    @Override
    public void setLocale(Locale locale) {
        //Should be implemented in interface
    }

    @Override
    public void setFormatPattern(String formatPattern) {
        //Should be implemented in interface
    }
}
