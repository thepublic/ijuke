/**
 * Created under non-commercial project "ijuke"
 *
 */
package stingion.ijuke.frontside.web.ext.context;

import lombok.AccessLevel;
import lombok.Getter;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.integration.spring.SpringBean;
import stingion.ijuke.frontside.data.services.FolderService;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;
import stingion.ijuke.utils.ijuke.dto.MessageDTO;

public class MyActionBeanContext extends ActionBeanContext {

    private static final String FOLDER  = "folder";
    private static final String MESSAGE = "message";

    @SpringBean("restFolderService")
    @Getter(AccessLevel.PROTECTED)
    protected FolderService folderService;

    public void setCurrentFolder(FolderDTO folder) {
        setCurrent(FOLDER, folder);
    }

    public FolderDTO getCurrentFolder() {
        FolderDTO folder = folderService.getAll().get(0);
        return getCurrent(FOLDER, folder);
    }

    public void setMessageCompose(MessageDTO message) {
        setCurrent(MESSAGE, message);
    }

    public MessageDTO getMessageCompose() {
        return getCurrent(MESSAGE, new MessageDTO());
    }

    protected void setCurrent(String key, Object value) {
        getRequest().getSession().setAttribute(key, value);
    }

    @SuppressWarnings("unchecked")
    protected <T> T getCurrent(String key, T defaultValue) {
        T value = (T) getRequest().getSession().getAttribute(key);
        if (value == null) {
            value = defaultValue;
            setCurrent(key, value);
        }
        return value;
    }
}
