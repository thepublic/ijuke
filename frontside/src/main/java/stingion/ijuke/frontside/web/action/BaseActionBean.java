/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.web.action;

import lombok.Getter;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import stingion.ijuke.frontside.web.ext.context.MyActionBeanContext;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;

public abstract class BaseActionBean implements ActionBean {

    @Getter
    private MyActionBeanContext context;

    public void setContext(ActionBeanContext actionBeanContext) {
        this.context = (MyActionBeanContext) actionBeanContext;
    }

    public void setFolder(FolderDTO folder) {
        context.setCurrentFolder(folder);
    }
}
