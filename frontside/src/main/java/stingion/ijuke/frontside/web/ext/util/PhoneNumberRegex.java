package stingion.ijuke.frontside.web.ext.util;

import java.util.regex.Pattern;

/**
 * Created by ievgen on 10/7/14 12:52 AM.
 */
public final class PhoneNumberRegex {

    public static final String AREA = "area";
    public static final String PREFIX = "prefix";
    public static final String SUFFIX = "suffix";

    public static final String PHONE_NUMBER_MASK =
            "\\(?(?<" + AREA + ">\\d{3})\\)?[-. ]?(?<" + PREFIX + ">\\d{3})[-. ]?(?<" + SUFFIX + ">\\d{4})";

    public static final Pattern PATTERN = Pattern.compile(PHONE_NUMBER_MASK);

    private PhoneNumberRegex() {
    }
}
