/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.web.action;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.EmailTypeConverter;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import stingion.ijuke.frontside.data.services.ContactService;
import stingion.ijuke.frontside.web.ext.converter.PhoneNumberTypeConverter;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

import java.util.Date;

public class ContactBaseActionBean extends BaseActionBean {

    @Setter
    private ContactDTO contact;

    @SpringBean("restContactService")
    @Getter(AccessLevel.PROTECTED)
    protected ContactService contactService;

    @Getter
    @Setter
    protected Integer contactId;

    public Date getToday() {
        return new Date();
    }

    @ValidateNestedProperties({
            @Validate(field = "email", required = true, on = "save", converter = EmailTypeConverter.class,label = "E-mail"),
            @Validate(field = "firstName", maxlength = 25, label = "Given name"),
            @Validate(field = "middleName", maxlength = 25, label = "Middle name"),
            @Validate(field = "lastName", minlength = 2, maxlength = 40, label = "Surname"),
            @Validate(field = "birthDay", expression = "${contact.birthDay <= today}", label = "Date of birth"),
            @Validate(field = "phoneNumber", converter = PhoneNumberTypeConverter.class, label = "Telephone number"),
            @Validate(field = "gender", label = "Gender")
    })

    public ContactDTO getContact() {
        if (contactId != null) {
            return contactService.getById(contactId);
        }
        return contact;
    }
}
