package stingion.ijuke.frontside.web.ext.converter;

import net.sourceforge.stripes.config.DontAutoLoad;
import net.sourceforge.stripes.validation.SimpleError;
import net.sourceforge.stripes.validation.TypeConverter;
import net.sourceforge.stripes.validation.ValidationError;
import stingion.ijuke.frontside.web.ext.util.PhoneNumberRegex;

import java.util.Collection;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@DontAutoLoad
public class PhoneNumberTypeConverter implements TypeConverter<String> {
    private static final Pattern pattern = Pattern.compile(PhoneNumberRegex.PHONE_NUMBER_MASK);

    public String convert(String input, Class<? extends String> type, Collection<ValidationError> errors) {
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            return matcher.group(PhoneNumberRegex.AREA) + matcher.group(PhoneNumberRegex.PREFIX) + matcher.group(PhoneNumberRegex.SUFFIX);
        } else {
            errors.add(new SimpleError("{1} is not a valid {0}"));
            return null;
        }
    }

    public void setLocale(Locale locale) {
        //Should be implemented in interface
    }
}

