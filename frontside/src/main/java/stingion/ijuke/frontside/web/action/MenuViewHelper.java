package stingion.ijuke.frontside.web.action;

import lombok.Getter;
import lombok.Setter;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 * Created under not commercial project
 */
public class MenuViewHelper extends BaseActionBean {

    @SpringBean("menu.jsp")
    private String menuJsp;

    @Getter
    @Setter
    private Section currentSection;

    public Section[] getSections() {
        return Section.values();
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(menuJsp);
    }

    public enum Section {
        MESSAGE_LIST("Messages", MessageListActionBean.class),
        CONTACT_LIST("Contact List", ContactListActionBean.class),
        COMPOSE("Compose", MessageComposeActionBean.class);

        @Getter
        private String text;

        @Getter
        private String beanClass;

        Section(String text, Class<? extends ActionBean> beanClass) {//NOSONAR
            this.text = text;
            this.beanClass = beanClass.getName();
        }
    }
}
