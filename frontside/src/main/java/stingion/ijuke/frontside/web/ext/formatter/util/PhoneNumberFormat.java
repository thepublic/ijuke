package stingion.ijuke.frontside.web.ext.formatter.util;

import org.springframework.stereotype.Component;
import stingion.ijuke.frontside.web.ext.formatter.CanNotFormatException;
import stingion.ijuke.frontside.web.ext.formatter.IFormatter;
import stingion.ijuke.frontside.web.ext.util.PhoneNumberRegex;

import java.util.regex.Matcher;

@Component
public class PhoneNumberFormat implements IFormatter<String> {

    private String getFormatPattern(String formatName) {
        switch (formatName.toLowerCase()) {
            case UniqueFormatTypeName.PHONE_NUMBER_PARENS:
                return "(%s) %s-%s";
            case UniqueFormatTypeName.PHONE_NUMBER_DASHES:
                return "%s-%s-%s";
            case UniqueFormatTypeName.PHONE_NUMBER_NUMBER:
                return "%s%s%s";
            default:
                return null;
        }
    }

    @Override
    public String format(String phoneNumberType, String phoneNumber) throws CanNotFormatException {

        if (phoneNumberType != null && phoneNumber != null && getFormatPattern(phoneNumberType) != null) {
            Matcher phoneNumberMatcher = PhoneNumberRegex.PATTERN.matcher(phoneNumber);
            if (phoneNumberMatcher.matches()) {
                return String.format(getFormatPattern(phoneNumberType),
                                     phoneNumberMatcher.group(PhoneNumberRegex.AREA),
                                     phoneNumberMatcher.group(PhoneNumberRegex.PREFIX),
                                     phoneNumberMatcher.group(PhoneNumberRegex.SUFFIX));
            }
        }

        throw new CanNotFormatException();
    }
}
