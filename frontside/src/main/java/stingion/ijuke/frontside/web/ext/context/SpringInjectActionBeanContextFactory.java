/**
 * Created under not commercial project "ijuke"
 *
 */
package stingion.ijuke.frontside.web.ext.context;

import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.controller.DefaultActionBeanContextFactory;
import net.sourceforge.stripes.integration.spring.SpringHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SpringInjectActionBeanContextFactory extends DefaultActionBeanContextFactory {

    @Override
    public ActionBeanContext getContextInstance(HttpServletRequest request,
                                                HttpServletResponse response) throws ServletException {
        ActionBeanContext context = super.getContextInstance(request, response);
        SpringHelper.injectBeans(context, request.getSession().getServletContext());
        return context;
    }
}
