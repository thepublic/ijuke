/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import stingion.ijuke.frontside.data.services.BankService;
import stingion.ijuke.utils.exception.FireRuntimeException;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

import java.io.IOException;

@Service("restBankService")
public class RestBankServiceImpl extends RestAbstractServiceImpl<BankDTO> implements BankService {

    @Value("${rest.path.bank.name}")
    private String name;

    @Override
    public BankDTO getBankByName(String name) {
        try {
            return objectMapper.readValue(getJson(entityProps.getEntity(), this.name, name), BankDTO.class);
        } catch (IOException e) {
            throw new FireRuntimeException(e);
        }
    }
}
