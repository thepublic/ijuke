/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import org.springframework.stereotype.Service;
import stingion.ijuke.frontside.data.services.CreditCardService;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;

@Service("restCreditCardService")
public class RestCreditCardServiceImpl extends RestAbstractServiceImpl<CreditCardDTO> implements CreditCardService {
}
