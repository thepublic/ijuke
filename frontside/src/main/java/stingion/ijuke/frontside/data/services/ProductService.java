/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services;

import stingion.ijuke.utils.ijuke.dto.ProductDTO;

public interface ProductService extends AbstractService<ProductDTO> {
}
