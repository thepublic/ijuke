/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest.utils;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import stingion.ijuke.utils.ijuke.dto.BankDTO;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;
import stingion.ijuke.utils.ijuke.dto.MessageDTO;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

import javax.annotation.PostConstruct;

@Configuration
public class PropsComponents { //NOSONAR Todo: fix for Sonar

    @Getter
    public static class EntityProps<T> { //NOSONAR
        @Value("${rest.path.entity.id}")
        protected String entityId;
        @Value("${rest.path.entity.save}")
        protected String entitySave;
        @Value("${rest.path.entity.update}")
        protected String entityUpdate;
        @Value("${rest.path.entity.delete}")
        protected String entityDelete;

        protected String entity;
    }

    @Component
    @Getter
    public static class ContactProps extends EntityProps<ContactDTO> {
        @Value("${rest.path.contact}")
        private String contact;

        @PostConstruct
        private void init() {
            entity = contact;
        }
    }

    @Component
    @Getter
    public static class BankProps extends EntityProps<BankDTO> {
        @Value("${rest.path.bank}")
        private String bank;

        @PostConstruct
        private void init() {
            entity = bank;
        }
    }

    @Component
    @Getter
    public static class CreditCardProps extends EntityProps<CreditCardDTO> {
        @Value("${rest.path.creditCard}")
        private String creditCard;

        @PostConstruct
        private void init() {
            entity = creditCard;
        }
    }

    @Component
    @Getter
    public static class OrderingProps extends EntityProps<OrderingDTO> {
        @Value("${rest.path.ordering}")
        private String ordering;

        @PostConstruct
        private void init() {
            entity = ordering;
        }
    }

    @Component
    @Getter
    public static class ProductProps extends EntityProps<ProductDTO> {
        @Value("${rest.path.product}")
        private String product;

        @PostConstruct
        private void init() {
            entity = product;
        }
    }

    @Component
    @Getter
    public static class SupplerProps extends EntityProps<SupplierDTO> {
        @Value("${rest.path.supplier}")
        private String supplier;

        @PostConstruct
        private void init() {
            entity = supplier;
        }
    }

    @Component
    @Getter
    public static class ProductSupplerProps extends EntityProps<ProductSupplierDTO> {
        @Value("${rest.path.productSupplier}")
        private String productSupplier;

        @PostConstruct
        private void init() {
            entity = productSupplier;
        }
    }

    @Component
    @Getter
    public static class MessageProps extends EntityProps<MessageDTO> {
        @Value("${rest.path.message}")
        private String message;

        @PostConstruct
        private void init() {
            entity = message;
        }
    }

    @Component
    @Getter
    public static class FolderProps extends EntityProps<FolderDTO> {
        @Value("${rest.path.folder}")
        private String folder;

        @PostConstruct
        private void init() {
            entity = folder;
        }
    }
}
