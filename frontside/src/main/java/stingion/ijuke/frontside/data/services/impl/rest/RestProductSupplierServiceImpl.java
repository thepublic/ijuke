/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import com.sun.jersey.api.client.ClientResponse;//NOSONAR
import org.springframework.stereotype.Service;
import stingion.ijuke.frontside.data.services.ProductSupplierService;
import stingion.ijuke.utils.exception.FireRuntimeException;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierIdDTO;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.Serializable;

@Service("restProductSupplierService")
public class RestProductSupplierServiceImpl extends RestAbstractServiceImpl<ProductSupplierDTO> implements ProductSupplierService {

    @Override
    public ProductSupplierDTO getById(Serializable id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Serializable create(ProductSupplierDTO entity) {
        try {
            ClientResponse response = service.path(entityProps.getEntity()).path(entityProps.getEntitySave())
                    .type(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, objectMapper.writeValueAsString(entity));

            return objectMapper.readValue(response.getEntity(String.class), ProductSupplierIdDTO.class);
        } catch (IOException ex) {
            throw new FireRuntimeException(ex);
        }
    }
}
