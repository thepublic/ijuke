/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest.utils;

import com.sun.jersey.api.client.Client;//NOSONAR
import com.sun.jersey.api.client.WebResource;//NOSONAR
import com.sun.jersey.api.client.config.ClientConfig;//NOSONAR
import com.sun.jersey.api.client.config.DefaultClientConfig;//NOSONAR
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;//NOSONAR

import javax.ws.rs.core.UriBuilder;

public class WebResourceInit {

    private WebResourceInit() {
    }

    public static WebResource initBean(String restBaseURI, String restUsername, String restPassword) {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        client.addFilter(new HTTPBasicAuthFilter(restUsername, restPassword));

        return client.resource(UriBuilder.fromUri(restBaseURI).build());
    }
}
