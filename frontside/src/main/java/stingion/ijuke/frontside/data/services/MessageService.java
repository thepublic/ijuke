/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services;

import stingion.ijuke.utils.ijuke.dto.MessageDTO;

public interface MessageService extends AbstractService<MessageDTO> {
}
