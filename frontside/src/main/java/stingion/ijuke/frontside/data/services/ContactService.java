/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services;

import stingion.ijuke.utils.ijuke.dto.ContactDTO;

public interface ContactService extends AbstractService<ContactDTO> {

    ContactDTO getContactByEmail(String email);
}
