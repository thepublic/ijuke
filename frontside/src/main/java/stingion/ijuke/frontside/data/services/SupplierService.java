/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services;

import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

public interface SupplierService extends AbstractService<SupplierDTO> {
}
