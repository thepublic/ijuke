/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services;

import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;

public interface CreditCardService extends AbstractService<CreditCardDTO> {
}
