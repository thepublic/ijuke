/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import org.springframework.stereotype.Service;
import stingion.ijuke.frontside.data.services.SupplierService;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

@Service("restSupplierService")
public class RestSupplierServiceImpl extends RestAbstractServiceImpl<SupplierDTO> implements SupplierService {
}
