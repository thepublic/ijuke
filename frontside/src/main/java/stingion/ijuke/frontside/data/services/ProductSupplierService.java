/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services;

import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;

public interface ProductSupplierService extends AbstractService<ProductSupplierDTO> {
}
