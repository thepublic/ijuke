/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import org.springframework.stereotype.Service;
import stingion.ijuke.frontside.data.services.FolderService;
import stingion.ijuke.utils.ijuke.dto.FolderDTO;

@Service("restFolderService")
public class RestFolderServiceImpl extends RestAbstractServiceImpl<FolderDTO> implements FolderService {
}
