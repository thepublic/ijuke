/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import stingion.ijuke.frontside.data.services.ContactService;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

import java.io.IOException;

@Service("restContactService")
public class RestContactServiceImpl extends RestAbstractServiceImpl<ContactDTO> implements ContactService {

    static final Logger log = Logger.getLogger(RestContactServiceImpl.class);

    @Value("${rest.path.contact.email}")
    private String email;

    @Override
    public ContactDTO getContactByEmail(String email) {
        try {
            return objectMapper.readValue(getJson(entityProps.getEntity(), this.email, email), ContactDTO.class);
        } catch (IOException e) {
            log.error(e);
            return null;
        }
    }
}
