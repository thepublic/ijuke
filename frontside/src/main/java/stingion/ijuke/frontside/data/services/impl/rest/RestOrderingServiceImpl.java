/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import org.springframework.stereotype.Service;
import stingion.ijuke.frontside.data.services.OrderingService;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

@Service("restOrderingService")
public class RestOrderingServiceImpl extends RestAbstractServiceImpl<OrderingDTO> implements OrderingService {
}
