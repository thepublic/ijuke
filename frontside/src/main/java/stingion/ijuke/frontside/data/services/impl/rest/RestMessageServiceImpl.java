/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import org.springframework.stereotype.Service;
import stingion.ijuke.frontside.data.services.MessageService;
import stingion.ijuke.utils.ijuke.dto.MessageDTO;

@Service("restMessageService")
public class RestMessageServiceImpl extends RestAbstractServiceImpl<MessageDTO> implements MessageService {
}
