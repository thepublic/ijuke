/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services;

import stingion.ijuke.utils.ijuke.dto.BankDTO;

public interface BankService extends AbstractService<BankDTO> {

    BankDTO getBankByName(String name);
}
