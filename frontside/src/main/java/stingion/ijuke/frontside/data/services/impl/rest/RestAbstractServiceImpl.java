/**
 * Created under not commercial project
 */
package stingion.ijuke.frontside.data.services.impl.rest;

import com.sun.jersey.api.client.ClientResponse;//NOSONAR
import com.sun.jersey.api.client.WebResource;//NOSONAR
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import stingion.ijuke.frontside.data.services.AbstractService;
import stingion.ijuke.frontside.data.services.impl.rest.utils.PropsComponents;
import stingion.ijuke.utils.exception.FireRuntimeException;
import stingion.ijuke.utils.exception.NotImplementedException;
import stingion.ijuke.utils.useful.Reflection;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public class RestAbstractServiceImpl<T> implements AbstractService<T> {

    @Autowired
    protected WebResource service;

    @Autowired
    protected PropsComponents.EntityProps<T> entityProps;

    protected final ObjectMapper objectMapper = new ObjectMapper();

    protected String getJson(String path, String paramName, String paramValue) {
        return service.path(path).queryParam(paramName, paramValue)
                .accept(MediaType.APPLICATION_JSON).get(String.class);
    }

    protected String getJson(String path) {
        return service.path(path).accept(MediaType.APPLICATION_JSON).get(String.class);
    }

    @Override
    public T getById(Serializable id) {
        try {
            return objectMapper.readValue(getJson(entityProps.getEntity(), entityProps.getEntityId(), id.toString()),
                    (Class<T>) Reflection.getParameterClass(this));
        } catch (IOException ex) {
            throw new FireRuntimeException(ex);
        }
    }

    @Override
    public List<T> getAll() {
        try {
            List linkedHashMapList = objectMapper.readValue(getJson(entityProps.getEntity()), List.class);
            return objectMapper.convertValue(linkedHashMapList, new TypeReference<List<T>>() {
            });
        } catch (IOException e) {
            throw new FireRuntimeException(e);
        }
    }

    @Override
    public Serializable create(T entity) {
        try {
            ClientResponse response = service.path(entityProps.getEntity()).path(entityProps.getEntitySave())
                    .type(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, objectMapper.writeValueAsString(entity));
            return objectMapper.readValue(response.getEntity(String.class), Integer.class);
        } catch (IOException ex) {
            throw new FireRuntimeException(ex);
        }
    }

    @Override
    public void update(T entity) {
        try {
            service.path(entityProps.getEntity()).path(entityProps.getEntityUpdate())
                    .type(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, objectMapper.writeValueAsString(entity));
        } catch (IOException ex) {
            throw new FireRuntimeException(ex);
        }
    }

    @Override
    public void saveOrUpdate(T entity) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(T entity) {
        try {
            service.path(entityProps.getEntity()).path(entityProps.getEntityDelete())
                    .type(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, objectMapper.writeValueAsString(entity));
        } catch (IOException ex) {
            throw new FireRuntimeException(ex);
        }
    }
}
