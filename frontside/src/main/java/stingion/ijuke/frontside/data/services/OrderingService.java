/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services;

import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

public interface OrderingService extends AbstractService<OrderingDTO> {
}
