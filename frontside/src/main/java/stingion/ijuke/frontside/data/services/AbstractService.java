/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services;

import java.io.Serializable;
import java.util.List;

public interface AbstractService<T> {

    Serializable create(T t);

    T getById(Serializable id);

    void update(T t);

    void saveOrUpdate(T t);

    void delete(T t);

    List<T> getAll();
}
