/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services;

import stingion.ijuke.utils.ijuke.dto.FolderDTO;

public interface FolderService extends AbstractService<FolderDTO> {
}
