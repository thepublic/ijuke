/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import org.springframework.stereotype.Service;
import stingion.ijuke.frontside.data.services.ProductService;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;

@Service("restProductService")
public class RestProductServiceImpl extends RestAbstractServiceImpl<ProductDTO> implements ProductService {
}
