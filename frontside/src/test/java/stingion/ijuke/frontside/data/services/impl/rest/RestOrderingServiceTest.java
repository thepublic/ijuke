/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/frontside/spring-test/contexts-bundle.xml")
public class RestOrderingServiceTest extends RestAbstractServiceTesting<OrderingDTO> {
}
