/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import com.sun.jersey.api.client.WebResource;//NOSONAR
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.frontside.data.services.ContactService;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/frontside/spring-test/contexts-bundle.xml")
public class RestContactServiceTest extends RestAbstractServiceTesting<ContactDTO> {
    @Mock
    protected WebResource webResourceEmailParam;

    @Mock
    protected WebResource.Builder builderEmailParam;

    @Value("${rest.return.contact.email.param}")
    public String contactParamEmail;

    @Value("${rest.return.contact.email}")
    public String contactEmail;

    @Value("${rest.return.contact.by.email}")
    public String contactByEmail;

    protected ContactDTO expectedContactByEmailDTO;

    @Before
    public void init() throws IOException {
        super.init();

        when(webResource.queryParam(contactParamEmail, contactEmail)).thenReturn(webResourceEmailParam);
        when(webResourceEmailParam.accept(MediaType.APPLICATION_JSON)).thenReturn(builderEmailParam);
        when(builderEmailParam.get(String.class)).thenReturn(contactByEmail);

        expectedContactByEmailDTO = objectMapper.readValue(contactByEmail, ContactDTO.class);
    }

    @Test
    public void testGetEntityByEmail() throws IOException {
        assertEquals(expectedContactByEmailDTO, ((ContactService) restAbstractService).getContactByEmail(contactEmail));
    }
}
