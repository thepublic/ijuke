/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest.util;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Configuration;
import stingion.ijuke.utils.ijuke.dto.BankDTO;
import stingion.ijuke.utils.ijuke.dto.ContactDTO;
import stingion.ijuke.utils.ijuke.dto.CreditCardDTO;
import stingion.ijuke.utils.ijuke.dto.OrderingDTO;
import stingion.ijuke.utils.ijuke.dto.ProductDTO;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;
import stingion.ijuke.utils.ijuke.dto.SupplierDTO;

@Configuration
public class EntityDataContainer {//NOSONAR

    @Getter
    @Setter
    public abstract static class EntityData<T> {//NOSONAR
        private String entityPath;//NOSONAR
        private String entityId;//NOSONAR
        private String entityIdValue;//NOSONAR
        private String entity;//NOSONAR
        private String entities;//NOSONAR
        private String entityForSave;//NOSONAR
        private String entityForSaveResponseId;//NOSONAR
    }

    public static class BankData extends EntityData<BankDTO> {
    }

    public static class ContactData extends EntityData<ContactDTO> {
    }

    public static class CreditCardData extends EntityData<CreditCardDTO> {
    }

    public static class OrderingData extends EntityData<OrderingDTO> {
    }

    public static class ProductData extends EntityData<ProductDTO> {
    }

    public static class SupplierData extends EntityData<SupplierDTO> {
    }

    public static class ProductSupplierData extends EntityData<ProductSupplierDTO> {
    }
}
