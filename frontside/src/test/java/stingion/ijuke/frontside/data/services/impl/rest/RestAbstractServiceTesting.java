/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import com.sun.jersey.api.client.ClientResponse;//NOSONAR
import com.sun.jersey.api.client.WebResource;//NOSONAR
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import stingion.ijuke.frontside.data.services.AbstractService;
import stingion.ijuke.frontside.data.services.impl.rest.util.EntityDataContainer;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class RestAbstractServiceTesting<T> {

    @Value("${rest.path.entity.save}")
    private String saveName;

    private Class<T> entityClass;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected AbstractService<T> restAbstractService;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected EntityDataContainer.EntityData<T> entityData;

    protected final ObjectMapper objectMapper = new ObjectMapper();

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected WebResource webResource;

    @Mock
    protected WebResource.Builder builder;

    @Mock
    protected WebResource webResourceWithParam;

    @Mock
    protected WebResource.Builder builderWithParam;

    @Mock
    protected WebResource webResourceWithPost;

    @Mock
    protected WebResource.Builder builderWithPost;

    @Mock
    protected ClientResponse response;

    protected T expectedEntityDTO;

    protected List<T> expectedEntityDTOs;

    @Before
    public void init() throws IOException {
        MockitoAnnotations.initMocks(this);

        when(webResource.path(entityData.getEntityPath())).thenReturn(webResource);

        when(webResource.accept(MediaType.APPLICATION_JSON)).thenReturn(builder);
        when(builder.get(String.class)).thenReturn(entityData.getEntities());

        when(webResource.queryParam(entityData.getEntityId(), entityData.getEntityIdValue()))
                .thenReturn(webResourceWithParam);
        when(webResourceWithParam.accept(MediaType.APPLICATION_JSON)).thenReturn(builderWithParam);
        when(builderWithParam.get(String.class)).thenReturn(entityData.getEntity());

        //Expected values initialization
        List linkedHashMapList = objectMapper.readValue(entityData.getEntities(), List.class);
        expectedEntityDTOs = objectMapper.convertValue(linkedHashMapList, new TypeReference<List<T>>() {
        });

        //Check for productSupplier
        if (entityData.getEntity() != null)
            expectedEntityDTO = objectMapper.readValue(entityData.getEntity(), getEntityClass());

        //Post mocking
        when(webResource.path(saveName)).thenReturn(webResourceWithPost);
        when(webResourceWithPost.type(MediaType.APPLICATION_JSON)).thenReturn(builderWithPost);
        when(builderWithPost.accept(MediaType.APPLICATION_JSON)).thenReturn(builderWithPost);
        when(builderWithPost.post(ClientResponse.class, entityData.getEntityForSave())).thenReturn(response);
        when(response.getEntity(String.class)).thenReturn(entityData.getEntityForSaveResponseId());
    }

    @Test
    public void testGetEntity() throws IOException {
        assertEquals(expectedEntityDTO, restAbstractService.getById(entityData.getEntityIdValue()));
    }

    @Test
    public void testGetEntities() throws IOException {
        assertEquals(expectedEntityDTOs, restAbstractService.getAll());
    }

    @Test
    public void testSaveEntity() throws IOException {
        Serializable id = restAbstractService
                .create(objectMapper.readValue(entityData.getEntityForSave(), getEntityClass()));

        assertEquals(Integer.valueOf(entityData.getEntityForSaveResponseId()), id);
    }

    public Class<T> getEntityClass() {
        if (entityClass == null) {
            ParameterizedType pType = (ParameterizedType) getClass().getGenericSuperclass();
            this.entityClass = (Class<T>) pType.getActualTypeArguments()[0];
        }

        return entityClass;
    }
}
