/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierDTO;
import stingion.ijuke.utils.ijuke.dto.ProductSupplierIdDTO;

import java.io.IOException;
import java.io.Serializable;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/frontside/spring-test/contexts-bundle.xml")
public class RestProductSupplierServiceTest extends RestAbstractServiceTesting<ProductSupplierDTO> {

    @Test(expected = UnsupportedOperationException.class)
    @Override
    public void testGetEntity() throws IOException {
        super.testGetEntity();
    }

    @Test
    @Override
    public void testSaveEntity() throws IOException {
        Serializable id = restAbstractService
                .create(objectMapper.readValue(entityData.getEntityForSave(), getEntityClass()));

        assertEquals(objectMapper.readValue(entityData.getEntityForSaveResponseId(), ProductSupplierIdDTO.class), id);
    }
}
