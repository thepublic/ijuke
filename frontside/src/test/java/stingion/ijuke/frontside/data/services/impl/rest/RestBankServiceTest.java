/**
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.frontside.data.services.impl.rest;

import com.sun.jersey.api.client.WebResource;//NOSONAR
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.frontside.data.services.BankService;
import stingion.ijuke.utils.ijuke.dto.BankDTO;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/frontside/spring-test/contexts-bundle.xml")
public class RestBankServiceTest extends RestAbstractServiceTesting<BankDTO> {

    @Mock
    protected WebResource webResourceNameParam;

    @Mock
    protected WebResource.Builder builderNameParam;

    @Value("${rest.return.bank.name.param}")
    public String bankParamName;

    @Value("${rest.return.bank.name}")
    public String bankName;

    @Value("${rest.return.bank.by.name}")
    public String bankByName;

    protected BankDTO expectedBankByNameDTO;

    @Before
    public void init() throws IOException {
        super.init();

        when(webResource.queryParam(bankParamName, bankName)).thenReturn(webResourceNameParam);
        when(webResourceNameParam.accept(MediaType.APPLICATION_JSON)).thenReturn(builderNameParam);
        when(builderNameParam.get(String.class)).thenReturn(bankByName);

        expectedBankByNameDTO = objectMapper.readValue(bankByName, BankDTO.class);
    }

    @Test
    public void testGetBankByName() throws IOException {
        assertEquals(expectedBankByNameDTO, ((BankService) restAbstractService).getBankByName(bankName));
    }
}
