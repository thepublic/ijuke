package stingion.ijuke.informmavenplugin;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Print Greeting
 */
@Mojo(name = "print")
public class PrintMojo extends AbstractMojo {

    /**
     * Name for greetings (module name expected ${project.name})
     */
    @Parameter(property = "inform.module", defaultValue = "Module")
    private String moduleName;

    public void execute() throws MojoFailureException {
        getLog().info("Inform Maven Plugin: Greeting, " + moduleName);
    }
}
