drop table if exists Course;
create table Course (course_id integer, name varchar(50), course varchar(50), PRIMARY KEY (course_id));

insert into Course values (1,'Sam', 'Java');
insert into Course values (2,'Peter', 'J2EE');
insert into Course values (3,'Paul', 'JSF');
insert into Course values (4,'Jonathan', 'Hibernate');
insert into Course values (5,'James', 'Spring');
