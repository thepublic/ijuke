package stingion.ijuke.utils.sql;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import stingion.ijuke.utils.marker.IntegrationTest;

import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by ievgen on 7/31/14 9:46 PM.
 * <p/>
 * The test is defined as integration for testing "integration-test" functionality in the module
 */
@Category(IntegrationTest.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/META-INF/stingion/ijuke/utils/test-spring-context/test-sql-pack.xml")
public class IntegrationTestForSQLScriptExecutor {

    @Autowired
    @Qualifier("sqlScriptExecutorHSQL")
    private SQLScriptExecutor sqlScriptExecutor;

    @Autowired
    private URI absScriptPathURI;

    @Autowired
    private String url;

    @Test
    public void testExecute() throws SQLException {
        sqlScriptExecutor.execute(absScriptPathURI);

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select course_id, name, course from Course where course_id = '1'");) {

            rs.next();
            assertEquals("Sam", rs.getString("name"));
            assertEquals("Java", rs.getString("course"));
            assertNotEquals("Bam", rs.getString("name"));

            try (ResultSet rsNext = stmt.executeQuery("select course_id, name, course from Course where name = 'Jonathan'")) {
                rsNext.next();
                assertEquals(4, rsNext.getInt("course_id"));
                assertEquals("Hibernate", rsNext.getString("course"));
                assertNotEquals(5, rsNext.getInt("course_id"));
            }
        }
    }
}
