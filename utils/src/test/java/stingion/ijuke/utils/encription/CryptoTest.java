package stingion.ijuke.utils.encription;

import org.junit.Test;
import stingion.ijuke.utils.exception.CryptoException;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class CryptoTest {

    private static final String ANY_TEXT_WORD = "any text";

    private String getPassword() {
        return "userpass1";
    }

    @Test
    public void testBasicHTTPAuth() {
        final String username = "user";
        final String password = getPassword();//Needed for Sonar

        String expected = "Basic dXNlcjp1c2VycGFzczE=";
        String actual = Crypto.basicHTTPAuth(username, password, true);
        assertEquals(expected, actual);

        expected = "dXNlcjp1c2VycGFzczE=";
        actual = Crypto.basicHTTPAuth(username, password, false);
        assertEquals(expected, actual);

        expected = "Basic dXNlcjp1c2VycGFzczE=";
        actual = Crypto.basicHTTPAuth(username, password);
        assertEquals(expected, actual);
    }

    @Test(expected = CryptoException.class)
    public void testHashCodeWithNotExistedAlgorithm() {
        String input = ANY_TEXT_WORD;
        String algorithm = "NOT_EXISTED_ALGORITHM";
        Crypto.hashCode(input, algorithm);
    }

    @Test
    public void testHashCodeMD5() {
        String expected = "ae821dc7991e1b65351446ab46f47d17";

        String input = ANY_TEXT_WORD;
        String actual = Crypto.hashCodeMD5(input);

        assertEquals(expected, actual);
    }

    @Test
    public void testHashCodeSHA1() {
        String expected = "5d48d16e1980be64d8246b52be2aa9c03aa552a9";

        String input = ANY_TEXT_WORD;
        String actual = Crypto.hashCodeSHA1(input);

        assertEquals(expected, actual);
    }

    @Test
    public void testHashCodeSHA256() {
        String expected = "06c83efcd22e3b755ca95ffe22954a304740027769fb541a126a5d23b6f5ac1f";

        String input = ANY_TEXT_WORD;
        String actual = Crypto.hashCodeSHA256(input);

        assertEquals(expected, actual);
    }

    @Test
    public void testHashCodeMD5Bytes() {
        byte[] expected = new byte[]{-82, -126, 29, -57, -103, 30, 27, 101, 53, 20, 70, -85, 70, -12, 125, 23};

        byte[] input = new byte[]{97, 110, 121, 32, 116, 101, 120, 116};
        byte[] actual = Crypto.hashCodeMD5(input);

        assertArrayEquals(expected, actual);
    }

    @Test
    public void testHashCodeSHA1Bytes() {
        byte[] expected = new byte[]{93, 72, -47, 110, 25, -128, -66, 100, -40, 36, 107, 82, -66, 42, -87, -64,
                58, -91, 82, -87};

        byte[] input = new byte[]{97, 110, 121, 32, 116, 101, 120, 116};
        byte[] actual = Crypto.hashCodeSHA1(input);

        assertArrayEquals(expected, actual);
    }

    @Test
    public void testHashCodeSHA256Bytes() {
        byte[] expected = new byte[]{6, -56, 62, -4, -46, 46, 59, 117, 92, -87, 95, -2, 34, -107, 74, 48, 71, 64,
                2, 119, 105, -5, 84, 26, 18, 106, 93, 35, -74, -11, -84, 31};

        byte[] input = new byte[]{97, 110, 121, 32, 116, 101, 120, 116};
        byte[] actual = Crypto.hashCodeSHA256(input);

        assertArrayEquals(expected, actual);
    }

    @Test
    public void testConvertBytesToHex() {
        String expected = "f57b5879f40305071ce9ff58827f0303";

        byte[] bytes = new byte[]{-11, 123, 88, 121, -12, 3, 5, 7, 28, -23, -1, 88, -126, 127, 3, 3};
        String actual = Crypto.convertBytesToHex(bytes);

        assertEquals(expected, actual);
    }

    @Test
    public void testConvertHexToBytes() {
        byte[] expected = new byte[]{-11, 123, 88, 121, -12, 3, 5, 7, 28, -23, -1, 88, -126, 127, 3, 3};

        String hex = "f57b5879f40305071ce9ff58827f0303";
        byte[] actual = Crypto.convertHexToBytes(hex);

        assertArrayEquals(expected, actual);
    }
}