/*
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.utils.useful;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesUtilTest {

    @Test
    public void testGetProperties() throws IOException {
        // Preparing test data
        File tempPropertiesFile = File.createTempFile("testPropertiesFile", ".file");
        tempPropertiesFile.deleteOnExit();
        OutputStream outputStreamForPropertiesFile = new FileOutputStream(tempPropertiesFile);

        Properties prop = new Properties();
        prop.setProperty("key1", "value1");
        prop.setProperty("key3", "value3");
        prop.setProperty("key2", "value2");
        prop.store(outputStreamForPropertiesFile, null);

        // Testing

        String key1 = "key1";
        String key2 = "key3";

        String expectedResult1 = "value1";
        String expectedResult2 = "value3";

        Properties properties = PropertiesUtil.getProperties(tempPropertiesFile.getPath());
        String actualResult1 = properties.getProperty(key1);
        String actualResult2 = properties.getProperty(key2);

        Assert.assertEquals(expectedResult1, actualResult1);
        Assert.assertEquals(expectedResult2, actualResult2);
    }
}
