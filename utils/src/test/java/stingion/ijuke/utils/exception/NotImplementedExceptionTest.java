/*
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.utils.exception;

import org.junit.Assert;
import org.junit.Test;

public class NotImplementedExceptionTest {

    @Test
    public void checkExceptionMessageTest() {
        String expectedMessage = "Not implemented";
        String actualMessage = new NotImplementedException().getMessage();
        Assert.assertEquals(expectedMessage, actualMessage);
    }
}
