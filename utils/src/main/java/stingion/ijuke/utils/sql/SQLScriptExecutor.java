package stingion.ijuke.utils.sql;

import org.apache.log4j.Logger;
import stingion.ijuke.utils.exception.FireRuntimeException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created under not commercial project
 */
public class SQLScriptExecutor {

    static final Logger logger = Logger.getLogger(SQLScriptExecutor.class);

    private String url;

    public SQLScriptExecutor(String driverName, String url) {
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException ex) {
            logger.error("Couldn't load JDBC driver", ex);
        }
        this.url = url;
    }

    public void execute(String scriptPath) throws URISyntaxException {
        execute(new URI("file://" + scriptPath));
    }

    public void execute(URI scriptURI) {
        execute(new File(scriptURI));
    }

    public boolean execute(File file) {

        boolean result = true;

        try (Connection con = DriverManager.getConnection(url);
             Statement stmt = con.createStatement();) {

            String newLineSymbol = System.lineSeparator();
            String fullScript = readFileToStringVar(file).replaceAll(newLineSymbol, "");
            String[] scriptStmts = fullScript.split(";");

            for (String scriptStmt : scriptStmts) {
                result &= stmt.execute(scriptStmt);
                if (logger.isDebugEnabled())
                    logger.debug(scriptStmt);
            }
        } catch (SQLException | IOException ex) {
            throw new FireRuntimeException(ex);
        }
        return result;
    }

    private static String readFileToStringVar(File file) throws IOException {
        //noinspection TryFinallyCanBeTryWithResources
        try (FileInputStream stream = new FileInputStream(file);
             FileChannel fc = stream.getChannel()) {
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            /* Instead of using default, pass in a decoder. */
            return Charset.defaultCharset().decode(bb).toString();
        }
    }
}
