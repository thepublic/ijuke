package stingion.ijuke.utils.annotation.processor;

import javax.lang.model.element.Element;

class Util {

    private Util() {
    }

    public static String elementType(Element element) {
        String type = new StringBuilder(element.getClass().getSimpleName())
                .reverse().replace(0, 6, "").reverse().toString();
        switch (type.toLowerCase()) {
            case "class":
                return "class";
            case "var":
                return "variable";
            case "method":
                return "method";
            case "package":
                return "package";
            default:
                return "unknown element type";
        }
    }
}
