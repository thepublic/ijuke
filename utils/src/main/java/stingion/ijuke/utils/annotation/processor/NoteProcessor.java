package stingion.ijuke.utils.annotation.processor;

import org.apache.log4j.Logger;
import stingion.ijuke.utils.annotation.Note;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.FilerException;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.io.Writer;
import java.util.Set;

@SupportedAnnotationTypes(value = {"stingion.ijuke.utils.annotation.Note"})
@SupportedSourceVersion(SourceVersion.RELEASE_7)
public class NoteProcessor extends AbstractProcessor {

    private static final Logger logger = Logger.getLogger(NoteProcessor.class);

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Messager consoleLogger = processingEnv.getMessager();

        try {
            Filer filer = processingEnv.getFiler();
            FileObject o = filer.createResource(StandardLocation.SOURCE_OUTPUT, "stingion.ijuke.backside", "Note.txt");
            Writer w = o.openWriter();
            w.append("Notes about project entities:\n");
            for (Element element : roundEnv.getElementsAnnotatedWith(Note.class)) {
                w.append(" - [").append(element.toString()).append("](").append(Util.elementType(element))
                        .append("): ").append(element.getAnnotation(Note.class).record()).append("\n");
                w.flush();
                consoleLogger.printMessage(Diagnostic.Kind.NOTE, "File finished");
            }
            w.close();
        } catch (FilerException ex) {
            if (!ex.getMessage().matches("Attempt to reopen a file for .*"))
                consoleLogger.printMessage(Diagnostic.Kind.ERROR, ex.getMessage());
            logger.error(ex);
        } catch (IOException ex) {
            consoleLogger.printMessage(Diagnostic.Kind.ERROR, ex.getMessage());
            logger.error(ex);
        }
        return Boolean.TRUE;
    }

}
