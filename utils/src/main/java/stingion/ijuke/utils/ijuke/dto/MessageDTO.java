package stingion.ijuke.utils.ijuke.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created under not commercial project
 */
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MessageDTO {
    private Integer id;//NOSONAR
    private String from;//NOSONAR
    private String to;//NOSONAR
    private String cc;//NOSONAR
    private String bcc;//NOSONAR
    private String subject;//NOSONAR
    private String text;//NOSONAR
    private Date date;//NOSONAR
    private List<AttachmentDTO> attachments = new ArrayList<>();//NOSONAR
    private FolderDTO folder;//NOSONAR

}
