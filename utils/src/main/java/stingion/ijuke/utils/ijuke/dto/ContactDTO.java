package stingion.ijuke.utils.ijuke.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;
import stingion.ijuke.utils.ijuke.modeltypes.Gender;

import java.util.Date;
import java.util.Set;

/**
 * Created under not commercial project
 */
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactDTO {
    private Integer id;//NOSONAR
    private String firstName;//NOSONAR
    private String middleName;//NOSONAR
    private String lastName;//NOSONAR
    private String email;//NOSONAR
    private String phoneNumber;//NOSONAR
    private Date birthDay;//NOSONAR
    private Set<CreditCardDTO> creditCards;//NOSONAR
    private Gender gender;//NOSONAR
}
