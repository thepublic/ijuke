/**
 * Created under not commercial project "ijuke"
 *
 */
package stingion.ijuke.utils.ijuke.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FolderDTO {
    private Integer id;//NOSONAR
    private String name;//NOSONAR
    private Set<MessageDTO> messages;//NOSONAR
}
