package stingion.ijuke.utils.ijuke.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

/**
 * Created under not commercial project
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductSupplierDTO {
    private ProductSupplierIdDTO pk;

    public ProductSupplierDTO(ProductDTO product, SupplierDTO supplier) {
        pk.setProduct(product);
        pk.setSupplier(supplier);
    }
}
