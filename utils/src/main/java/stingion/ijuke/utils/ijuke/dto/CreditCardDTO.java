package stingion.ijuke.utils.ijuke.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;
import stingion.ijuke.utils.ijuke.modeltypes.CardType;

import java.util.Set;

/**
 * Created under not commercial project
 */
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreditCardDTO {
    private Integer id;//NOSONAR
    private Long number;//NOSONAR
    private CardType type;//NOSONAR
    private BankDTO bank;//NOSONAR
    private ContactDTO contact;//NOSONAR
    private Set<OrderingDTO> orderings;//NOSONAR
}
