package stingion.ijuke.utils.ijuke.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;
import stingion.ijuke.utils.ijuke.modeltypes.OrderStatus;

/**
 * Created under not commercial project
 */
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderingDTO {
    private Integer id;//NOSONAR
    private String description;//NOSONAR
    private OrderStatus status;//NOSONAR
    private ProductDTO product;//NOSONAR
    private CreditCardDTO creditCard;//NOSONAR
}
