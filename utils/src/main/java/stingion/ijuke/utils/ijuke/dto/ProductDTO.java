package stingion.ijuke.utils.ijuke.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.util.Set;

/**
 * Created under not commercial project
 */
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDTO {
    private Integer id;//NOSONAR
    private String name;//NOSONAR
    private Double price;//NOSONAR
    private Set<OrderingDTO> orderings;//NOSONAR
    private Set<ProductSupplierDTO> productSuppliers;//NOSONAR
}
