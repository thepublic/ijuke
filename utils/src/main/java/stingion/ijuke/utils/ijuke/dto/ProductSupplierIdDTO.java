package stingion.ijuke.utils.ijuke.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.io.Serializable;

/**
 * Created under not commercial project
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductSupplierIdDTO implements Serializable {
    private ProductDTO product;//NOSONAR
    private SupplierDTO supplier;//NOSONAR
}
