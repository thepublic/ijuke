package stingion.ijuke.utils.ijuke.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

/**
 * Created under not commercial project
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AttachmentDTO {
    private String fileName;//NOSONAR
    private Long size;//NOSONAR
    private String contentType;//NOSONAR
}
