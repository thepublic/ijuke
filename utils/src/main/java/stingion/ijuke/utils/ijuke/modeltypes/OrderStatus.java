package stingion.ijuke.utils.ijuke.modeltypes;

/**
 * Created under not commercial project
 */
public enum OrderStatus {
    PENDING, PERFORMED, CANCELLED
}
