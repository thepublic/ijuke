/*
 * Created under not commercial project
 */

package stingion.ijuke.utils.useful;

import org.jbehave.core.model.ExamplesTable;

import java.util.List;
import java.util.Map;

/**
 * Created under not commercial project
 */
public class JBehaveUtil {

    private JBehaveUtil() {
    }

    /**
     * Convert jbehave table (given usually as parameter) to String[][]
     *
     * @param table for convert
     * @return String[][] array
     */
    public static String[][] convertJbehaveTable(ExamplesTable table) {
        List<Map<String, String>> rows = table.getRows();
        String[][] convertedTable = new String[rows.size() + 1][rows.get(0).size()];

        //Create header of table
        convertedTable[0] = table.getHeaders().toArray(new String[0]);

        //Content of table cells
        int tableRow = 0;
        for (Map<String, String> row : rows) {

            tableRow++;
            int rowCell = 0;

            for (Map.Entry<String,String> entry : row.entrySet()) {
                convertedTable[tableRow][rowCell++] = row.get(entry.getKey());
            }
        }

        return convertedTable;
    }
}
