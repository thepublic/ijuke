/*
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.utils.useful;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesUtil {

    private PropertiesUtil() {
    }

    /**
     * @param propertiesPath file path of property file
     */
    public static Properties getProperties(String propertiesPath) throws IOException {
        FileInputStream propertyFile = new FileInputStream(propertiesPath);
        Properties prop = new Properties();
        prop.load(propertyFile);
        return prop;
    }
}
