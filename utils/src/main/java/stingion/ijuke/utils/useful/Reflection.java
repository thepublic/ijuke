package stingion.ijuke.utils.useful;

import stingion.ijuke.utils.exception.ReflectionRuntimeException;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

/**
 * Created under not commercial project
 */
public class Reflection {

    private Reflection() {
    }

    public static <T> Serializable invokeGetId(T entity) {
        return invokeMethodByName(entity, "getId");
    }

    public static <T> Serializable invokeMethodByName(T entity, String methodName) {
        try {
            return (Serializable) getMethod(entity.getClass(), methodName).invoke(entity);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            throw new ReflectionRuntimeException(ex);
        }
    }

    public static Method getMethod(Class<?> clazz, String methodName, Class<?>... params) {
        try {
            return clazz.getDeclaredMethod(methodName, params);
        } catch (NoSuchMethodException ex) {
            throw new ReflectionRuntimeException(ex);
        }
    }

    /**
     * After getting class you should cast it to {@code Class<T>}, where {@code T} is your class param
     */
    public static Class<?> getParameterClass(Class<?> parametrizedClass) {
        ParameterizedType pType = (ParameterizedType) parametrizedClass.getGenericSuperclass();
        return (Class<?>) pType.getActualTypeArguments()[0];
    }

    /**
     * See overloaded method {@link #getParameterClass(Class)}
     */
    public static Class<?> getParameterClass(Object parametrizedObject) {
        return getParameterClass(parametrizedObject.getClass());
    }
}
