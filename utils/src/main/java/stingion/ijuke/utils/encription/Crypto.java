package stingion.ijuke.utils.encription;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.codec.binary.Base64;
import stingion.ijuke.utils.exception.CryptoException;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Crypto { //NOSONAR

    public static String basicHTTPAuth(String username, String password) {
        return basicHTTPAuth(username, password, true);
    }

    /**
     * @param isBasic Add "Basic " as prefix (It's useful for browser Basic Auth)
     */
    public static String basicHTTPAuth(String username, String password, boolean isBasic) {
        String authString = username + ":" + password;
        return (isBasic ? "Basic " : "") + Base64.encodeBase64String(authString.getBytes());
    }

    public static String hashCodeMD5(String input) {
        return hashCode(input, "MD5");
    }

    public static String hashCodeSHA1(String input) {
        return hashCode(input, "SHA-1");
    }

    public static String hashCodeSHA256(String input) {
        return hashCode(input, "SHA-256");
    }

    public static byte[] hashCodeMD5(byte[] input) {
        return hashCode(input, "MD5");
    }

    public static byte[] hashCodeSHA1(byte[] input) {
        return hashCode(input, "SHA-1");
    }

    public static byte[] hashCodeSHA256(byte[] input) {
        return hashCode(input, "SHA-256");
    }

    public static String hashCode(String input, String algorithm) {
        try {
            byte[] digest = hashCode(input.getBytes("UTF-8"), algorithm);
            return convertBytesToHex(digest);
        } catch (UnsupportedEncodingException ex) {
            throw new CryptoException(ex);
        }
    }

    public static byte[] hashCode(byte[] input, String algorithm) {
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm.toUpperCase());
            md.reset();
            md.update(input);
            return md.digest();
        } catch (NoSuchAlgorithmException ex) {
            throw new CryptoException(ex);
        }
    }

    public static String convertBytesToHex(byte[] bytes) {
        String hexStr = "";
        for (byte curByte : bytes) {
            hexStr += Integer.toString((curByte & 0xff) + 0x100, 16).substring(1);
        }
        return hexStr;
    }

    public static byte[] convertHexToBytes(String hex) {
        int len = hex.length();
        byte[] bytesArr = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            bytesArr[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4)
                    + Character.digit(hex.charAt(i + 1), 16));
        }
        return bytesArr;
    }
}
