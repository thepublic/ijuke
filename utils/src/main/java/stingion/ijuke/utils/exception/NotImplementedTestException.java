package stingion.ijuke.utils.exception;

/**
 * Created under not commercial project
 */
public class NotImplementedTestException extends UnsupportedOperationException {

    public NotImplementedTestException() {
        super("Not implemented test");
    }
}
