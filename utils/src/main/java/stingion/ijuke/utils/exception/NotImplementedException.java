package stingion.ijuke.utils.exception;

/**
 * Created under not commercial project
 */
public class NotImplementedException extends UnsupportedOperationException {

    public NotImplementedException() {
        super("Not implemented");
    }
}

