package stingion.ijuke.utils.exception;

/**
 * Created under not commercial project
 */
public class CryptoException extends UnsupportedOperationException {

    public CryptoException(Throwable cause) {
        super(cause);
    }
}

