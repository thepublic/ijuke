package stingion.ijuke.utils.exception;

public class FireRuntimeException extends RuntimeException {
    public FireRuntimeException(Throwable cause) {
        super(cause);
    }

    public FireRuntimeException(String message) {
        super(message);
    }
}
