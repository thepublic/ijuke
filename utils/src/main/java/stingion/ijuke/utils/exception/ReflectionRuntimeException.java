package stingion.ijuke.utils.exception;

public class ReflectionRuntimeException extends RuntimeException {
    public ReflectionRuntimeException(Throwable cause) {
        super(cause);
    }
}
