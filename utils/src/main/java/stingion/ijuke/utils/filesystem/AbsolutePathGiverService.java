package stingion.ijuke.utils.filesystem;

import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.net.URI;

/**
 * Created under not commercial project
 */
public class AbsolutePathGiverService {

    public String getClassPath() throws IOException {
        return new ClassPathResource(".").getFile().getAbsolutePath();
    }

    public File getFile(String relScriptPath) throws IOException {
        return new ClassPathResource(relScriptPath).getFile();
    }

    public URI getURI(String relScriptPath) throws IOException {
        return new ClassPathResource(relScriptPath).getURI();
    }

    public String getPath(String relScriptPath) throws IOException {
        return new ClassPathResource(relScriptPath).getFile().getAbsolutePath();
    }
}
