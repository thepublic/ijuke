package stingion.ijuke.utils.ws;

import lombok.Getter;
import lombok.Setter;

import javax.xml.ws.Endpoint;

/**
 * Created under not commercial project
 */
public class EndpointPublisher {

    @Getter
    @Setter
    private String endpointURL;

    @Getter
    @Setter
    private Object implementer;

    private Endpoint endpoint;

    public void start() {
        if (!isPublished())
            endpoint = Endpoint.publish(endpointURL, implementer);
    }

    public void stop() {
        if (isPublished())
            endpoint.stop();
    }

    public boolean isPublished() {
        return endpoint != null && endpoint.isPublished();
    }
}
