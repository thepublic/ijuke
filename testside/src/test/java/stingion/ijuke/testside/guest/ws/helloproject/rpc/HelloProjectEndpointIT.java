/*
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.testside.guest.ws.helloproject.rpc;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:META-INF/stingion/ijuke/testside/spring-test/webservices.xml")
@Ignore//NOSONAR Todo: It should be fixed/non-ignored in scope of Issue #17
public class HelloProjectEndpointIT {

    @Value("${backside.image.path}")
    private String moduleImageFileRelPath;
    
    @Autowired
    private HelloProjectEndpoint helloProjectEndpoint;

    @Test
    public void testGetHello() {
        String expected = "Hello, this is \"backside\" module of \"ijuke\" project";
        String actual = helloProjectEndpoint.getHello();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetHelloOverloaded() {
        String expected = "Hello, USER, this is \"backside\" module of \"ijuke\" project";
        String actual = helloProjectEndpoint.getHelloOverloaded("USER");
        assertEquals(expected, actual);
    }

    @Test
    public void testGetHelloWithName() {
        String expected = "Hello, USER, this is \"backside\" module of \"ijuke\" project";
        String actual = helloProjectEndpoint.getHelloWithName("USER");
        assertEquals(expected, actual);
    }

    @Test
    public void testDownloadModuleImage() throws IOException {
        Path imageFilePath = Paths.get(getClass().getClassLoader().getResource(moduleImageFileRelPath).getPath());
        byte[] imageDataExpected = Files.readAllBytes(imageFilePath);        
        byte[] imageDataActual = helloProjectEndpoint.downloadModuleImage();
        assertArrayEquals(imageDataExpected, imageDataActual);
    }

    @Test
    public void testUploadModuleImage() throws IOException {
        String imageFileNameForUpload = "uploadImage.png";
        Path imageFilePath = Paths.get(getClass().getClassLoader().getResource(moduleImageFileRelPath).getPath());
        byte[] imageFileDataForUpload = Files.readAllBytes(imageFilePath);

        SoapFile soapFile = new SoapFile();
        soapFile.setFileName(imageFileNameForUpload);
        soapFile.setFileData(imageFileDataForUpload);

        String actualResponse = helloProjectEndpoint.uploadModuleImage(soapFile);
        String expectedResponse = "Upload successful";

        assertEquals(expectedResponse, actualResponse);
    }
}
