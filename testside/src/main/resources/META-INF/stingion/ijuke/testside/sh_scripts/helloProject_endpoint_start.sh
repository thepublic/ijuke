#!/bin/sh
_script_dir=$(dirname $0);
_wscommand_dir="$_script_dir/../../../../../../../../../backside";
cd "$_wscommand_dir";
mvn exec:java -Dexec.mainClass="stingion.ijuke.backside.useful.HelloProjectPublisherStarter"&
_command_pid=$!;
echo "Started endpoint PID: $_command_pid";
cd "..";
_pid_file_path="helloProject_endpoint_pid.txt";
if [ -f "$_pid_file_path" ]; then
    rm -f "$_pid_file_path"
fi
echo ${_command_pid} > ${_pid_file_path};
echo "Writed endpoint PID: $_command_pid to file";
