#!/bin/sh
_script_dir=$(dirname $0);
_wscommand_dir="$_script_dir/../../../../../../../../..";
cd "$_wscommand_dir";
_pid_file_path="helloProject_endpoint_pid.txt";
_read_command_pid=$(cat "$_pid_file_path");
echo "Read endpoint PID: '$_read_command_pid' from file";
kill -9 "$_read_command_pid";
echo "Process with PID: $_read_command_pid is killed";
if [ -f "$_pid_file_path" ]; then
    rm -f "$_pid_file_path"
fi
