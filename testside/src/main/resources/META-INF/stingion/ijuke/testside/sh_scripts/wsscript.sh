#!/bin/sh
###
### For execute script go to assembly module and execute command: mvn exec:exec -Dwsimport
### README: For execute script go to assembly module and execute command:
### 1) Run HelloProjectPublisher
### 2) Go to "testside" module and execute: mvn exec:exec -Dwsimport
###

echo "ECHO: started script 'wsscript.sh' >>>";

_ws_url="http://localhost:9999/ws/helloProject?wsdl";
_script_dir=$(dirname $0);
_wsimport_dir="$_script_dir/../../../../../../java/stingion/ijuke/testside/guest/ws/helloproject/rpc";

if [ -d "$_wsimport_dir" ]; then
    echo "delete package: 'stingion.ijuke.testside.guest.ws.helloproject.rpc'";
    rm -rf "$_wsimport_dir/*.*";
fi
echo "ECHO: execute wsimport '$_ws_url";
wsimport -Xnocompile -p stingion.ijuke.testside.guest.ws.helloproject.rpc -s ${_script_dir}/../../../../../../java ${_ws_url};

echo "ECHO: finished script 'wsscript.sh' <<<";
