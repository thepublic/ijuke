/*
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.testside.guest.ws.helloproject.rpc.util;

import org.apache.log4j.Logger;
import stingion.ijuke.testside.guest.ws.helloproject.rpc.HelloProjectEndpoint;
import stingion.ijuke.testside.guest.ws.helloproject.rpc.HelloProjectEndpointImplService;

/**
 * Created under not commercial project
 */
public class WsClient {

    static final Logger logger = Logger.getLogger(WsClient.class);

    private WsClient() {
    }

    public static void main(String[] args) {
        HelloProjectEndpointImplService service = new HelloProjectEndpointImplService();
        HelloProjectEndpoint endpoint = service.getHelloProjectEndpointImplPort();

        logger.debug(endpoint.getHello());
    }
}
