/*
 * Created under not commercial project "ijuke"
 */

package stingion.ijuke.testside.guest.ws.helloproject.rpc.util;

import org.apache.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Set;

/**
 * Created under not commercial project
 * Add @HandlerChain(file = "MacInjection-handler-chain.xml") to HelloProjectEndpointImplService
 */
public class MacAddressInjectHandler implements SOAPHandler<SOAPMessageContext> {

    private static final Logger log = Logger.getLogger(MacAddressInjectHandler.class);

    @Override
    public boolean handleMessage(SOAPMessageContext context) {

        log.debug("Client : handleMessage()......");

        Boolean isRequest = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        //if this is a request, true for outbound messages, false for inbound
        if (isRequest) {

            try {
                SOAPMessage soapMsg = context.getMessage();
                SOAPEnvelope soapEnv = soapMsg.getSOAPPart().getEnvelope();
                SOAPHeader soapHeader = soapEnv.getHeader();

                //if no header, add one
                if (soapHeader == null) {
                    soapHeader = soapEnv.addHeader();
                }

                //get mac address
                String mac = getMACAddress();

                //add a soap header, name as "mac address"
                String namespaceURI = "http://rpc.helloproject.ws.backside.ijuke.stingion/";
                String localPart = "HelloProjectEndpointImplService";
                QName qname = new QName(namespaceURI, localPart);
                SOAPHeaderElement soapHeaderElement = soapHeader.addHeaderElement(qname);

                soapHeaderElement.setActor(SOAPConstants.URI_SOAP_ACTOR_NEXT);
                soapHeaderElement.addTextNode(mac);
                soapMsg.saveChanges();


            } catch (SOAPException ex) {
                log.warn(ex);
            }
        }

        //continue other handler chain
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        log.debug("Client : handleFault()......");
        return true;
    }

    @Override
    public void close(MessageContext context) {
        log.debug("Client : close()......");
    }

    @Override
    public Set<QName> getHeaders() {
        log.debug("Client : getHeaders()......");
        return Collections.emptySet();
    }

    //return current client mac address
    private String getMACAddress() {

        StringBuilder sb = new StringBuilder();

        try {
            NetworkInterface networkInterface = NetworkInterface.getByName("eth0");
            log.debug("Current IP address : " + networkInterface.getInterfaceAddresses().get(1).getAddress());

            byte[] mac = networkInterface.getHardwareAddress();

            for (int i = 0; i < mac.length; i++) {

                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));

            }

            log.debug("Current MAC address : " + sb.toString());

        } catch (SocketException ex) {
            log.warn(ex);
        }
        return sb.toString();
    }
}
