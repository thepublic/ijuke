package stingion.ijuke.testsidefrontside.steps;

import net.sourceforge.jwebunit.junit.WebTester;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import stingion.ijuke.utils.useful.JBehaveUtil;

import java.util.List;

@Component
public class CheckPageContent {

    @Autowired
    protected WebTester tester;

    @Autowired
    private String sitePlace;

    @Value("${frontside.hostPort}")
    private String defaultSitePlace;

    @Given("Go to project website placed on default domain and port")
    public void webSitePlaceByDefault() {
        sitePlace = defaultSitePlace;
    }

    @Given("Go to project website placed on domain and port $sitePlace")
    public void webSitePlace(@Named("sitePlace") String sitePlace) {
        this.sitePlace = sitePlace;
    }

    @When("As user I go to page $page")
    public void goToPage(@Named("page") String page) {
        tester.setBaseUrl(sitePlace);
        tester.beginAt(page);
    }

    @Then("Check that page title is $title")
    public void checkTitle(@Named("title") String title) {
        tester.assertTitleEquals(title);
    }

    @Then("Check such text: $text is present")
    public void checkSuchTextPresent(@Named("text") String text) {
        tester.assertTextPresent(text);
    }

    @Then("Http response code is $httpResponseCode")
    public void checkHttpStatusCode(@Named("httpResponseCode") String httpStatusCode) {
        tester.assertResponseCode(Integer.valueOf(httpStatusCode));
    }

    @Then("Check such table (give id or name or summary): $table is present")
    public void checkSuchTablePresent(@Named("table") String table) {
        tester.assertTablePresent(table);
    }

    @Then("Check such text: $text in table $table (give id or name or summary) exists")
    public void checkSuchTextInTableIsPresen11t(@Named("table") String table, @Named("text") String text) {
        tester.assertTextInTable(table, text);
    }

    @Then("Check such text: $text in table $table (give id or name or summary) doesn't exist")
    public void checkSuchTextInTableIsNotPresent(@Named("table") String table, @Named("text") String text) {
        tester.assertNoMatchInTable(table, text);
    }

    @Then("Check such array of text: $tableContent in table $table (give id or name or summary) is present")
    public void checkSuchArrayTextInTableIsPresent(@Named("table") String table, @Named("tableContent") ExamplesTable tableContent) {
        List<String> arrayOfText = tableContent.getHeaders();
        tester.assertTextInTable(table, arrayOfText.toArray(new String[0]));
    }

    @Then("Check such array of text: $tableContent in table $table (give id or name or summary) isn't present")
    public void checkSuchArrayTextInTableIsNotPresent(@Named("table") String table, @Named("tableContent") ExamplesTable tableContent) {
        List<String> arrayOfText = tableContent.getHeaders();
        tester.assertNoMatchInTable(table, arrayOfText.toArray(new String[0]));
    }

    @Then("Check the table $table (give id or name or summary) is the same as given $comparedTable")
    public void checkSuchTableIsAsGiven(@Named("table") String table, @Named("comparedTable") ExamplesTable comparedTable) {
        String[][] conversionFromTable = JBehaveUtil.convertJbehaveTable(comparedTable);
        tester.assertTableEquals(table, conversionFromTable);
    }
}
