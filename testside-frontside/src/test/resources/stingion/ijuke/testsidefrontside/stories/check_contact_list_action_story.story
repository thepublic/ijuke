Narrative:
As a user I want to check if underlying web pages are correct

Scenario: Check ClientList.action page and its table

Given Go to project website placed on default domain and port
When As user I go to page /frontside/ContactList.action
Then Http response code is 200
And Check that page title is Contact List
And Check such table (give id or name or summary): contact is present
And Check such table (give id or name or summary): Contact List Table is present
And Check such text: First name in table Contact List Table (give id or name or summary) exists
And Check such text: Middle name in table Contact List Table (give id or name or summary) exists
And Check such text: Last name in table Contact List Table (give id or name or summary) exists
And Check such text: Email in table Contact List Table (give id or name or summary) exists
And Check such text: Action in table Contact List Table (give id or name or summary) exists
And Check such text: Telephone number in table Contact List Table (give id or name or summary) doesn't exist
And Check such text: Date of birth in table Contact List Table (give id or name or summary) doesn't exist
And Check such array of text: |Donna|Ostin|McCallum|dm@isystem.com|
    in table Contact List Table (give id or name or summary) is present
And Check such array of text: |DonnaFake|OstinFake|McCallumFake|dmFake@isystem.com|
    in table Contact List Table (give id or name or summary) isn't present
