Narrative:
As a user I want to check if underlying web pages are correct

Scenario: Check titles and contents of pages:
    frontside
    frontside/description.jsp
    frontside/MessageList.action
    frontside/MessageCompose.action

Given Go to project website placed on domain and port "<sitePlace>"
When As user I go to page "<page>"
Then Http response code is "<httpResponseCode>"
And Check that page title is "<title>"
And Check such text: "<text>" is present

Examples:
|sitePlace             |page                             |title                     |text                      |httpResponseCode |
|${frontside.hostPort} |/frontside                       |Frontside Web Application |Welcome to Ijuke          |200              |
|${frontside.hostPort} |/frontside/description.jsp       |Description               |                          |200              |
|${frontside.hostPort} |/frontside/MessageList.action    |Message List              |Message list goes here    |200              |
|${frontside.hostPort} |/frontside/MessageCompose.action |Message Compose           |Message Compose goes here |200              |
